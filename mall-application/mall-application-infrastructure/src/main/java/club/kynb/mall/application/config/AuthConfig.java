package club.kynb.mall.application.config;

import lombok.Data;
import lombok.experimental.Accessors;


/**
 * 认证配置数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2020-12-25
 */

@Data
@Accessors(chain = true)
public class AuthConfig {
    private static final long serialVersionUID = 1L;
    /**
     * 客户端类型（PC、APP、H5）
     */
    private String clientType;
    /**
     * 监听时间
     */
    private Integer watchTime;
    /**
     * 失败次数
     */
    private Integer failedCount;
    /**
     * 锁定时间(单位分钟)
     */
    private Integer lockedTime;
    /**
     * token的过期时间(单位分钟)
     */
    private Integer tokenExpire;
    /**
     * 开启图形验证码
     */
    private boolean captchaEnable = false;
}
