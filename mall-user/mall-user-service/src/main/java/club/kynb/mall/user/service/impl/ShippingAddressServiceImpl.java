package club.kynb.mall.user.service.impl;

import club.kynb.mall.user.api.IShippingAddressService;
import club.kynb.mall.user.model.dto.ShippingAddressDTO;
import club.kynb.mall.user.mysql.mapper.ShippingAddressMapper;
import club.kynb.mall.user.mysql.po.ShippingAddressPO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.exception.Errors;
import org.pizza.constant.GlobalConstant;
import org.pizza.util.Checker;
import org.pizza.util.Convertor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


/**
 * @author kynb_club@163.com
 * @since 2021/6/27 1:00 下午
 */
@Slf4j
@Service
@AllArgsConstructor
public class ShippingAddressServiceImpl implements IShippingAddressService {
    private final ShippingAddressMapper shippingAddressMapper;

    @Override
    public List<ShippingAddressDTO> addressList(Long userId) {
        final List<ShippingAddressPO> poList = this.getPoList(userId);
        return Convertor.convert(poList, ShippingAddressDTO.class);
    }

    private List<ShippingAddressPO> getPoList(Long userId) {
        return shippingAddressMapper.selectList(new QueryWrapper<ShippingAddressPO>().lambda().eq(ShippingAddressPO::getUserId, userId));
    }

    @Override
    public void deleteAddress(Long addressId) {
        final int delete = shippingAddressMapper.deleteById(addressId);
        Checker.ifNotThrow(delete > 0, () -> Errors.SYSTEM.exception("系统异常，删除收货地址失败"));
    }

    @Override
    @Transactional
    public void updateAddress(ShippingAddressDTO addressDTO) {
        final ShippingAddressPO addressPO = Convertor.convert(ShippingAddressPO.class, addressDTO);
        this.defaultExclusion(addressPO);
        final int update = shippingAddressMapper.updateById(addressPO);
        Checker.ifNotThrow(update > 0, () -> Errors.SYSTEM.exception("系统异常，保存收货地址失败！"));
    }

    @Override
    @Transactional
    public void createAddress(ShippingAddressDTO addressDTO) {
        final Integer count = shippingAddressMapper.selectCount(new QueryWrapper<ShippingAddressPO>().lambda()
                .eq(ShippingAddressPO::getUserId, addressDTO.getUserId()));
        Checker.ifThrow(count >= 5, () -> Errors.BIZ.exception("不可创建超过5个收货地址~"));
        //没有其他
        if (count == 0) {
            addressDTO.setDefaulted(GlobalConstant.YES);
        }
        final ShippingAddressPO addressPO = Convertor.convert(ShippingAddressPO.class, addressDTO);
        this.defaultExclusion(addressPO);
        final int insert = shippingAddressMapper.insert(addressPO);
        Checker.ifNotThrow(insert > 0, () -> Errors.SYSTEM.exception("系统异常，保存收货地址失败！"));
    }

    @Override
    public Optional<ShippingAddressDTO> getById(Long shippingAddressId) {
        final ShippingAddressPO shippingAddressPO = shippingAddressMapper.selectById(shippingAddressId);
        if (shippingAddressPO == null) {
            return Optional.empty();
        }
        return Optional.of(Convertor.convert(ShippingAddressDTO.class, shippingAddressPO));
    }

    private void defaultExclusion(ShippingAddressPO addressPO) {
        //默认收货地址互斥处理
        if (addressPO.getDefaulted().equals(GlobalConstant.YES)) {
            final List<ShippingAddressPO> poList = this.getPoList(addressPO.getUserId());
            poList.forEach(po -> po.setDefaulted(GlobalConstant.NO));
            poList.forEach(shippingAddressMapper::updateById);
        }
    }
}
