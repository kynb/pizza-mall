package club.kynb.mall.application.statemachine.order.action;

import club.kynb.mall.application.context.OrderContext;
import club.kynb.mall.application.model.model.command.ConfirmOrderCommand;
import club.kynb.mall.application.model.model.event.OrderConfirmEvent;
import club.kynb.mall.order.api.IOrderService;
import club.kynb.mall.order.constant.OrderEventEnum;
import club.kynb.mall.order.constant.OrderStatusEnum;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.event.publish.EventBus;
import org.pizza.statemachine.Action;
import org.springframework.stereotype.Component;

/**
 * @author kynb_club@163.com
 * @since 2021/7/9 11:18 上午
 */
@Slf4j
@Component
@AllArgsConstructor
public class OrderConfirmAction implements Action<OrderStatusEnum, OrderEventEnum, OrderContext> {
    private final IOrderService iOrderService;
    private final EventBus eventBus;

    @Override
    public void execute(OrderStatusEnum from, OrderStatusEnum to, OrderEventEnum on, OrderContext orderContext) {
        final ConfirmOrderCommand command = (ConfirmOrderCommand) orderContext.getCommand();
        iOrderService.orderConfirm(command.getInnerOrderNo(), command.getConfirmTypeEnum());
        eventBus.publishEvent(OrderConfirmEvent.builder().innerOrderNo(command.getInnerOrderNo()).build());
    }
}
