package club.kynb.mall.payment.constant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * 支付客户端类型
 *
 * @author kynb_club@163.com
 * @date:  2021/6/29
 */
@Getter
@AllArgsConstructor
public enum PayClientTypeEnum implements EnumValidI {

	IOS("IOS",""),
	ANDROID("ANDROID",""),
	H5("H5",""),
	;
	
	private String code;
	private String desc;

	@Override
	public String code() {
		return String.valueOf(this.code);
	}

	@Override
	public String message() {
		return this.desc;
	}

	@JsonIgnore
	public static Optional<PayClientTypeEnum> get(String code) {
		return Stream.of(PayClientTypeEnum.values()).filter(i -> i.getCode().equalsIgnoreCase(code)).findFirst();
	}
}
