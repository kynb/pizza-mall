package club.kynb.mall.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Arrays;
import java.util.Optional;

/**
 * 付款状态
 *
 * @author kynb_club@163.com
 * @date 2021/07/07
 */
@Getter
@AllArgsConstructor
public enum OrderPayStatusEnum implements EnumValidI {

    NO_PAY("NO_PAY", "未付款"),
    PAYING("PAYING", "付款中"),
    PAID("PAID", "已付款"),
    ;

    private final String status;
    private final String desc;

    @Override
    public String code() {
        return String.valueOf(this.status);
    }

    @Override
    public String message() {
        return this.desc;
    }

    public static Optional<OrderPayStatusEnum> get(String status) {
        return Arrays.stream(OrderPayStatusEnum.values())
                .filter(item -> item.getStatus().equals(status))
                .findFirst();
    }
}
