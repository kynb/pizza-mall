package club.kynb.mall.product.mysql.mapper;

import club.kynb.mall.product.mysql.po.ProductCategoryPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
/**
 * 商品分类表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */
@Mapper
public interface ProductCategoryMapper extends BaseMapper<ProductCategoryPO> {


}
