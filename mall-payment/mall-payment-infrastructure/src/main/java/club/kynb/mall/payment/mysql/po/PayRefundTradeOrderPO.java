package club.kynb.mall.payment.mysql.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.mybatis.plus.base.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 退款交易流水表持久实体类
 * @author fyh
 * @since 2021-07-04
 */

@Data
@Accessors(chain = true)
@TableName("mall_pay_refund_trade_order")
@EqualsAndHashCode(callSuper = true)
public class PayRefundTradeOrderPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 订单类型： 1 普通订单；2 钱包充值
     */
    private Integer originOrderType;
    /**
     * 订单号
     */
    private String originOrderNo;
    /**
     * 用户
     */
    private Long userId;
    /**
     * 原支付订单号
     */
    private String payTradeOrderNo;
    /**
     * 退款交易订单号
     */
    private String tradeOrderNo;
    /**
     * 退款金额
     */
    private BigDecimal refundAmount;
    /**
     * 状态：0 待处理； 1 成功；2 失败
     */
    private Integer status;
    /**
     * 退款时间（第三方）
     */
    private Date refundTime;
    /**
     * 退款流水号（第三方）
     */
    private String refundFlowNo;
    /**
     * 扩展字段
     */
    private String extension;


}
