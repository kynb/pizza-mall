package club.kynb.mall.application.repository;

import club.kynb.mall.application.model.model.dto.PreviewOrderDTO;
import club.kynb.mall.product.dto.ProductSpuDetailDTO;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/7/6 9:49 下午
 */
public interface OrderRepository {

    void savePreOrder(String innerOrderNo, PreviewOrderDTO previewOrderDTO);

    void savePreOrderSpuList(String innerOrderNo, List<ProductSpuDetailDTO> filteredSpuList);

    PreviewOrderDTO getPreOrder(String innerOrderNo);

    List<ProductSpuDetailDTO> getPreOrderSpuList(String innerOrderNo);

    void clearPreOrderCache(String innerOrderNo);
}