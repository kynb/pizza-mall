package club.kynb.mall.user.constant;


import org.pizza.valid.EnumValidI;


/**
 * @author kynb_club@163.com
 * @since 2020/12/8 2:44 下午
 */
public enum UserType implements EnumValidI {
    CUSTOMER,//商城客户
    ADMINISTRATOR,//管理员
    ;

    @Override
    public String code() {
        return this.name();
    }

    @Override
    public String message() {
        return "类型：" + this.name();
    }

}
