package club.kynb.mall.application.context;

import club.kynb.mall.application.model.model.command.AuthenticationCommand;
import club.kynb.mall.application.model.model.dto.TokenDTO;
import club.kynb.mall.basic.dto.HeaderDTO;
import club.kynb.mall.user.model.dto.UserDTO;
import lombok.Data;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 1:46 下午
 */
@Data
public class AuthenticationContext {
    private TokenDTO token = new TokenDTO();
    private UserDTO user;
    private AuthenticationCommand authenticationCommand;
    private HeaderDTO headerDTO = new HeaderDTO();
}
