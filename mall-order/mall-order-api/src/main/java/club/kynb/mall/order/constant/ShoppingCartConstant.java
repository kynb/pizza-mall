package club.kynb.mall.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author kynb_club@163.com
 * @since 2021/7/1 10:25 下午
 */
public interface ShoppingCartConstant {
    /**
     * 单选
     */
    int CHECK_SINGLE = 1;
    /**
     * 全选
     */
    int CHECK_ALL = 2;

    @Getter
    @AllArgsConstructor
    enum ShoppingCartChannelEnum {
        APP_MALL,
        H5
    }

    @Getter
    @AllArgsConstructor
    enum ShoppingCartStatusEnum {
        NORMAL(1, "正常"),
        DOWN(2, "已下架"),
        SELL_OUT(3, "已售罄"),
        ;

        private final Integer status;
        private final String desc;

    }

}
