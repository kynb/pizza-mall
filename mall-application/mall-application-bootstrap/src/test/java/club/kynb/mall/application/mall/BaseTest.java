package club.kynb.mall.application.mall;

import club.kynb.mall.product.mysql.mapper.ProductCategoryMapper;
import club.kynb.mall.product.mysql.po.ProductCategoryPO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author kynb_club@163.com
 * @date: 2021/6/26
 */
@SpringBootTest
public class BaseTest {

    @Autowired
    ProductCategoryMapper productCategoryMapper;

    @Test
    void test(){
        ProductCategoryPO productCategoryPO = new ProductCategoryPO();
        productCategoryPO.setCateName("11");
        productCategoryMapper.insert(productCategoryPO);
    }

}
