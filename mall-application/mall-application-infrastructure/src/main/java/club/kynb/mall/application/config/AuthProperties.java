package club.kynb.mall.application.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/1/4 11:18 上午
 */
@Component
@ConfigurationProperties(prefix = "mall.auth")
public class AuthProperties {

    private List<AuthConfig> configs;

    private List<String> ignoreUrs;

    private List<String> specialUrs;

    public Optional<AuthConfig> get(String clientType) {
        return configs.stream().filter(authConfig -> authConfig.getClientType().equals(clientType)).findFirst();
    }

    public List<AuthConfig> getConfigs() {
        return configs;
    }
    public void setConfigs(List<AuthConfig> configs) {
        this.configs = configs;
    }

    public List<String> getIgnoreUrs() {
        return ignoreUrs;
    }

    public void setIgnoreUrs(List<String> ignoreUrs) {
        this.ignoreUrs = ignoreUrs;
    }

    public List<String> getSpecialUrs() {
        return specialUrs;
    }

    public void setSpecialUrs(List<String> specialUrs) {
        this.specialUrs = specialUrs;
    }
}
