package club.kynb.mall.user.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

/**
 * 用户基础信息表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-06-26
 */

@Data
@ApiModel(value = "UserDTO对象", description = "用户基础信息表")
@Accessors(chain = true)
public class UserDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID(userId)
     */
    @ApiModelProperty(value = "主键ID(userId)", example = "")
    private Long id;
    /**
     * 电话
     */
    @ApiModelProperty(value = "电话", example = "")
    private String phone;
    @ApiModelProperty(value = "昵称", example = "")
    private String nickname;
    /**
     * 密码MD5(密码+盐)
     */
    @ApiModelProperty(value = "密码MD5(密码+盐)", example = "")
    private String password;
    /**
     * 盐
     */
    @ApiModelProperty(value = "盐", example = "")
    private String salt;
    /**
     * 状态枚举(FREEZE、NORMAL)
     */
    @ApiModelProperty(value = "状态枚举(FREEZE、NORMAL)", example = "NORMAL")
    private String status;
    /**
     * 类型枚举(CUSTOMER、ADMINISTRATOR)
     */
    @ApiModelProperty(value = "类型枚举(CUSTOMER、ADMINISTRATOR)", example = "CUSTOMER")
    private String type;
    /**
     * 当前token
     */
    @ApiModelProperty(value = "当前token", example = "")
    private String currentToken;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;

}
