package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;


/**
 * @author kynb_club@163.com
 * @since 2020/12/18 11:45 上午
 */
@Data
@ApiModel(value = "AppInfoDTO对象", description = "应用信息")
@Accessors(chain = true)
public class AppInfoDTO implements DTO {

    @ApiModelProperty(value = "版本号", example = "v1.0")
    private String version;


}
