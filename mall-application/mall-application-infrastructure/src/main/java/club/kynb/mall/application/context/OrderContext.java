package club.kynb.mall.application.context;

import org.pizza.model.Command;

/**
 * @author kynb_club@163.com
 * @since 2021/7/9 10:23 上午
 */
public interface OrderContext {

    Command getCommand();
}
