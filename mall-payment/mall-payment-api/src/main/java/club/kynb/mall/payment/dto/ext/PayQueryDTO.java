package club.kynb.mall.payment.dto.ext;

import club.kynb.mall.payment.constant.PayOrderTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

/**
 * 支付请求对象
 *
 * @author kynb_club@163.com
 * @date:  2021/7/2
 */
@Data
@ApiModel(value = "PayQueryDTO对象", description = "支付查询请求信息")
@Accessors(chain = true)
public class PayQueryDTO implements DTO {

	@ApiModelProperty(value = "支付订单类型", example = "")
	private PayOrderTypeEnum orderType;
	/**
	 * 	订单编号
	 */
	@ApiModelProperty(value = "订单编号", example = "")
	private String orderNo;


}
