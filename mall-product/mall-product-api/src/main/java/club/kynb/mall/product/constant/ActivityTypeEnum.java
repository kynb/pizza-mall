package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Arrays;
import java.util.Optional;

/**
 * 活动类型 1 商品 2 链接
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Getter
@AllArgsConstructor
public enum ActivityTypeEnum implements EnumValidI {

    PRODUCT(1,"商品"),
    URL(2,"链接"),
    ;

    private Integer type;
    private String desc;

    @Override
    public String code() {
        return String.valueOf(this.type);
    }

    @Override
    public String message() {
        return this.desc;
    }

    public static Optional<ActivityTypeEnum> get(Integer type){
        return Arrays.stream(ActivityTypeEnum.values())
                .filter(item-> item.getType().equals(type))
                .findFirst();
    }
}
