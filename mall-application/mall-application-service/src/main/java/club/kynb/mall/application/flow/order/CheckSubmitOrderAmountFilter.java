package club.kynb.mall.application.flow.order;

import club.kynb.mall.application.context.OrderSubmitContext;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author kynb_club@163.com
 * @since 2021/7/7 1:56 下午
 */
@Slf4j
@Component
@AllArgsConstructor
public class CheckSubmitOrderAmountFilter extends AbstractSubmitOrderFlowFilter {

    @Override
    protected void doService(OrderSubmitContext orderSubmitContext) {
        //校验金额如有需要
    }
}
