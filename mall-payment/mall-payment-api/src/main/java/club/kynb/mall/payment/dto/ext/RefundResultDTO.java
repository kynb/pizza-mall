package club.kynb.mall.payment.dto.ext;

import club.kynb.mall.payment.constant.RefundResultStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

/**
 * 支付结果对象
 *
 * @author kynb_club@163.com
 * @date:  2021/7/2
 */
@Data
@ApiModel(value = "PayResultDTO对象", description = "支付结果信息")
@Accessors(chain = true)
public class RefundResultDTO implements DTO {
	/**
	 * 	原始订单号
	 */
	@ApiModelProperty(value = "退款流水号", example = "")
	private String refundTradeOrderNo;

	@ApiModelProperty(value = "退款状态", example = "")
	private RefundResultStatusEnum status;


}
