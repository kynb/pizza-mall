package club.kynb.mall.payment.mysql.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.mybatis.plus.base.BaseEntity;


/**
 * 持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_pay_channel")
@EqualsAndHashCode(callSuper = true)
public class PayChannelPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 支付方式code
     */
    private String channelCode;
    /**
     * 支付方式名称
     */
    private String channelName;
    /**
     * 支付方式图标
     */
    private String channelIcon;
    /**
     * 开启状态
     */
    private Integer openStatus;
    /**
     * 客户端类型
     */
    private String clientTypes;
    /**
     * 支持订单类型
     */
    private String orderTypes;
    /**
     * 排序
     */
    private Integer sequence;
    /**
     * 扩展字段
     */
    private String extension;
    /**
     * 支付方式说明
     */
    private String channelDesc;


}
