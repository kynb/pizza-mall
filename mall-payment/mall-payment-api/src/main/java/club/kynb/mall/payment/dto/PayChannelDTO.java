package club.kynb.mall.payment.dto;

import org.pizza.model.DTO;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;

/**
 * 数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "PayChannelDTO对象", description = "PayChannelDTO对象")
@Accessors(chain = true)
public class PayChannelDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ApiModelProperty(value = "", example = "")
    private Long id;
    /**
     * 支付方式code
     */
    @ApiModelProperty(value = "支付方式code", example = "")
    private String channelCode;
    /**
     * 支付方式名称
     */
    @ApiModelProperty(value = "支付方式名称", example = "")
    private String channelName;
    /**
     * 支付方式图标
     */
    @ApiModelProperty(value = "支付方式图标", example = "")
    private String channelIcon;
    /**
     * 开启状态
     */
    @ApiModelProperty(value = "开启状态", example = "")
    private Integer openStatus;
    /**
     * 客户端类型
     */
    @ApiModelProperty(value = "客户端类型", example = "")
    private String clientTypes;
    /**
     * 支持订单类型
     */
    @ApiModelProperty(value = "支持订单类型", example = "")
    private String orderTypes;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序", example = "")
    private Integer sequence;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 支付方式说明
     */
    @ApiModelProperty(value = "支付方式说明", example = "")
    private String channelDesc;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
