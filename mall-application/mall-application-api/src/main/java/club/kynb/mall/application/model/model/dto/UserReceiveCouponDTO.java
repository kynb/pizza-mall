package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.util.Date;

/**
 * @author kynb_club@163.com
 * @since 2021/6/29 7:53 下午
 */
@Data
@ApiModel(value = "UserReceiveCouponDTO对象", description = "用户领取的优惠券")
@Accessors(chain = true)
public class UserReceiveCouponDTO implements DTO {
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", example = "")
    private Long userId;
    /**
     * 领取时间
     */
    @ApiModelProperty(value = "领取时间", example = "2021-05-28 08:28:00")
    private Date receiveTime;
    /**
     * 状态：0未使用，1已使用，2已过期
     */
    @ApiModelProperty(value = "状态：0未使用，1已使用，2已过期", example = "0")
    private Integer status;
    /**
     * 状态信息，例如：4天后即将过期
     */
    @ApiModelProperty(value = "状态信息，例如：4天后即将过期", example = "4天后即将过期")
    private String statusInfo;
    /**
     * ----------------------下列为模板字段-----------------------
     * 券名字（外部显示）
     */
    @ApiModelProperty(value = "优惠券名称", example = "")
    private String name;
    /**
     * 标题（外部显示）
     */
    @ApiModelProperty(value = "优惠券标题", example = "满99减28")
    private String title;
    /**
     * 描述（外部显示）
     */
    @ApiModelProperty(value = "优惠券描述", example = "全场商品满减")
    private String description;
    /**
     * 标签（外部显示）
     */
    @ApiModelProperty(value = "优惠券标签", example = "平台券")
    private String tag;
    /**
     * 面值（单位：抵扣券：元；折扣券：百分号前的数值，0.1-99.9）
     */
    @ApiModelProperty(value = "面值（单位：抵扣券：元；折扣券：百分号前的数值，0.1-99.9）", example = "28.00")
    private String couponValue;
    /**
     * 券类型（1：现金券、2：折扣券）
     */
    @ApiModelProperty(value = "券类型（1：现金券、2：折扣券）", example = "1")
    private Integer couponType;
    /**
     * 叠加使用，0：不允许叠加，1：允许同类叠加
     */
    @ApiModelProperty(value = "叠加使用，0：不允许叠加，1：允许同类叠加", example = "0")
    private Integer useSuperimposedType;
    /**
     * 领取方式，1：主动领取，2：系统发放
     */
    @ApiModelProperty(value = "领取方式，1：主动领取，2：系统发放", example = "1")
    private Integer receiveWay;
    /**
     * 生效时间
     */
    @ApiModelProperty(value = "生效时间", example = "2021-05-28 08:00:00")
    private Date effectiveTime;
    /**
     * 失效时间
     */
    @ApiModelProperty(value = "失效时间", example = "2021-06-28 08:00:00")
    private Date invalidTime;
    /**
     * 商品使用范围（1：指定商品，2：全部商品）
     */
    @ApiModelProperty(value = "商品使用范围（1：指定商品，2：全部商品）", example = "2")
    private Integer spuRange;
    /**
     * 指定商品的spuId列表,指定商品时有效
     */
    private String spuIds;
    /**
     * 撤单返还 0：不返还，1：返还
     */
    @ApiModelProperty(value = "撤单返还 0：不返还，1：返还", example = "",hidden = true)
    private Integer returnQualification;
    /**
     * 使用金额门槛，达到门槛，才能使用券
     */
    @ApiModelProperty(value = "使用金额门槛，达到门槛，才能使用券", example = "99.00")
    private String thresholdAmount;
}
