package club.kynb.mall.application.model.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Query;

import javax.validation.constraints.NotNull;

/**
 * @author kynb_club@163.com
 * @date: 2021/6/26
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "ProductQuery对象", description = "商品搜索")
@Accessors(chain = true)
public class CategoryProductListQuery extends Query {

    @NotNull(message = "请选择分类")
    @ApiModelProperty(value = "二级分类id", example = "",required = true)
    private Long categoryId;

}
