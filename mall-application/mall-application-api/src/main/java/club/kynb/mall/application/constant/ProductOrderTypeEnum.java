package club.kynb.mall.application.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @date: 2021/6/30
 */
@AllArgsConstructor
@Getter
public enum ProductOrderTypeEnum {

    SALES_DESC(1,"销量倒序"),
    PRICE_ASC(2,"价格正序"),
    PRICE_DESC(3,"价格倒序序"),
    ;

    private Integer type;

    private String desc;

    public static Optional<ProductOrderTypeEnum> get(Integer type){
        return Arrays.stream(ProductOrderTypeEnum.values())
                .filter(item -> item.getType().equals(type))
                .findFirst();
    }
}
