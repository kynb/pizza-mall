package club.kynb.mall.application.config;

import club.kynb.mall.application.constant.MallResultCode;
import org.pizza.common.web.response.CommonCode;
import org.pizza.properties.SwaggerGlobalResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.*;
import springfox.documentation.service.Response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/1/11 4:19 下午
 * 全局错误状态码
 */
@Configuration
public class MallKnife4jConfiguration implements SwaggerGlobalResponse {

    @Bean
    public SwaggerGlobalResponse swaggerGlobalResponse() {
        return new SwaggerGlobalResponse() {
            @Override
            public List<Response> get() {
                List<Response> responseMessageList = new ArrayList<>();
                Arrays.stream(CommonCode.values()).forEach(errorEnums -> {
                    responseMessageList.add(
                            new ResponseBuilder().code(String.valueOf(errorEnums.getCode())).description(errorEnums.getMsg()).build()
                    );
                });
                Arrays.stream(MallResultCode.values()).forEach(errorEnums -> {
                    responseMessageList.add(
                            new ResponseBuilder().code(String.valueOf(errorEnums.getCode())).description(errorEnums.getMsg()).build()
                );
                });
                return responseMessageList;
            }
        };
    }

}
