package club.kynb.mall.order.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;


/**
 * 购物车表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-06-30
 */

@Data
@ApiModel(value = "ShoppingCartDTO对象", description = "购物车表")
@Accessors(chain = true)
public class ShoppingCartDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", example = "")
    private Long userId;
    /**
     * 商品id
     */
    @ApiModelProperty(value = "商品id", example = "")
    private String spuId;
    /**
     * skuid
     */
    @ApiModelProperty(value = "skuid", example = "")
    private String skuId;
    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量", example = "")
    private Integer skuNum;
    /**
     * 是否选中 0否 1是
     */
    @ApiModelProperty(value = "是否选中 0否 1是", example = "")
    private Integer checked;
    /**
     * 分组展示依据  展示时按此字段进行分组（如按活动分组保存活动id）
     */
    @ApiModelProperty(value = "分组展示依据  展示时按此字段进行分组（如按活动分组、按供应商..）", example = "")
    private String groupKey;
    /**
     * 加购渠道来源 枚举值 APP_MALL
     */
    @ApiModelProperty(value = "加购渠道来源 枚举值 APP_MALL", example = "")
    private String addChannel;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
}
