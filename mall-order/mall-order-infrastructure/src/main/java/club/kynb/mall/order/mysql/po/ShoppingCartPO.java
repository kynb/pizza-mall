package club.kynb.mall.order.mysql.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.mybatis.plus.base.BaseEntity;


/**
 * 购物车表持久实体类
 * @author kynb_club@163.com
 * @since 2021-06-28
 */

@Data
@Accessors(chain = true)
@TableName("mall_shopping_cart")
@EqualsAndHashCode(callSuper = true)
public class ShoppingCartPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 商品id
     */
    private Long spuId;
    /**
     * skuid
     */
    private Long skuId;
    /**
     * 商品数量
     */
    private Integer skuNum;
    /**
     * 是否选中 0否 1是
     */
    private Integer checked;
    /**
     * 分组展示依据  展示时按此字段进行分组（如按活动分组保存活动id）
     */
    private String groupKey;
    /**
     * 加购渠道来源 枚举值 APP_MALL
     */
    private String addChannel;
    /**
     * 扩展字段
     */
    private String extension;


}
