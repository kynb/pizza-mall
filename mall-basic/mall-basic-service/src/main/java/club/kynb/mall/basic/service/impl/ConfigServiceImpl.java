package club.kynb.mall.basic.service.impl;

import club.kynb.mall.basic.api.IConfigService;
import club.kynb.mall.basic.constant.ConfigScope;
import club.kynb.mall.basic.dto.ConfigDTO;
import club.kynb.mall.basic.mysql.mapper.ConfigMapper;
import club.kynb.mall.basic.mysql.po.ConfigPO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.util.Convertor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/27 8:25 下午
 */
@Slf4j
@Service
@AllArgsConstructor
public class ConfigServiceImpl implements IConfigService {
    private final ConfigMapper configMapper;

    @Override
    public List<ConfigDTO> getByScope(ConfigScope scope) {
        final List<ConfigPO> poList = configMapper.selectList(new QueryWrapper<ConfigPO>().lambda().eq(ConfigPO::getScope, scope.name()));
        return Convertor.convert(poList, ConfigDTO.class);
    }

    @Override
    public Optional<ConfigDTO> getByScopeAndKey(ConfigScope scope, String configKey) {
        final Optional<ConfigPO> first = configMapper.selectList(new QueryWrapper<ConfigPO>().lambda().eq(ConfigPO::getScope, scope.name())).stream().findFirst();
        return first.map(configPO -> Convertor.convert(ConfigDTO.class, configPO));
    }
}
