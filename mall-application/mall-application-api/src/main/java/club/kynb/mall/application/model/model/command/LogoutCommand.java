package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

/**
 * @author kynb_club@163.com
 * @since 2020/12/28 9:00 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "LogoutCommand对象", description = "登出命令")
@Accessors(chain = true)
public class LogoutCommand extends Command {

    @ApiModelProperty(value = "token", example = "", required = true)
    private String token;

}
