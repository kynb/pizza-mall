package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 2:53 下午
 */
@Getter
@AllArgsConstructor
public enum CouponTemplateStatusEnum implements EnumValidI {

    INIT(0,"待生效"),
    PROCESSING(1,"进行中"),
    FINISH(2,"已完成"),
    FORBIDDEN(3,"已禁用"),
    ;

    private final Integer status;
    private final String desc;

    @Override
    public String code() {
        return String.valueOf(this.status);
    }

    @Override
    public String message() {
        return this.desc;
    }

    public static Optional<CouponTemplateStatusEnum> get(Integer status){
        return Arrays.stream(CouponTemplateStatusEnum.values())
                .filter(item-> item.getStatus().equals(status))
                .findFirst();
    }
}
