package club.kynb.mall.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.util.Date;

/**
 * 用户优惠券表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-06-28
 */

@Data
@ApiModel(value = "CouponreceiveUserDTO对象", description = "用户领取优惠券表")
@Accessors(chain = true)
public class CouponReceiveUserDTO implements DTO {
    private static final long serialVersionUID = 1L;


    /**
     * 优惠券ID
     */
    @ApiModelProperty(value = "优惠券ID", example = "")
    private Long couponTemplateId;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", example = "")
    private Long userId;
    /**
     * 领取时间
     */
    @ApiModelProperty(value = "领取时间", example = "")
    private Date receiveTime;
    /**
     * 状态：0未使用，1已使用，2已过期
     */
    @ApiModelProperty(value = "状态：0未使用，1已使用，2已过期", example = "")
    private Integer status;




}
