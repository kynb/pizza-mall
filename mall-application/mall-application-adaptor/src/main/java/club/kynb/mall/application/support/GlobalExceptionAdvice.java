package club.kynb.mall.application.support;

import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.exception.BizException;
import org.pizza.common.web.mvc.exception.ExceptionAdvice;
import org.pizza.common.web.response.R;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author kynb_club@163.com
 * @since 2020/11/19 1:51 下午
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionAdvice extends ExceptionAdvice {
    @Override
    public R bizException(BizException e) {
        //此处控制日志级别
        this.warn(e);
        return R.fail(e);
    }
}
