package club.kynb.mall.application.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


/**
 * 业务配置类
 *
 * @author kynb_club@163.com
 * @date:  2021/6/28
 */
@Configuration
public class BusinessConfig {
    /**
     * 商品详情相关商品返回最大数
     */
    @Value("${mall.product.detailRelatedNum:6}")
    private Integer detailRelatedNum;
    /**
     * 商品详情推荐商品返回最大数
     */
    @Value("${mall.product.detailRecommendNum:6}")
    private Integer detailRecommendNum;

    /**
     * 商品详情推荐商品返回最大数
     */
    @Value("${mall.product.searchMaxNum:50}")
    private Integer searchMaxNum;

    public Integer getDetailRelatedNum() {
        return detailRelatedNum;
    }

    public Integer getDetailRecommendNum() {
        return detailRecommendNum;
    }

    public Integer getSearchMaxNum() {
        return searchMaxNum;
    }
}
