package club.kynb.mall.order.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单详情表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "OrderDetailDTO对象", description = "订单详情表")
@Accessors(chain = true)
public class OrderDetailDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", example = "")
    private Long userId;
    /**
     * 内部订单编号
     */
    @ApiModelProperty(value = "内部订单编号", example = "")
    private String innerOrderNo;
    /**
     * 商品id
     */
    @ApiModelProperty(value = "商品id", example = "")
    private Long spuId;
    /**
     * skuid
     */
    @ApiModelProperty(value = "skuid", example = "")
    private Long skuId;
    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量", example = "")
    private Integer skuNum;
    /**
     * 退款数量
     */
    @ApiModelProperty(value = "退款数量", example = "")
    private Integer refundNum;
    /**
     * 原商品总金额
     */
    @ApiModelProperty(value = "原商品总金额", example = "")
    private BigDecimal totalAmount;
    /**
     * 分摊优惠后总金额
     */
    @ApiModelProperty(value = "分摊优惠后总金额", example = "")
    private BigDecimal shareAmount;
    /**
     * 下单时sku的快照信息
     */
    @ApiModelProperty(value = "下单时sku的快照信息", example = "")
    private String skuSnapshoot;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
