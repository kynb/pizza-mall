package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author kynb_club@163.com
 * @since 2020/12/28 9:00 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "ShippingAddressCreateCommand对象", description = "创建收货地址命令")
@Accessors(chain = true)
public class ShippingAddressCreateCommand extends ShippingAddressUpdateCommand {


}
