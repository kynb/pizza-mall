package club.kynb.mall.application.controller;

import club.kynb.mall.application.model.model.query.CategoryProductListQuery;
import club.kynb.mall.application.model.model.query.ProductQuery;
import club.kynb.mall.application.model.model.vo.CategoryProductListVO;
import club.kynb.mall.application.model.model.vo.ProductDetailResultVO;
import club.kynb.mall.application.service.ProductApplicationService;
import club.kynb.mall.product.dto.ProductCategoryDTO;
import club.kynb.mall.product.dto.ProductCategoryFullDTO;
import club.kynb.mall.product.dto.ProductSpuDetailDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.response.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 11:34 上午
 */
@Slf4j
@RestController
@RequestMapping("/product")
@AllArgsConstructor
@Api(value = "", tags = "商品相关")
public class ProductController {

    private final ProductApplicationService productService;

    @GetMapping("/listCategoryTop")
    @ApiOperation(value = "首页-分类列表", notes = "首页分类列表")
    public R<List<ProductCategoryDTO>> listCategoryTop() {
        return R.ok(productService.listCategoryTop());
    }

    @GetMapping("/listCategoryAll")
    @ApiOperation(value = "商品页-分类列表", notes = "商品页分类列表-两级分类同时返回")
    public R<List<ProductCategoryFullDTO>> listCategoryAll() {
        return R.ok(productService.listCategoryFull());
    }

    @GetMapping("/listProductByCategory")
    @ApiOperation(value = "分类页-商品列表", notes = "二级分类下商品列表")
    public R<CategoryProductListVO> listProductByCategory(@Valid CategoryProductListQuery query) {
        return R.ok(productService.productListByCategory(query));
    }

    @GetMapping("/detail/{id}")
    @ApiOperation(value = "商品-详情", notes = "商品详情")
    public R<ProductDetailResultVO> detailById(@PathVariable(value = "id")Long id) {
        return R.ok(productService.detailById(id));
    }

    @GetMapping("/search")
    @ApiOperation(value = "全局-商品搜索", notes = "商品搜索")
    public R<List<ProductSpuDetailDTO>> search(@Valid ProductQuery pageQuery) {
        return R.ok(productService.search(pageQuery));
    }
}
