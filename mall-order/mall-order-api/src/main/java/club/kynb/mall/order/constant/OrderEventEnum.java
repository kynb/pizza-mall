package club.kynb.mall.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

/**
 * 商城端订单事件
 *
 * @author kynb_club@163.com
 * @since 2021/7/8
 */
@Getter
@AllArgsConstructor
public enum OrderEventEnum implements EnumValidI {
    PREVIEW,            //订单预览
    CREATE,             //用户提交创建订单
    PAID,               //用户支付成功
    AUTO_CANCELLED,     //超时自动取消订单
    MANUAL_CANCELLED,   //用户手动取消订单
    AUTO_CONFIRM,       //超时自动取消订单
    MANUAL_CONFIRM,     //用户手动取消订单
    DELIVERED,          //仓库订单已发货
    RECEIVED,           //用户订单已收货
    EVALUATED,          //用户订单已评价
    UN_DELIVERY_APPLY_REFUND,       //用户申请退款
    UN_RECEIVE_APPLY_REFUND,       //用户申请退款
    UN_DELIVERY_REFUND_CANCEL, //用户取消申请退款
    UN_RECEIVE_REFUND_CANCEL, //用户取消申请退款
    AGREE_REFUND,       //运营同意退款
    CLOSED,             //限定时间订单关闭
    ;

    @Override
    public String code() {
        return this.name();
    }

    @Override
    public String message() {
        return this.name();
    }

}
