package club.kynb.mall.order.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单退款表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "OrderRefundDTO对象", description = "订单退款表")
@Accessors(chain = true)
public class OrderRefundDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", example = "")
    private Long userId;
    /**
     * 审核人用户ID
     */
    @ApiModelProperty(value = "审核人用户ID", example = "")
    private Long auditUserId;
    /**
     * 退款申请编号
     */
    @ApiModelProperty(value = "退款申请编号", example = "")
    private String refundNo;
    /**
     * 内部订单编号
     */
    @ApiModelProperty(value = "内部订单编号", example = "")
    private String innerOrderNo;
    /**
     * 退款申请前的订单状态 见订单状态枚举
     */
    @ApiModelProperty(value = "退款申请前的订单状态 见订单状态枚举", example = "")
    private String fromOrderStatus;
    /**
     * 退款状态  1审核通过 2审核失败 3进行中
     */
    @ApiModelProperty(value = "退款状态  1审核通过 2审核失败 3进行中", example = "")
    private Integer status;
    /**
     * 退款状态  1全部退款 2部分退款
     */
    @ApiModelProperty(value = "退款状态  1全部退款 2部分退款", example = "")
    private Integer type;
    /**
     * 退款原因描述
     */
    @ApiModelProperty(value = "退款原因描述", example = "")
    private String refundDesc;
    /**
     * 退款金额
     */
    @ApiModelProperty(value = "退款金额", example = "")
    private BigDecimal refundAmount;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息", example = "")
    private String remark;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
