package club.kynb.mall.user.mysql.mapper;

import club.kynb.mall.user.mysql.po.ShippingAddressPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * 收货地址表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2021-06-27
 */
@Mapper
public interface ShippingAddressMapper extends BaseMapper<ShippingAddressPO> {


}
