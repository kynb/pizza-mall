package club.kynb.mall.user.api;


import club.kynb.mall.user.model.dto.ShippingAddressDTO;

import java.util.List;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/27 10:51 上午
 */
public interface IShippingAddressService {
    List<ShippingAddressDTO> addressList(Long userId);
    void deleteAddress(Long addressId);
    void updateAddress(ShippingAddressDTO addressDTO);
    void createAddress(ShippingAddressDTO addressDTO);
    Optional<ShippingAddressDTO> getById(Long shippingAddressId);

}
