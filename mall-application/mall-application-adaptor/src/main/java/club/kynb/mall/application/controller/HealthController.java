package club.kynb.mall.application.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 11:34 上午
 */
@Slf4j
@RestController
@Api(value = "", tags = "健康检测", hidden = true)
public class HealthController {
    @GetMapping("/health")
    @ResponseBody
    public String health() {
        return "ok";
    }

    @GetMapping("/favicon.ico")
    @ResponseBody
    public String favicon() {
        return "ok";
    }
}
