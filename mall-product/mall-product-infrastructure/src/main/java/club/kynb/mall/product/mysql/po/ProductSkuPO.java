package club.kynb.mall.product.mysql.po;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import org.pizza.mybatis.plus.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 商品sku表持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_product_sku")
@EqualsAndHashCode(callSuper = true)
public class ProductSkuPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    private Long spuId;
    private String skuCode;
    private String skuOutCode;
    /**
     * 商品名称
     */
    private String skuName;
    /**
     * 商品价格
     */
    private BigDecimal skuPrice;
    /**
     * 计价单位
     */
    private String skuUnit;
    /**
     * 重量(g)
     */
    private BigDecimal skuWeight;
    private String skuImage;
    /**
     * 状态 0初始化 1上架 2下架
     */
    private Integer skuStatus;
    /**
     * 销售数量
     */
    private Integer skuSaleNum;
    /**
     * 排序
     */
    private Integer sequence;
    /**
     * 扩展字段
     */
    private String extension;


}
