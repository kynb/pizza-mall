package club.kynb.mall.application.model.model.event;


import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.pizza.event.model.DomainEvent;

/**
 * @author kynb_club@163.com
 * @since 2020/11/21 10:55 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class OrderCanceledEvent extends DomainEvent {
    /**
     * 内部订单流水号
     */
    private String innerOrderNo;

    @Override
    public boolean isStore() {
        return true;
    }
}


