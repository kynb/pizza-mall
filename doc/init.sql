DROP TABLE IF EXISTS `mall_administrator`;
CREATE TABLE `mall_administrator` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID(userId)',
  `real_name` varchar(20) NOT NULL COMMENT '真实姓名',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) unsigned DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) unsigned DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='管理员表';

DROP TABLE IF EXISTS `mall_config`;
CREATE TABLE `mall_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `scope` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '作用域枚举(MALL)',
  `config_desc` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '配置描述',
  `config_key` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '配置key',
  `config_value` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '配置value',
  `remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注信息',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='全局配置表';

DROP TABLE IF EXISTS `mall_coupon_template`;
CREATE TABLE `mall_coupon_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(100) NOT NULL COMMENT '券名字（外部显示）',
  `title` varchar(100) NOT NULL COMMENT '标题（外部显示）',
  `description` varchar(300) NOT NULL COMMENT '描述信息（外部显示）',
  `tag` varchar(100) NOT NULL COMMENT '标签（外部显示）',
  `coupon_value` decimal(18,2) NOT NULL COMMENT '面值（单位：抵扣券：元；折扣券：百分号前的数值，0.1-99.9）',
  `coupon_type` tinyint(4) NOT NULL COMMENT '券类型（1：现金券、2：折扣券）',
  `use_superimposed_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '叠加使用，0：不允许叠加，1：允许同类叠加',
  `receive_way` tinyint(4) NOT NULL COMMENT '领取方式，1：主动领取，2：系统发放',
  `release_quantity` int(11) DEFAULT '-1' COMMENT '发放数量，主动领取方式有效',
  `residue_quantity` int(11) DEFAULT '-1' COMMENT '剩余数量，主动领取方式有效',
  `receive_start_time` datetime DEFAULT NULL COMMENT '领取开始时间，主动领取方式有效',
  `receive_end_time` datetime DEFAULT NULL COMMENT '领取截止时间，主动领取方式有效',
  `effective_time` datetime DEFAULT NULL COMMENT '生效时间',
  `invalid_time` datetime DEFAULT NULL COMMENT '失效时间',
  `spu_range` tinyint(4) NOT NULL DEFAULT '2' COMMENT '商品使用范围（1：指定商品，2：全部商品）',
  `spu_ids` varchar(300) DEFAULT NULL COMMENT '限定的商品ID列表，指定商品时有效',
  `return_qualification` tinyint(4) DEFAULT NULL COMMENT '撤单返还 0：不返还，1：返还',
  `threshold_amount` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT '使用金额门槛，达到门槛，才能使用券',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态：0待生效，1进行中，2已结束，3禁用',
  `remark` varchar(300) DEFAULT NULL COMMENT '内部备注',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='优惠券模板表';

DROP TABLE IF EXISTS `mall_driver_delivery`;
CREATE TABLE `mall_driver_delivery` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '配送id',
  `warehouse_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
  `driver_id` bigint(20) DEFAULT NULL COMMENT '司机id',
  `order_ids` text COMMENT '订单id集合',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `clock_time` datetime DEFAULT NULL COMMENT '打卡时间',
  `clock_img` varchar(255) DEFAULT NULL COMMENT '打卡照片',
  `date_str` int(8) DEFAULT NULL COMMENT '日期 yyyyMMDD',
  `order_count` int(10) DEFAULT '0' COMMENT '订单数量',
  `amount` decimal(12,2) DEFAULT '0.00' COMMENT '总金额',
  `finish_count` int(10) DEFAULT '0' COMMENT '完成子订单数量',
  `status` tinyint(2) DEFAULT '1' COMMENT '状态1进行中2已完成',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='司机配送表';

DROP TABLE IF EXISTS `mall_order`;
CREATE TABLE `mall_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `outer_order_no` varchar(50) NOT NULL COMMENT '外部订单编号（显示）',
  `inner_order_no` varchar(50) NOT NULL COMMENT '内部订单编号',
  `order_status` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '订单状态 见订单状态枚举',
  `total_amount` decimal(24,2) DEFAULT '0.00' COMMENT '总金额',
  `payable_amount` decimal(24,2) DEFAULT '0.00' COMMENT '应付金额',
  `pay_amount` decimal(24,2) DEFAULT '0.00' COMMENT '实付金额',
  `freight_amount` decimal(24,2) DEFAULT '0.00' COMMENT '运费金额',
  `discount_amount` decimal(24,2) DEFAULT '0.00' COMMENT '优惠总金额',
  `coupon_amount` decimal(24,2) DEFAULT '0.00' COMMENT '优惠券总金额',
  `total_item_num` int(11) DEFAULT '0' COMMENT '订单商品总数',
  `user_coupon_id` bigint(20) DEFAULT NULL COMMENT '用户优惠券ID',
  `warehouse_id` bigint(20) DEFAULT NULL COMMENT '分发的仓库ID',
  `receipt_type` tinyint(2) unsigned DEFAULT '0' COMMENT '开票类型',
  `receipt_info` varchar(1000) DEFAULT '0' COMMENT '开票信息JSON',
  `place_time` datetime DEFAULT NULL COMMENT '下单时间',
  `auto_delivery_time` datetime DEFAULT NULL COMMENT '自动确认收货时间',
  `end_time` datetime DEFAULT NULL COMMENT '完成时间',
  `cancel_end_time` datetime DEFAULT NULL COMMENT '订单最后付款时间',
  `pay_no` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '支付流水号',
  `pay_channel` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '支付方式',
  `pay_create_time` datetime DEFAULT NULL COMMENT '支付开始时间',
  `pay_status` varchar(20) DEFAULT 'NO_PAY' COMMENT '支付状态',
  `pay_finish_time` datetime DEFAULT NULL COMMENT '支付完成时间',
  `deposit_pay_time` datetime DEFAULT NULL COMMENT '订单支付时间',
  `cancel_time` datetime DEFAULT NULL COMMENT '订单取消时间',
  `cancel_type` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '取消订单类型',
  `cancel_desc` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '取消原因',
  `delivery_type` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '物流方式',
  `delivery_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '收货人名称',
  `delivery_mobile` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '收货人手机号',
  `delivery_address` varchar(256) CHARACTER SET utf8 DEFAULT NULL COMMENT '收货地址信息',
  `confirm_delivery_time` datetime DEFAULT NULL COMMENT '确认发货时间',
  `expect_arrive_time` datetime DEFAULT NULL COMMENT '期望送达时间',
  `actual_arrive_time` datetime DEFAULT NULL COMMENT '实际送达时间',
  `lng` double DEFAULT NULL COMMENT '经度',
  `lat` double DEFAULT NULL COMMENT '纬度',
  `user_remark` varchar(300) DEFAULT NULL COMMENT '用户备注信息',
  `inner_remark` varchar(300) DEFAULT NULL COMMENT '内部备注信息',
  `sync_wms` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否同步进销存系统0否1是',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单表';

DROP TABLE IF EXISTS `mall_order_detail`;
CREATE TABLE `mall_order_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `inner_order_no` varchar(50) NOT NULL COMMENT '内部订单编号',
  `spu_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `sku_id` bigint(20) DEFAULT NULL COMMENT 'skuid',
  `sku_num` int(11) NOT NULL COMMENT '商品数量',
  `refund_num` int(11) DEFAULT '0' COMMENT '退款数量',
  `total_amount` decimal(24,4) DEFAULT '0.0000' COMMENT '原商品总金额',
  `share_amount` decimal(24,4) DEFAULT '0.0000' COMMENT '分摊优惠后总金额',
  `sku_snapshoot` varchar(2000) DEFAULT NULL COMMENT '下单时sku的快照信息',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='订单详情表';

DROP TABLE IF EXISTS `mall_order_refund`;
CREATE TABLE `mall_order_refund` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `audit_user_id` bigint(20) DEFAULT NULL COMMENT '审核人用户ID',
  `refund_no` varchar(50) NOT NULL COMMENT '退款申请编号',
  `inner_order_no` varchar(50) NOT NULL COMMENT '内部订单编号',
  `from_order_status` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '退款申请前的订单状态 见订单状态枚举',
  `status` tinyint(2) NOT NULL COMMENT '退款状态  1审核通过 2审核失败 3进行中',
  `type` tinyint(2) NOT NULL COMMENT '退款状态  1全部退款 2部分退款',
  `refund_desc` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '退款原因描述',
  `refund_amount` decimal(24,4) NOT NULL DEFAULT '0.0000' COMMENT '退款金额',
  `remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注信息',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单退款表';

DROP TABLE IF EXISTS `mall_order_refund_detail`;
CREATE TABLE `mall_order_refund_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `refund_no` varchar(50) NOT NULL COMMENT '退款申请编号',
  `order_detail_id` bigint(20) NOT NULL COMMENT '订单详情id',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '数量',
  `amount` decimal(12,2) DEFAULT NULL COMMENT '退款金额',
  `remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注信息',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='订单退款详情表';

DROP TABLE IF EXISTS `mall_pay_channel`;
CREATE TABLE `mall_pay_channel` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `channel_code` varchar(10) NOT NULL COMMENT '支付方式code',
  `channel_name` varchar(10) NOT NULL COMMENT '支付方式名称',
  `channel_icon` varchar(255) DEFAULT NULL COMMENT '支付方式图标',
  `open_status` smallint(1) NOT NULL DEFAULT '0' COMMENT '开启状态',
  `client_types` varchar(20) NOT NULL COMMENT '客户端类型',
  `order_types` varchar(100) DEFAULT NULL COMMENT '支持订单类型',
  `sequence` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `channel_desc` varchar(200) NOT NULL COMMENT '支付方式说明',
  PRIMARY KEY (`id`),
  UNIQUE KEY `channel_code_uqidx` (`channel_code`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `mall_pay_trade_order`;
CREATE TABLE `mall_pay_trade_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `origin_order_type` varchar(20) NOT NULL COMMENT '原始订单类型',
  `origin_order_no` varchar(64) NOT NULL COMMENT '原始订单编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `trade_order_no` varchar(64) NOT NULL COMMENT '交易单流水',
  `order_amount` decimal(10,2) NOT NULL COMMENT '订单金额',
  `status` smallint(3) NOT NULL COMMENT '状态',
  `pay_channel` varchar(64) NOT NULL COMMENT '支付方式',
  `client_type` varchar(10) NOT NULL COMMENT '客户端类型',
  `pay_amount` decimal(10,2) DEFAULT NULL COMMENT '实付金额',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `pay_flow_no` varchar(100) DEFAULT NULL COMMENT '对接系统流水号',
  `biz_params` varchar(1000) DEFAULT NULL COMMENT '业务参数',
  `notify_info` varchar(1000) DEFAULT NULL COMMENT '通知信息',
  `extension` varchar(1000) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pay_td_ord_idx2` (`trade_order_no`) COMMENT '支付流水号唯一',
  KEY `pay_trd_ord_idx1` (`origin_order_type`,`origin_order_no`) COMMENT '订单类型+订单编号索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='支付交易订单表';

DROP TABLE IF EXISTS `mall_product_category`;
CREATE TABLE `mall_product_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '父ID',
  `cate_name` varchar(20) NOT NULL COMMENT '分类名称',
  `image_url` varchar(300) DEFAULT NULL COMMENT '图片地址',
  `icon_url` varchar(300) DEFAULT NULL COMMENT '图标',
  `sequence` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='商品分类表';

DROP TABLE IF EXISTS `mall_product_sku`;
CREATE TABLE `mall_product_sku` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `spu_id` bigint(20) unsigned NOT NULL,
  `sku_code` varchar(100) NOT NULL DEFAULT '',
  `sku_out_code` varchar(100) DEFAULT NULL,
  `sku_name` varchar(50) DEFAULT NULL COMMENT '商品名称',
  `sku_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `sku_unit` varchar(50) DEFAULT NULL COMMENT '计价单位',
  `sku_weight` decimal(10,2) DEFAULT NULL COMMENT '重量(g)',
  `sku_image` varchar(300) DEFAULT NULL,
  `sku_status` smallint(1) NOT NULL DEFAULT '0' COMMENT '状态 0初始化 1上架 2下架',
  `sku_sale_num` int(11) NOT NULL DEFAULT '0' COMMENT '销售数量',
  `sequence` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `product_spu_id_index` (`spu_id`),
  KEY `spu_id_and_sku_status_index` (`spu_id`,`sku_status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品sku表';

DROP TABLE IF EXISTS `mall_product_spu`;
CREATE TABLE `mall_product_spu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_ids` varchar(255) NOT NULL COMMENT '二级分类',
  `spu_code` varchar(100) DEFAULT NULL COMMENT '商品编码',
  `spu_out_code` varchar(100) DEFAULT NULL COMMENT '外部编码',
  `spu_name` varchar(50) DEFAULT NULL COMMENT '名称',
  `spu_keywords` varchar(100) DEFAULT NULL COMMENT '关键词',
  `spu_price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `spu_desc` text COMMENT '详情',
  `spu_imgs` varchar(1000) DEFAULT NULL COMMENT '图片',
  `spu_on_sale` smallint(1) NOT NULL DEFAULT '0' COMMENT '上架状态',
  `off_sale_time` datetime DEFAULT NULL COMMENT '下架时间',
  `spu_sale_num` int(11) NOT NULL DEFAULT '0' COMMENT '销售数量',
  `sequence` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `category_id_idx` (`category_ids`) USING BTREE,
  KEY `spu_code_idx` (`spu_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品信息表';

DROP TABLE IF EXISTS `mall_shipping_address`;
CREATE TABLE `mall_shipping_address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT '用户id',
  `landmark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '' COMMENT '地标名',
  `house_no` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '门牌号',
  `address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '收货地址(地图上定位的地址)',
  `contact` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '联系人',
  `phone` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '电话',
  `tag` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '标签',
  `longitude` double DEFAULT NULL COMMENT '经度',
  `latitude` double DEFAULT NULL COMMENT '纬度',
  `defaulted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认0否1是',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='收货地址表';

DROP TABLE IF EXISTS `mall_shopping_cart`;
CREATE TABLE `mall_shopping_cart` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `spu_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `sku_id` bigint(20) DEFAULT NULL COMMENT 'skuid',
  `sku_num` int(11) NOT NULL COMMENT '商品数量',
  `checked` tinyint(4) NOT NULL COMMENT '是否选中 0否 1是',
  `group_key` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '分组展示依据  展示时按此字段进行分组（如按活动分组保存活动id）',
  `add_channel` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '加购渠道来源 枚举值 APP_MALL',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `unique` (`user_id`,`spu_id`,`sku_id`) USING BTREE COMMENT '用户+spu+sku唯一索引',
  KEY `user_id_index` (`user_id`) USING BTREE COMMENT 'userId',
  KEY `sku_id_index` (`sku_id`) USING BTREE COMMENT 'skuId'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='购物车表';

DROP TABLE IF EXISTS `mall_user`;
CREATE TABLE `mall_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID(userId)',
  `phone` varchar(20) NOT NULL COMMENT '电话',
  `nickname` varchar(20) NOT NULL COMMENT '昵称',
  `password` varchar(32) NOT NULL COMMENT '密码MD5(密码+盐)',
  `salt` varchar(100) NOT NULL COMMENT '盐',
  `status` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '状态枚举(FREEZE、NORMAL、DESTROY)',
  `type` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '类型枚举(CUSTOMER、ADMINISTRATOR)',
  `current_token` varchar(300) DEFAULT NULL COMMENT '当前token',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) unsigned DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) unsigned DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `unique_phone_type` (`phone`,`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户基础信息表';

DROP TABLE IF EXISTS `mall_user_coupon`;
CREATE TABLE `mall_user_coupon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `coupon_template_id` bigint(20) NOT NULL COMMENT '优惠券ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `receive_time` datetime DEFAULT NULL COMMENT '领取时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态：0未使用，1已使用，2已过期',
  `extension` varchar(300) DEFAULT NULL COMMENT '扩展字段',
  `deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除0否1是',
  `create_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建用户',
  `update_user` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新用户',
  `tenant_id` bigint(20) unsigned DEFAULT '0' COMMENT '租户ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='用户优惠券表';



