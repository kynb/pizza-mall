package club.kynb.mall.application.model.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.VO;


/**
 * @author kynb_club@163.com
 * @since 2021/6/20 4:05 下午
 */
@Data
@ApiModel(value = "RegisterResult对象", description = "注册结果对象")
@Accessors(chain = true)
public class RegisterResultVO implements VO {
    @ApiModelProperty(value = "用户ID", example = "1")
    private String userId;

}
