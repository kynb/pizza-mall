package club.kynb.mall.payment.mysql.mapper;

import club.kynb.mall.payment.mysql.po.PayTradeOrderPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 支付交易订单表 Mapper 接口
 *
 * @author fyh
 * @since 2021-06-29
 */
@Mapper
public interface PayTradeOrderMapper extends BaseMapper<PayTradeOrderPO> {

    @Select("select * from mall_pay_trade_order where trade_order_no = #{tradeOrderNo}")
    PayTradeOrderPO getByOrderNo(@Param("tradeOrderNo")String tradeOrderNo);
}
