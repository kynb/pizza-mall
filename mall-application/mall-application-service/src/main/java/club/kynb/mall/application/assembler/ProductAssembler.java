package club.kynb.mall.application.assembler;

import club.kynb.mall.product.dto.ProductSkuDTO;
import club.kynb.mall.product.dto.ProductSpuDetailDTO;
import cn.hutool.core.collection.CollectionUtil;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.exception.Errors;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 商品过滤处理类
 *
 * @author kynb_club@163.com
 * @date: 2021/6/30
 */
@Slf4j
public class ProductAssembler {



    public static List<ProductSpuDetailDTO> skuFilter(List<ProductSpuDetailDTO> levelFilteredList, List<Long> skuIdList) {
        return levelFilteredList.stream().peek(productSpuDTO -> {
            final List<ProductSkuDTO> filteredList = productSpuDTO.getSkuList()
                    .stream()
                    .filter(productSkuDTO -> skuIdList.contains(Long.valueOf(productSkuDTO.getId())))
                    .collect(Collectors.toList());
            if (CollectionUtil.isEmpty(filteredList)) {
                log.error("购物车内skuId与商品匹配失败：{}", skuIdList);
                throw Errors.SYSTEM.exception("系统异常，过滤sku失败");
            }
            productSpuDTO.setSkuList(filteredList);
        }).collect(Collectors.toList());
    }
}
