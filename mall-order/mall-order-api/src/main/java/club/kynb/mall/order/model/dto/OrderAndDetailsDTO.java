package club.kynb.mall.order.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 订单表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-07-07
 */

@Data
@ApiModel(value = "OrderAndDetailsDTO对象", description = "订单表")
@Accessors(chain = true)
public class OrderAndDetailsDTO extends OrderDTO {
    private static final long serialVersionUID = 1L;

    /**
     * 订单明细
     */
    @ApiModelProperty(value = "订单明细", example = "")
    private List<OrderDetailDTO> details;

}
