package club.kynb.mall.order.mysql.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.mybatis.plus.base.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 订单表持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_order")
@EqualsAndHashCode(callSuper = true)
public class OrderPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 外部订单编号（显示）
     */
    private String outerOrderNo;
    /**
     * 内部订单编号
     */
    private String innerOrderNo;
    /**
     * 订单状态 见订单状态枚举
     */
    private String orderStatus;
    /**
     * 总金额
     */
    private BigDecimal totalAmount;
    /**
     * 应付金额
     */
    private BigDecimal payableAmount;
    /**
     * 实付金额
     */
    private BigDecimal payAmount;
    /**
     * 运费金额
     */
    private BigDecimal freightAmount;
    /**
     * 优惠总金额
     */
    private BigDecimal discountAmount;
    /**
     * 优惠券总金额
     */
    private BigDecimal couponAmount;
    /**
     * 订单商品总数
     */
    private Integer totalItemNum;
    /**
     * 用户优惠券ID
     */
    private Long userCouponId;
    /**
     * 下单时间
     */
    private Date placeTime;
    /**
     * 自动确认收货时间
     */
    private Date autoDeliveryTime;
    /**
     * 完成时间
     */
    private Date endTime;
    /**
     * 订单最后付款时间
     */
    private Date cancelEndTime;
    /**
     * 支付流水号
     */
    private String payNo;
    /**
     * 支付方式
     */
    private String payChannel;
    /**
     * 支付开始时间
     */
    private Date payCreateTime;
    /**
     * 支付状态
     */
    private String payStatus;
    /**
     * 支付完成时间
     */
    private Date payFinishTime;
    /**
     * 订单支付时间
     */
    private Date depositPayTime;
    /**
     * 订单取消时间
     */
    private Date cancelTime;
    /**
     * 取消订单类型
     */
    private String cancelType;
    /**
     * 取消原因
     */
    private String cancelDesc;
    /**
     * 物流方式
     */
    private String deliveryType;
    /**
     * 收货人名称
     */
    private String deliveryName;
    /**
     * 收货人手机号
     */
    private String deliveryMobile;
    /**
     * 收货地址信息
     */
    private String deliveryAddress;
    /**
     * 确认发货时间
     */
    private Date confirmDeliveryTime;
    /**
     * 实际送达时间
     */
    private Date actualArriveTime;
    /**
     * 经度
     */
    private Double lng;
    /**
     * 纬度
     */
    private Double lat;
    /**
     * 用户备注信息
     */
    private String userRemark;
    /**
     * 内部备注信息
     */
    private String innerRemark;
    /**
     * 是否同步进销存系统0否1是
     */
    private Boolean syncWms;
    /**
     * 扩展字段
     */
    private String extension;


}
