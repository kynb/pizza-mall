package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;


/**
 * @author kynb_club@163.com
 * @since 2020/12/18 11:45 上午
 */
@Data
@ApiModel(value = "TokenDTO对象", description = "令牌")
@Accessors(chain = true)
public class TokenDTO implements DTO {

    @ApiModelProperty(value = "令牌", example = "1256")
    private String accessToken;

    @ApiModelProperty(value = "颁发时间时间戳", example = "")
    private Long issue;

    @ApiModelProperty(value = "token的过期时间(单位分钟)", example = "18000")
    private Integer tokenExpire;

}
