package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author kynb_club@163.com
 * @since 2020/12/18 9:35 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "PwdAuthenticationCommand对象", description = "密码认证登录命令")
@Accessors(chain = true)
public class PwdAuthenticationCommand extends AuthenticationCommand {

    @NotBlank(message = "密码不能为空")
    @Length(message = "密码最少为6个字符，最长为20个字符", min = 6, max = 20)
    @ApiModelProperty(value = "密码", example = "123456", required = true)
    private String password;

    @Length(message = "图形验证key长度有误", min = 10, max = 32)
    @ApiModelProperty(value = "图形验证key(开启图形验证码时必传)", example = "1234-1234-1234-1234")
    private String captchaKey;

    @Length(message = "图形验证码最少为4个字符", min = 4, max = 4)
    @ApiModelProperty(value = "图形验证码(开启图形验证码时必传)", example = "1234")
    private String pictureVerificationCode;


}
