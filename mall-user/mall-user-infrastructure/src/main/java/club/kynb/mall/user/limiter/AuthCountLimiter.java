package club.kynb.mall.user.limiter;

import org.pizza.limiter.CountLimiter;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author kynb_club@163.com
 * @since 2021/7/18 1:26 下午
 */
@Component
public class AuthCountLimiter implements CountLimiter {
    @Override
    public boolean isExceedCount(String s, int i) {
        return false;
    }

    @Override
    public long lockTime(String s) {
        return 0;
    }

    @Override
    public void increment(String s, int i, long l, long l1, TimeUnit timeUnit) {

    }

    @Override
    public void clean(String s) {

    }
}
