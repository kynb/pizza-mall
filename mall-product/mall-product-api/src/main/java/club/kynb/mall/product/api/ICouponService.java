package club.kynb.mall.product.api;

import club.kynb.mall.product.dto.*;
import org.pizza.model.page.PageResult;

import java.util.List;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/28 10:18 下午
 */
public interface ICouponService {

    List<CouponTemplateDTO> drawCouponTemplateList();

    List<CouponTemplateDTO> allCouponTemplateList();

    List<UserCouponDTO> userCouponList(Long userId, Integer status);

    int userCouponNum(Long userId, Integer status);

    void receiveCoupon(Long userId, Long couponTemplateId);

    void useCoupon(Long id);

    void backCoupon(Long userId, Long userCouponId);


    /**
     * 优惠券列表
     * @param dto
     * @return
     */
    PageResult<CouponTemplateDTO> pageCouponTemplate(CouponTemplateParamDTO dto);

    /**
     * 创建优惠券
     * @param dto
     */
    void createCouponTemplate(CouponTemplateDTO dto);

    /**
     * 修改优惠券
     * @param dto
     */
    void updateCouponTemplate(CouponTemplateDTO dto);

    /**
     *  删除优惠券
     * @param id
     */
    void deleteCouponTemplate(Long id);

    /**
     * 优惠券详情
     * @param id
     * @return
     */
    Optional<CouponTemplateDTO> couponTemplatedetailById(Long id);

    /**
     * 优惠券领取用户列表
     * @param dto
     * @return
     */
    PageResult<CouponReceiveUserDTO> pageCoupoUser(CouponUserParamDTO dto);

}
