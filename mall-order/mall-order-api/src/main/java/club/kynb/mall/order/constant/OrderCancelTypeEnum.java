package club.kynb.mall.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

/**
 * 订单取消类型
 * @author kynb_club@163.com
 * @since  2021/7/8
 */
@Getter
@AllArgsConstructor
public enum OrderCancelTypeEnum implements EnumValidI {
    AUTO_CANCELLED("超时自动取消订单"),     //超时自动取消订单
    MANUAL_CANCELLED("用户手动取消订单"),   //用户手动取消订单
    WAREHOUSE_REJECT("仓库拒绝取消订单"),   //仓库拒绝取消订单
    ;
    private final String desc;
    @Override
    public String code() {
        return this.name();
    }

    @Override
    public String message() {
        return this.name();
    }


}
