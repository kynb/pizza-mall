package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Pattern;

/**
 * @author kynb_club@163.com
 * @since 2020/12/18 9:35 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "SmsFindPasswordCommand对象", description = "短信找回密码命令")
@Accessors(chain = true)
public class SmsFindPasswordCommand extends SmsAuthenticationCommand {

    @Pattern(regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$", message = "密码需要6~20数字字母组合")
    @ApiModelProperty(value = "新密码", example = "Ab1234aa11111", required = true)
    private String newPassword;

}
