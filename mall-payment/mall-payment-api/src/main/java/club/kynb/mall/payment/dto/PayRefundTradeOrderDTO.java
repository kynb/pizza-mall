package club.kynb.mall.payment.dto;

import club.kynb.mall.payment.constant.PayOrderTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;
import org.pizza.valid.EnumValid;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 退款交易流水表数据传输实体类
 *
 * @author fyh
 * @since 2021-07-04
 */

@Data
@ApiModel(value = "PayRefundTradeOrderDTO对象", description = "退款交易流水表")
@Accessors(chain = true)
public class PayRefundTradeOrderDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ApiModelProperty(value = "", example = "")
    private Long id;
    /**
     * 订单类型：PURCHASE, BILL
     */
    @ApiModelProperty(value = "订单类型： PURCHASE：采购订单, BILL：账单订单", example = "")
    @EnumValid(name = "订单类型",target = PayOrderTypeEnum.class)
    private String originOrderType;
    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号", example = "")
    private String originOrderNo;
    /**
     * 用户
     */
    @ApiModelProperty(value = "用户", example = "")
    private Long userId;
    /**
     * 原支付订单号
     */
    @ApiModelProperty(value = "原支付订单号", example = "")
    private String payTradeOrderNo;
    /**
     * 退款交易订单号
     */
    @ApiModelProperty(value = "退款交易订单号", example = "")
    private String tradeOrderNo;
    /**
     * 退款金额
     */
    @ApiModelProperty(value = "退款金额", example = "")
    private BigDecimal refundAmount;
    /**
     * 状态：0 待处理； 1 成功；2 失败
     */
    @ApiModelProperty(value = "状态：0 待处理； 1 成功；2 失败", example = "")
    private Integer status;
    /**
     * 退款时间（第三方）
     */
    @ApiModelProperty(value = "退款时间（第三方）", example = "")
    private Date refundTime;
    /**
     * 退款流水号（第三方）
     */
    @ApiModelProperty(value = "退款流水号（第三方）", example = "")
    private String refundFlowNo;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
