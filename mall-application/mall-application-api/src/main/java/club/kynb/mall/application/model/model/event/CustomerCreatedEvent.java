package club.kynb.mall.application.model.model.event;


import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.pizza.event.model.DomainEvent;

/**
 * @author kynb_club@163.com
 * @since 2020/11/21 10:55 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class CustomerCreatedEvent extends DomainEvent {
    /**
     * 成员ID
     */
    private Long userId;
    /**
     * 注册客户端IP
     */
    private String clientIp;
    /**
     * 客户端类型
     */
    private String clientType;


    @Override
    public boolean isStore() {
        return true;
    }
}


