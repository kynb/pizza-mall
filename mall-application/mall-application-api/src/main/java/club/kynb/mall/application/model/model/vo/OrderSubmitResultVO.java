package club.kynb.mall.application.model.model.vo;

import club.kynb.mall.application.model.model.dto.PreviewOrderSpuDTO;
import club.kynb.mall.order.constant.OrderSubmitStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.VO;
import org.pizza.valid.EnumValid;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/7/5 9:12 下午
 */
@Data
@ApiModel(value = "OrderSubmitResult对象", description = "订单提交结果")
@Accessors(chain = true)
public class OrderSubmitResultVO implements VO {

    @ApiModelProperty(value = "内部订单编号", example = "454158125549748224")
    private String innerOrderNo;

    @ApiModelProperty(value = "订单提交状态 1 SUCCESS 2FAILED", example = " 1")
    @EnumValid(name = "订单提交状态枚举", target = OrderSubmitStatusEnum.class)
    private Integer submitStatus;
    /**
     * 实付金额
     */
    @ApiModelProperty(value = "实付金额", example = "")
    private BigDecimal payAmount;
    /**
     * 订单最后付款时间
     */
    @ApiModelProperty(value = "订单最后付款时间", example = "")
    private Date cancelEndTime;


    @ApiModelProperty(value = "提交失败的原因", example = "")
    private String failureReason="";
    /**
     * 库存不足的商品信息
     */
    @ApiModelProperty(value = "库存不足的商品列表", example = "")
    private List<PreviewOrderSpuDTO> stockLessList = new ArrayList<>();
}
