package club.kynb.mall.application.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


/**
 * @author kynb_club@163.com
 * @since 2020/12/18 2:28 下午
 */
@Data
@Configuration
public class OssConfig {
    @Value("${spring.cloud.alicloud.bucket}")
    private String bucket;
    @Value("${spring.cloud.alicloud.oss.endpoint}")
    private String endpoint;
    @Value("${spring.cloud.alicloud.expire-time}")
    private Long expireTime;
    @Value("${spring.cloud.alicloud.access-key}")
    private String accessKey;
    @Value("${spring.cloud.alicloud.secret-key}")
    private String accessSecret;
    @Value("${spring.cloud.alicloud.host}")
    private String host;
    @Value("${spring.cloud.alicloud.region-id}")
    private String regionId;
    @Value("${spring.cloud.alicloud.role-arn}")
    private String roleArn;
    @Value("${spring.cloud.alicloud.role-session-name-prefix}")
    private String roleSessionNamePrefix;
    @Value("${spring.cloud.alicloud.temp-path}")
    private String tempPath;



}
