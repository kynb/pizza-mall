package club.kynb.mall.product.dto;


import club.kynb.mall.product.constant.SpuStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Data
@ApiModel(value = "商品列表搜索参数", description = "商品列表搜索参数")
@Accessors(chain = true)
public class ProductByParamDTO implements DTO {

    @ApiModelProperty(value = "id列表",example = "")
    private List<Long> ids ;
    @ApiModelProperty(value = "二级分类id",example = "")
    private Long cateId;
    @ApiModelProperty(value = "关键词",example = "")
    private String keyword;
    @ApiModelProperty(value = "状态",example = "")
    private SpuStatusEnum statusEnum;


}
