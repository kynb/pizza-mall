package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 优惠券表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-06-28
 */

@Data
@ApiModel(value = "UserCouponTemplateDTO对象", description = "用户优惠券模板状态对象")
@Accessors(chain = true)
public class UserCouponTemplateDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "1")
    private Long id;
    /**
     * 券名字（外部显示）
     */
    @ApiModelProperty(value = "券名字（外部显示）", example = "平台通用优惠券")
    private String name;
    /**
     * 标题（外部显示）
     */
    @ApiModelProperty(value = "标题（外部显示）", example = "满58减18")
    private String title;
    /**
     * 描述（外部显示）
     */
    @ApiModelProperty(value = "描述（外部显示）", example = "全部商品可用")
    private String description;
    /**
     * 标签（外部显示）
     */
    @ApiModelProperty(value = "标签（外部显示）", example = "平台券")
    private String tag;
    /**
     * 面值（单位：抵扣券：元；折扣券：百分号前的数值，0.1-99.9）
     */
    @ApiModelProperty(value = "面值（单位：抵扣券：元；折扣券：百分号前的数值，0.1-99.9）", example = "18")
    private BigDecimal couponValue;
    /**
     * 券类型（1：现金券、2：折扣券）
     */
    @ApiModelProperty(value = "券类型（1：现金券、2：折扣券）", example = "1")
    private Integer couponType;
    /**
     * 叠加使用，0：不允许叠加，1：允许同类叠加
     */
    @ApiModelProperty(value = "叠加使用，0：不允许叠加，1：允许同类叠加", example = "0")
    private Integer useSuperimposedType;
    /**
     * 领取方式，1：主动领取，2：系统发放
     */
    @ApiModelProperty(value = "领取方式，1：主动领取，2：系统发放", example = "1")
    private Integer receiveWay;
    /**
     * 发放数量，主动领取方式有效
     */
    @ApiModelProperty(value = "发放数量，主动领取方式有效", example = "100")
    private Integer releaseQuantity;
    /**
     * 剩余数量，主动领取方式有效
     */
    @ApiModelProperty(value = "剩余数量，主动领取方式有效", example = "89")
    private Integer residueQuantity;
    /**
     * 剩余百分比
     */
    @ApiModelProperty(value = "剩余百分比", example = "0.89")
    private BigDecimal residueRatio;
    /**
     * 领取开始时间，主动领取方式有效
     */
    @ApiModelProperty(value = "领取开始时间，主动领取方式有效", example = "2021-05-28 08:00:00")
    private Date receiveStartTime;
    /**
     * 领取截止时间，主动领取方式有效
     */
    @ApiModelProperty(value = "领取截止时间，主动领取方式有效", example = "2021-06-28 08:00:00")
    private Date receiveEndTime;
    /**
     * 生效时间
     */
    @ApiModelProperty(value = "生效时间", example = "2021-05-28 08:00:00")
    private Date effectiveTime;
    /**
     * 失效时间
     */
    @ApiModelProperty(value = "失效时间", example = "2021-07-28 08:00:00")
    private Date invalidTime;
    /**
     * 商品使用范围（1：指定商品，2：指定商品）
     */
    @ApiModelProperty(value = "商品使用范围（1：指定商品，2：指定商品）", example = "2")
    private Integer spuRange;
    /**
     * 指定商品的spuId列表,指定商品时有效
     */
    private String spuIds;
    /**
     * 撤单返还 0：不返还，1：返还
     */
    @ApiModelProperty(value = "撤单返还 0：不返还，1：返还", example = "1")
    private Integer returnQualification;
    /**
     * 使用金额门槛，达到门槛，才能使用券
     */
    @ApiModelProperty(value = "使用金额门槛，达到门槛，才能使用券", example = "58")
    private BigDecimal thresholdAmount;
    /**
     * 状态：0待生效，1进行中，2已结束，3禁用
     */
    @ApiModelProperty(value = "状态：0待生效，1进行中，2已结束，3禁用", example = "1")
    private Integer status;

    @ApiModelProperty(value = "当前用户是否领取 0否1是", example = "0")
    private Integer isDraw;

}
