package club.kynb.mall.payment.exec;

import org.pizza.common.web.exception.Errors;
import org.pizza.util.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author kynb_club@163.com
 * @date: 2021/7/7
 */
@Component
public class PayExecutorFactory {

    @Autowired
    private List<PayExecutorService> executors;

    private Map<String, PayExecutorService> orderServiceMap = new HashMap<>();

    @PostConstruct
    private void init() {
        executors.forEach(bean -> orderServiceMap.put(bean.getChannel(), bean));
    }
    /**
     * 获取支付执行服务
     *
     * @param type
     * @return
     */
    public PayExecutorService getService(String type) {
        PayExecutorService service = orderServiceMap.get(type);
        Checker.ifThrow(Objects.isNull(service), () -> Errors.BIZ.exception("付款功能开小差了，请联系客服"));
        return service;
    }
}
