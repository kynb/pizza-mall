package club.kynb.mall.payment.constant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Optional;
import java.util.stream.Stream;

/**
* Description: 	支付通道
* @author 
* @version 1.0.0 2020年8月25日 上午10:17:39
*/
@Getter
@AllArgsConstructor
public enum PayChannelEnum implements EnumValidI {

	WECHAT("WECHAT", "微信支付"),
	ALIPAY("ALIPAY", "支付宝支付"),
	;
	
	private final String code;
	private final String desc;

	@Override
	public String code() {
		return this.code;
	}

	@Override
	public String message() {
		return this.desc;
	}

	@JsonIgnore
	public static Optional<PayChannelEnum> get(String code) {
		return Stream.of(PayChannelEnum.values()).filter(i -> i.getCode().equalsIgnoreCase(code)).findFirst();
	}

}
