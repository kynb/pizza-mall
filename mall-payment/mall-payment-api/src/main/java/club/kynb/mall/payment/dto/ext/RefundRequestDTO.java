package club.kynb.mall.payment.dto.ext;


import club.kynb.mall.payment.constant.PayOrderTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;

/**
 * 退款请求对象
 *
 * @author kynb_club@163.com
 * @date:  2021/7/2
 */
@Data
@ApiModel(value = "PayRefundDTO对象", description = "退款请求信息")
@Accessors(chain = true)
public class RefundRequestDTO implements DTO {

	@ApiModelProperty(value = "订单类型", example = "")
	private PayOrderTypeEnum orderType;
	/**
	 * 	订单编号
	 */
	@ApiModelProperty(value = "订单号", example = "")
	private String orderNo;
	/**
	 * 	退款金额
	 */
	@ApiModelProperty(value = "退款金额", example = "")
	private BigDecimal refundAmount;
	

}
