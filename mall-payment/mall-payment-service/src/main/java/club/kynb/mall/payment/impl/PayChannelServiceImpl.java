package club.kynb.mall.payment.impl;


import club.kynb.mall.payment.api.IPayChannelService;
import club.kynb.mall.payment.constant.PayChannelOpenStatusEnum;
import club.kynb.mall.payment.constant.PayClientTypeEnum;
import club.kynb.mall.payment.constant.PayOrderTypeEnum;
import club.kynb.mall.payment.convert.PaymentConvertor;
import club.kynb.mall.payment.dto.PayChannelDTO;
import club.kynb.mall.payment.mysql.mapper.PayChannelMapper;
import club.kynb.mall.payment.mysql.po.PayChannelPO;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @date: 2021/7/1
 */
@Slf4j
@Service
@AllArgsConstructor
public class PayChannelServiceImpl implements IPayChannelService {

    final PayChannelMapper payChannelMapper;

    @Override
    public List<PayChannelDTO> listUsable(PayOrderTypeEnum orderType, PayClientTypeEnum clientType) {
        List<PayChannelPO> payChannelPOS = payChannelMapper.selectList(
                Wrappers.<PayChannelPO>lambdaQuery()
                        .eq(PayChannelPO::getOpenStatus, PayChannelOpenStatusEnum.OPEN.getStatus())
                        .like(PayChannelPO::getClientTypes,clientType.getCode())
                        .like(PayChannelPO::getOrderTypes,orderType.getCode())
                        .orderByAsc(PayChannelPO::getSequence)
        );
        return PaymentConvertor.convert(payChannelPOS,PayChannelDTO.class);
    }

}
