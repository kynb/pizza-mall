package club.kynb.mall.product.mysql.mapper;

import club.kynb.mall.product.mysql.po.ProductSkuPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
/**
 * 商品sku表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */
@Mapper
public interface ProductSkuMapper extends BaseMapper<ProductSkuPO> {


}
