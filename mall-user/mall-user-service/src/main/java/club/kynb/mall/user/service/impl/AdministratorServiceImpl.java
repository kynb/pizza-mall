package club.kynb.mall.user.service.impl;

import club.kynb.mall.user.api.IAdministratorService;
import club.kynb.mall.user.constant.UserStatus;
import club.kynb.mall.user.constant.UserType;
import club.kynb.mall.user.model.dto.AdministratorDTO;
import club.kynb.mall.user.model.query.AdministratorPageQuery;
import club.kynb.mall.user.mysql.mapper.AdministratorMapper;
import club.kynb.mall.user.mysql.mapper.UserMapper;
import club.kynb.mall.user.mysql.po.AdministratorPO;
import club.kynb.mall.user.mysql.po.UserPO;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.model.page.PageResult;
import org.pizza.mybatis.plus.support.Pages;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author kynb_club@163.com
 * @since 2021/8/3 2:26 下午
 */
@Slf4j
@Service
@AllArgsConstructor
public class AdministratorServiceImpl implements IAdministratorService {
    private final AdministratorMapper administratorMapper;
    private final UserMapper userMapper;

    @Override
    public PageResult<AdministratorDTO> pageQuery(AdministratorPageQuery pageQuery) {
        IPage<AdministratorPO> page = Pages.convert(pageQuery);
        QueryWrapper<AdministratorPO> wrapper = new QueryWrapper<>();
        if (StrUtil.isNotBlank(pageQuery.getKeyword())) {
            wrapper.like("realName", pageQuery.getKeyword());
        }

        IPage<AdministratorPO> poPageResult = administratorMapper.selectPage(page, wrapper);
        PageResult<AdministratorDTO> pageResult = Pages.convert(poPageResult, AdministratorDTO.class);
        if (pageResult.getTotal() > 0) {
            //用户ID列表
            List<Long> collect = pageResult.getRecords().stream().map(AdministratorDTO::getId).collect(Collectors.toList());
            //用户列表
            List<UserPO> userList = userMapper.selectList(Wrappers.<UserPO>lambdaQuery().in(UserPO::getId, collect));
            Map<Long, UserPO> userMap = userList.stream().collect(Collectors.toMap(UserPO::getId, user -> user, (u1, u2) -> u2));
            //补充数据
            pageResult.getRecords().forEach(dto -> {
                UserPO userPO = userMap.get(dto.getId());
                if (null == userPO) {
                    log.error("管理员ID :{} 没有找到User", dto.getId());
                } else {
                    dto.setPhone(userPO.getPhone());
                    dto.setStatus(userPO.getStatus());
                    dto.setExtension(userPO.getExtension());
                }
            });
        }
        return pageResult;

    }

    @Override
    @Transactional
    public void create(AdministratorDTO administratorDTO) {
        AdministratorPO administratorPO = new AdministratorPO();
        administratorPO.setId(administratorDTO.getId());
        administratorPO.setRealName(administratorDTO.getRealName());
        UserPO userPO = new UserPO();
        userPO.setId(administratorDTO.getId());
        userPO.setPhone(administratorDTO.getPhone());
        userPO.setSalt(UUID.randomUUID().toString());
        userPO.setPassword(SecureUtil.md5(administratorDTO.getPassword() + userPO.getSalt()));
        userPO.setStatus(UserStatus.NORMAL.name());
        userPO.setType(UserType.ADMINISTRATOR.name());
        userPO.setExtension(administratorDTO.getExtension());
        administratorMapper.insert(administratorPO);
        userMapper.insert(userPO);
    }

    @Override
    @Transactional
    public void update(AdministratorDTO administratorDTO) {
        if (StrUtil.isNotBlank(administratorDTO.getRealName())) {
            AdministratorPO administratorPO = new AdministratorPO();
            administratorPO.setId(administratorDTO.getId());
            administratorPO.setRealName(administratorDTO.getRealName());
            administratorMapper.updateById(administratorPO);
        }
        if (StrUtil.isNotBlank(administratorDTO.getPassword())) {
            UserPO userPO = new UserPO();
            userPO.setId(administratorDTO.getId());
            userPO.setSalt(UUID.randomUUID().toString());
            userPO.setPassword(SecureUtil.md5(administratorDTO.getPassword() + userPO.getSalt()));
            userPO.setExtension(administratorDTO.getExtension());
            userMapper.updateById(userPO);
        }
    }

    @Override
    public void delete(Long administratorId) {
        administratorMapper.deleteBy(administratorId);
        userMapper.deleteBy(administratorId);
    }
}
