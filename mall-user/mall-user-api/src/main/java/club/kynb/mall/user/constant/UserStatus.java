package club.kynb.mall.user.constant;


import org.pizza.valid.EnumValidI;

/**
 * @author kynb_club@163.com
 * @since 2020/12/8 2:44 下午
 */
public enum UserStatus implements EnumValidI {
    FREEZE,//停用
    NORMAL,//正常
    DESTROY;//注销

    @Override
    public String code() {
        return this.name();
    }

    @Override
    public String message() {
        return "状态：" + this.name();
    }
}
