package club.kynb.mall.user.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

/**
 * 收货地址表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-06-27
 */

@Data
@ApiModel(value = "ShippingAddressDTO对象", description = "收货地址表")
@Accessors(chain = true)
public class ShippingAddressDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", example = "")
    private Long userId;
    /**
     * 地标名
     */
    @ApiModelProperty(value = "地标名", example = "")
    private String landmark;
    /**
     * 门牌号
     */
    @ApiModelProperty(value = "门牌号", example = "")
    private String houseNo;
    /**
     * 收货地址(地图上定位的地址)
     */
    @ApiModelProperty(value = "收货地址(地图上定位的地址)", example = "")
    private String address;
    /**
     * 联系人
     */
    @ApiModelProperty(value = "联系人", example = "林嘉")
    private String contact;
    /**
     * 电话
     */
    @ApiModelProperty(value = "电话", example = "18099991234")
    private String phone;
    /**
     * 标签
     */
    @ApiModelProperty(value = "标签", example = "家")
    private String tag;
    /**
     * 经度
     */
    @ApiModelProperty(value = "经度", example = "198.00")
    private Double longitude;
    /**
     * 纬度
     */
    @ApiModelProperty(value = "纬度", example = "100.11")
    private Double latitude;
    /**
     * 是否默认0否1是
     */
    @ApiModelProperty(value = "是否默认0否1是", example = "")
    private Integer defaulted;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "", hidden = true)
    private String extension;
}