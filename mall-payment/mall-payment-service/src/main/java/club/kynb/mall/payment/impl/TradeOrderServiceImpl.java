package club.kynb.mall.payment.impl;

import club.kynb.mall.payment.api.ITradeOrderService;
import club.kynb.mall.payment.convert.PaymentConvertor;
import club.kynb.mall.payment.dto.PayTradeOrderDTO;
import club.kynb.mall.payment.mysql.mapper.PayTradeOrderMapper;
import club.kynb.mall.payment.mysql.po.PayTradeOrderPO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.exception.Errors;
import org.pizza.util.Checker;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @date: 2021/7/1
 */
@Slf4j
@Service
@AllArgsConstructor
public class TradeOrderServiceImpl implements ITradeOrderService {

    private final PayTradeOrderMapper payTradeOrderMapper;


    @Override
    public Optional<PayTradeOrderDTO> getByTradeNo(String tradeOrderNo) {
        PayTradeOrderPO payTradeOrderPO = payTradeOrderMapper.getByOrderNo(tradeOrderNo);
        if(payTradeOrderPO == null){
            return Optional.empty();
        }
        return Optional.of(PaymentConvertor.convert(PayTradeOrderDTO.class,payTradeOrderPO));
    }

    @Override
    public void update(PayTradeOrderDTO tradeOrderDTO) {
        PayTradeOrderPO po = PaymentConvertor.convert(PayTradeOrderPO.class,tradeOrderDTO);
        int update = payTradeOrderMapper.updateById(po);
        Checker.ifNotThrow(update > 0,()-> Errors.SYSTEM.exception("系统错误,支付流水更新失败"));
    }
}
