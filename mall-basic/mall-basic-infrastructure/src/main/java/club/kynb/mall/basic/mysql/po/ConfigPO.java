package club.kynb.mall.basic.mysql.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import org.pizza.mybatis.plus.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 配置表持久实体类
 * @author kynb_club@163.com
 * @since 2021-06-27
 */

@Data
@Accessors(chain = true)
@TableName("mall_config")
@EqualsAndHashCode(callSuper = true)
public class ConfigPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 作用域枚举
     */
    private String scope;
    /**
     * 配置描述
     */
    private String configDesc;
    /**
     * 配置key
     */
    private String configKey;
    /**
     * 配置value
     */
    private String configValue;
    /**
     * 备注信息
     */
    private String remark;
    /**
     * 扩展字段
     */
    private String extension;


}
