package club.kynb.mall.user.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.util.List;

/**
 * 客服表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-08-03
 */

@Data
@ApiModel(value = "AdministratorAuthDTO对象", description = "管理员权限信息表")
@Accessors(chain = true)
public class AdministratorAuthDTO implements DTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "授权应用列表，PMS_PC（平台运营管理后台）、OMS_PC（商城运营管理后台）、WMS_PC（仓库进销存管理后台）", example = "")
    private List<String> appList;

    @ApiModelProperty(value = "权限列表 应用为WMS_PC（仓库进销存管理后台）时 必传仓库id", example = "")
    private List<String> authList;
}
