package club.kynb.mall.payment.exec;

import club.kynb.mall.payment.constant.PayChannelEnum;
import club.kynb.mall.payment.mysql.po.PayTradeOrderPO;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.springframework.stereotype.Service;

/**
 * 微信
 * @author kynb_club@163.com
 * @date: 2021/7/7
 */
@Service("wechatPayExecutorImpl")
public class WechatPayExecutorImpl extends AbstractPayExecutor {

    public WechatPayExecutorImpl() {
        super();
    }

    @Override
    protected JSONObject doCreatePay() {
        return JSONUtil.parseObj("{\"flowNo\":\"001\",\"payData\":\"1\"}");
    }

    @Override
    protected JSONObject doQuery(PayTradeOrderPO tradeOrder) {
        return JSONUtil.parseObj("{\"status\":\"1\",\"amt\":\"1\"}");
    }

    @Override
    public String getChannel() {
        return PayChannelEnum.WECHAT.code();
    }
}
