package club.kynb.mall.application.service;

import club.kynb.mall.application.context.AuthenticationContext;
import club.kynb.mall.application.flow.user.CheckSmsVerificationCodeFilter;
import club.kynb.mall.application.flow.user.CheckUserAlreadyExistFilter;
import club.kynb.mall.application.flow.user.UserRegisterFilter;
import club.kynb.mall.application.model.model.command.SmsRegistryCommand;
import club.kynb.mall.application.model.model.vo.RegisterResultVO;
import club.kynb.mall.user.model.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.pattern.filter.FilterChain;
import org.pizza.pattern.filter.FilterChainFactory;
import org.springframework.stereotype.Service;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 10:24 下午
 */
@Slf4j
@Service
@AllArgsConstructor
public class RegisterApplicationService {

    public RegisterResultVO register(SmsRegistryCommand command) {
        AuthenticationContext authenticationContext = new AuthenticationContext();
        authenticationContext.setAuthenticationCommand(command);
        FilterChain<AuthenticationContext> filterChain = this.buildSmsLoginFilterChain();
        filterChain.doFilter(authenticationContext);
        final UserDTO user = authenticationContext.getUser();
        return new RegisterResultVO()
                .setUserId(String.valueOf(user.getId()));
    }

    private FilterChain<AuthenticationContext> buildSmsLoginFilterChain() {
        return FilterChainFactory.buildFilterChain(
                CheckUserAlreadyExistFilter.class
                , CheckSmsVerificationCodeFilter.class
                , UserRegisterFilter.class
        );
    }


}
