package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Pattern;

/**
 * @author kynb_club@163.com
 * @since 2020/12/18 9:35 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "SmsRegistryCommand对象", description = "用户注册命令")
@Accessors(chain = true)
public class SmsRegistryCommand extends SmsAuthenticationCommand {

    @Length(message = "密码需要6~20数字字母组合", min = 6, max = 20)
    @Pattern(regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z_]{6,20}$", message = "密码需要6~20数字字母组合")
    @ApiModelProperty(value = "密码", example = "Ab1234", required = true)
    private String password;

}
