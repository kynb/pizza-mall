package club.kynb.mall.application.flow.order;

import club.kynb.mall.application.context.OrderSubmitContext;
import club.kynb.mall.application.model.model.dto.PreviewOrderDTO;
import club.kynb.mall.application.model.model.dto.UserReceiveCouponDTO;
import club.kynb.mall.order.model.dto.OrderDTO;
import cn.hutool.core.util.NumberUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author kynb_club@163.com
 * @since 2021/7/7 1:56 下午
 */
@Slf4j
@Component
@AllArgsConstructor
public class CalculateSubmitAmountFilter extends AbstractSubmitOrderFlowFilter {

    @Override
    protected void doService(OrderSubmitContext orderSubmitContext) {
        this.calculateTotalItemNum(orderSubmitContext);
        this.calculateTotalAmount(orderSubmitContext);
        this.calculateFreightAmount(orderSubmitContext);
        this.calculateCouponAmount(orderSubmitContext);
        this.calculateDiscountAmount(orderSubmitContext);
        this.calculatePayableAmount(orderSubmitContext);
        this.calculatePayAmount(orderSubmitContext);
    }

    private void calculatePayAmount(OrderSubmitContext orderSubmitContext) {
        final OrderDTO orderDTO = orderSubmitContext.getOrderDTO();
        //实付金额（商品总价+运费-优惠总金额）
        orderDTO.setPayAmount(NumberUtil.sub(orderDTO.getPayableAmount(), orderDTO.getDiscountAmount()));
    }

    private void calculatePayableAmount(OrderSubmitContext orderSubmitContext) {
        final OrderDTO orderDTO = orderSubmitContext.getOrderDTO();
        //应付金额（商品总价+运费）
        orderDTO.setPayableAmount(NumberUtil.add(orderDTO.getTotalAmount(), orderDTO.getFreightAmount()));
    }

    private void calculateDiscountAmount(OrderSubmitContext orderSubmitContext) {
        //优惠总金额（优惠券总金额+其他优惠金额）
        final OrderDTO orderDTO = orderSubmitContext.getOrderDTO();
        orderDTO.setDiscountAmount(orderDTO.getCouponAmount());
    }

    private void calculateCouponAmount(OrderSubmitContext orderSubmitContext) {
        //优惠券费用使用当前的优惠券
        final UserReceiveCouponDTO usedCoupon = orderSubmitContext.getUsedCoupon();
        final OrderDTO orderDTO = orderSubmitContext.getOrderDTO();
        if (usedCoupon == null) {
            orderDTO.setCouponAmount(BigDecimal.ZERO);
        } else {
            orderDTO.setCouponAmount(new BigDecimal(usedCoupon.getCouponValue()));
        }
    }

    private void calculateFreightAmount(OrderSubmitContext orderSubmitContext) {
        //商品运费暂时保持和预览订单一样
        final PreviewOrderDTO previewOrder = orderSubmitContext.getPreviewOrder();
        final OrderDTO orderDTO = orderSubmitContext.getOrderDTO();
        orderDTO.setFreightAmount(previewOrder.getFreightAmount());
    }

    private void calculateTotalAmount(OrderSubmitContext orderSubmitContext) {
        //商品总价保持和预览订单一样
        final PreviewOrderDTO previewOrder = orderSubmitContext.getPreviewOrder();
        final OrderDTO orderDTO = orderSubmitContext.getOrderDTO();
        orderDTO.setTotalAmount(previewOrder.getTotalAmount());
    }

    private void calculateTotalItemNum(OrderSubmitContext orderSubmitContext) {
        //商品数量保持和预览订单一样
        final PreviewOrderDTO previewOrder = orderSubmitContext.getPreviewOrder();
        final OrderDTO orderDTO = orderSubmitContext.getOrderDTO();
        orderDTO.setTotalItemNum(previewOrder.getTotalItemNum());
    }
}
