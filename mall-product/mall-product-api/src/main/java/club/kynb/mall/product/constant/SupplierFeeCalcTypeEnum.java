package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Arrays;
import java.util.Optional;

/**
 * 供应商 分账手续计算类型 1:固定手续费 2：手续费率
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Getter
@AllArgsConstructor
public enum SupplierFeeCalcTypeEnum implements EnumValidI {

    FEE(1,"固定手续费"),
    RATE(2,"手续费率"),
    ;

    private Integer type;
    private String desc;

    @Override
    public String code() {
        return String.valueOf(this.type);
    }

    @Override
    public String message() {
        return this.desc;
    }

    public static Optional<SupplierFeeCalcTypeEnum> get(Integer type){
        return Arrays.stream(SupplierFeeCalcTypeEnum.values())
                .filter(item-> item.getType().equals(type))
                .findFirst();
    }
}
