package club.kynb.mall.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.page.PageQuery;



@Data
@ApiModel(value = "ProductCategoryParamDTO对象", description = "商品分类列表搜索参数")
@Accessors(chain = true)
public class ProductCategoryParamDTO extends PageQuery {
    private static final long serialVersionUID = 1L;

    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID", example = "")
    private Long parentId;
    /**
     * 分类名称
     */
    @ApiModelProperty(value = "分类名称", example = "")
    private String cateName;

}
