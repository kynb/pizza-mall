package club.kynb.mall.basic.api;

import club.kynb.mall.basic.constant.ConfigScope;
import club.kynb.mall.basic.dto.ConfigDTO;

import java.util.List;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/27 10:51 上午
 */
public interface IConfigService {
    List<ConfigDTO> getByScope(ConfigScope scope);
    Optional<ConfigDTO> getByScopeAndKey(ConfigScope scope, String configKey);
}

