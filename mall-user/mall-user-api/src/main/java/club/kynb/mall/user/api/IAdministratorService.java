package club.kynb.mall.user.api;


import club.kynb.mall.user.model.dto.AdministratorDTO;
import club.kynb.mall.user.model.query.AdministratorPageQuery;
import org.pizza.model.page.PageResult;

/**
 * @author kynb_club@163.com
 * @since 2021/8/3 2:26 下午
 */
public interface IAdministratorService {

    PageResult<AdministratorDTO> pageQuery(AdministratorPageQuery pageQuery);

    void create(AdministratorDTO administratorDTO);

    void update(AdministratorDTO administratorDTO);

    void delete(Long administratorId);
}
