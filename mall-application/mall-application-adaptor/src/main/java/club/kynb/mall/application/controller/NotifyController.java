package club.kynb.mall.application.controller;

import club.kynb.mall.application.service.NotifyApplicationService;
import cn.hutool.json.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 3:34 下午
 */
@Slf4j
@RequestMapping("notify")
@AllArgsConstructor
@RestController
@Api(value = "", tags = "通知类接口")
public class NotifyController {

    private final NotifyApplicationService notifyApplicationService;

    @RequestMapping("/pay/notify")
    @ApiOperation(value = "支付通知", notes = "")
    public String qbPayNotify(JSONObject request) {
        return notifyApplicationService.payNotify(request);
    }

}
