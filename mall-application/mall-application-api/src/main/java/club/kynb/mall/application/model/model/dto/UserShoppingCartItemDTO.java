package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;

/**
 * @author kynb_club@163.com
 * @since 2021/6/30 10:04 下午
 */
@Data
@ApiModel(value = "UserShoppingCartItemDTO对象", description = "用户购物车商品")
@Accessors(chain = true)
public class UserShoppingCartItemDTO implements DTO {

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 购物车SPU信息
     */
    @ApiModelProperty(value = "购物车SPU信息", example = "")
    private CartSpuDTO cartSpuDTO;
    /**
     * 是否选中 0否 1是
     */
    @ApiModelProperty(value = "是否选中 0否 1是", example = "")
    private Integer checked;
    /**
     * 当前最新状态 1正常、2已下架、3已售罄
     */
    @ApiModelProperty(value = "当前最新状态 1正常、2已下架、3已售罄", example = "")
    private Integer currentStatus;
    /**
     * 分组展示依据,展示时按此字段进行分组（如按活动分组、按供应商..）
     */
    @ApiModelProperty(value = "分组展示依据,展示时按此字段进行分组（如按活动分组、按供应商..）", example = "")
    private String groupKey;
    /**
     * 弱提示
     */
    @ApiModelProperty(value = "弱提示，例如比加入时降价了...", example = "")
    private String tips;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;


    @Data
    @ApiModel(value = "CartSpuDTO对象", description = "购物车SPU信息")
    @Accessors(chain = true)
    public static class CartSpuDTO implements DTO {
        @ApiModelProperty(value = "spuId", example = "1")
        private String id;
        /**
         * 商品名称
         */
        @ApiModelProperty(value = "商品名称", example = "云南甜玉米")
        private String spuName;

        /**
         * 列表展示图片
         */
        @ApiModelProperty(value = "商品图片", example = "")
        private String iconUrl;
        /**
         * SKU信息
         */
        @ApiModelProperty(value = "SKU信息", example = "")
        private CartSkuDTO cartSkuDTO;
    }

    @Data
    @ApiModel(value = "CartSkuDTO对象", description = "购物车SKU信息")
    @Accessors(chain = true)
    public static class CartSkuDTO implements DTO {
        @ApiModelProperty(value = "skuId", example = "1")
        private String id;
        /**
         * spuId
         */
        @ApiModelProperty(value = "spuId", example = "")
        private String spuId;
        /**
         * sku名称
         */
        @ApiModelProperty(value = "sku名称", example = "3斤")
        private String skuName;
        /**
         * 计价单位
         */
        @ApiModelProperty(value = "计价单位", example = "斤")
        private String skuUnit;
        /**
         * 加购数量
         */
        @ApiModelProperty(value = "商品数量", example = "3")
        private Integer skuNum;
        /**
         * sku价格
         */
        @ApiModelProperty(value = "sku价格", example = "")
        private CartSkuPriceDTO skuPriceDTO;
    }

    @Data
    @ApiModel(value = "CartSkuPriceDTO对象", description = "购物车SKU价格信息")
    @Accessors(chain = true)
    public static class CartSkuPriceDTO implements DTO {
        /**
         *
         */
        @ApiModelProperty(value = "基本价格", example = "18.88")
        private BigDecimal skuPrice;
        @ApiModelProperty(value = "计价单位", example = "斤")
        private String unit;
    }
}
