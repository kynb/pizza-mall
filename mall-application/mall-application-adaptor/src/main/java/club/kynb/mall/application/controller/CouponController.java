package club.kynb.mall.application.controller;

import club.kynb.mall.application.service.CouponApplicationService;
import club.kynb.mall.application.model.model.dto.UserCouponTemplateDTO;
import club.kynb.mall.application.model.model.dto.UserReceiveCouponDTO;
import club.kynb.mall.application.model.model.command.ReceiveCouponCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.response.R;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/6/29 12:54 下午
 */
@Slf4j
@RestController
@RequestMapping
@AllArgsConstructor
@Api(value = "", tags = "优惠券相关接口")
public class CouponController {
    private final CouponApplicationService couponApplicationService;

    @GetMapping("/couponTemplate/list")
    @ApiOperation(value = "领券中心-优惠券列表", notes = "")
    public R<List<UserCouponTemplateDTO>> couponList() {
        return R.ok(couponApplicationService.couponList());
    }

    @PostMapping("/couponTemplate/receive")
    @ApiOperation(value = "领券中心-领取优惠券", notes = "")
    public R<Void> drawCoupon(@RequestBody @Valid ReceiveCouponCommand command) {
        couponApplicationService.receiveCoupon(command);
        return R.ok();
    }

    @GetMapping("/userCoupon/list/{status}")
    @ApiOperation(value = "我的-优惠券列表", notes = "")
    public R<List<UserReceiveCouponDTO>> userCouponList(@PathVariable(name = "status") Integer status) {
        return R.ok(couponApplicationService.userCouponList(status));
    }
}
