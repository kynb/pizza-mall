package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

/**
 * 广告位状态
 *
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Getter
@AllArgsConstructor
public enum AdConfigStatusEnum implements EnumValidI {

    OPEN(1, "开启"),
    CLOSE(2, "关闭"),
    ;

    private final Integer status;
    private final String desc;

    @Override
    public String code() {
        return String.valueOf(this.status);
    }

    @Override
    public String message() {
        return this.desc;
    }
}
