package club.kynb.mall.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

/**
 * 订单取消类型
 * @author kynb_club@163.com
 * @since  2021/7/8
 */
@Getter
@AllArgsConstructor
public enum OrderConfirmTypeEnum implements EnumValidI {
    AUTO_CONFIRM,     //超时自动确认订单
    MANUAL_CONFIRM,   //用户手动确认订单
    ;

    @Override
    public String code() {
        return this.name();
    }

    @Override
    public String message() {
        return this.name();
    }


}
