package club.kynb.mall.application.model.model.vo;

import club.kynb.mall.order.constant.OrderPayStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.VO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付结果对象
 *
 * @author kynb_club@163.com
 * @date:  2021/7/2
 */
@Data
@ApiModel(value = "OrderPayResult对象", description = "支付结果信息")
@Accessors(chain = true)
public class OrderPayResultVO implements VO {

	@ApiModelProperty(value = "支付结果", example = "")
	private OrderPayStatusEnum status;
	@ApiModelProperty(value = "支付方式", example = "")
	private String channel;
	/**
	 * 实付金额
	 */
	@ApiModelProperty(value = "实付金额", example = "")
	private BigDecimal payAmount;
	/**
	 * 支付完成时间
	 */
	@ApiModelProperty(value = "支付完成时间", example = "")
	private Date payFinishTime;





}
