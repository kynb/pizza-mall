package club.kynb.mall.product.api;


import club.kynb.mall.product.dto.*;
import org.pizza.model.page.PageQuery;
import org.pizza.model.page.PageResult;

import java.util.List;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/20 4:59 下午
 */
public interface IProductInfoService {


    /**
     * 详情
     * @param spuId
     * @return
     */
    Optional<ProductSpuDetailDTO> detailOnSaleById(Long spuId);

    List<ProductSpuDetailDTO> listOnSaleByParam(ProductByParamDTO param);

    void check(Long spuId, Long skuId);

    List<ProductSpuDTO> listSpuByParam(ProductByParamDTO dto);

    /********************** 管理后台 ************************************************/
    /**
     * 管理后台分页查询
     * @param pageQuery
     * @return
     */
    PageResult<ProductSpuDTO> pageQuery(ProductByParamDTO dto, PageQuery pageQuery);

    Optional<ProductSpuDTO> getSpuById(Long spuId);

    Optional<ProductSkuDTO> getSkuById(Long skuId);

    List<ProductSkuDTO> listSkuBySpuId(Long spuId);

    void create(ProductSpuDTO spuDTO);

    void update(ProductSpuDTO spuDTO);

    void create(ProductSkuDTO skuDTO);

    void update(ProductSkuDTO skuDTO);

    Integer countOnSaleSkuBySpu(Long spuId);
}
