package club.kynb.mall.payment.dto.ext;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 支付结果扩展
 *
 * @author kynb_club@163.com
 * @date:  2021/7/2
 */
@Data
@ApiModel(value = "TradeOrderInfoDto对象", description = "支付返回扩展信息")
@Accessors(chain = true)
public class PayResultExtDTO implements Serializable{

	/**
	 * 错误
	 */
	@ApiModelProperty(value = "错误", example = "")
	private String errorCode;

	private String message;
}
