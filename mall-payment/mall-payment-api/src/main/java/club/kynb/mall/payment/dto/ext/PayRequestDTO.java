package club.kynb.mall.payment.dto.ext;

import club.kynb.mall.payment.constant.PayChannelEnum;
import club.kynb.mall.payment.constant.PayClientTypeEnum;
import club.kynb.mall.payment.constant.PayOrderTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;

/**
 * 支付请求对象
 *
 * @author kynb_club@163.com
 * @date:  2021/7/2
 */
@Data
@ApiModel(value = "PayRequestDTO对象", description = "支付请求信息")
@Accessors(chain = true)
public class PayRequestDTO<T extends PayExtConfigDTO> implements DTO {

	/**
	 * 	调用客户端类型
	 */
	@ApiModelProperty(value = "客户端类型", example = "")
	private PayClientTypeEnum payClientType;
	/**
	 * 	支付渠道
	 */
	@ApiModelProperty(value = "支付渠道", example = "")
	private PayChannelEnum payChannel;


	@ApiModelProperty(value = "支付订单类型", example = "")
	private PayOrderTypeEnum orderType;
	/**
	 * 	订单编号
	 */
	@ApiModelProperty(value = "订单编号", example = "")
	private String orderNo;

	@ApiModelProperty(value = "用户id", example = "")
	private Long userId;
	/**
	 * 	订单支付总金额
	 */
	@ApiModelProperty(value = "支付金额", example = "")
	private BigDecimal payAmount;
	/**
	 * 	订单支付说明
	 */
	@ApiModelProperty(value = "订单说明", example = "")
	private String orderSubject;
	/**
	 * 	支付额外配置
	 */
	@ApiModelProperty(value = "支付扩展参数", example = "")
	private T extParam;

}
