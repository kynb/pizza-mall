package club.kynb.mall.application.convert;

import club.kynb.mall.application.model.model.command.ShoppingCartAddCommand;
import club.kynb.mall.application.model.model.dto.PriceDTO;
import club.kynb.mall.application.model.model.dto.UserOrderDTO;
import club.kynb.mall.application.model.model.dto.UserOrderDetailDTO;
import club.kynb.mall.application.model.model.dto.UserOrderDetailDTO.UserOrderSkuDTO;
import club.kynb.mall.application.model.model.dto.UserOrderDetailDTO.UserOrderSpuDTO;
import club.kynb.mall.order.constant.OrderPayStatusEnum;
import club.kynb.mall.order.constant.ShoppingCartConstant;
import club.kynb.mall.order.model.dto.OrderAndDetailsDTO;
import club.kynb.mall.order.model.dto.ShoppingCartDTO;
import club.kynb.mall.payment.constant.TradeOrderStatusEnum;
import club.kynb.mall.product.dto.ProductSkuDTO;
import club.kynb.mall.product.dto.ProductSpuDetailDTO;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.exception.Errors;
import org.pizza.constant.GlobalConstant;
import org.pizza.model.page.PageResult;
import org.pizza.util.Convertor;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author kynb_club@163.com
 * @since 2021/7/3 8:40 上午
 */
@Slf4j
public class MallCommonConvertor extends Convertor {

    public static ShoppingCartDTO covert(ShoppingCartAddCommand command) {
        final ShoppingCartDTO shoppingCartDTO = new ShoppingCartDTO();
        shoppingCartDTO.setUserId(command.getUserId());
        shoppingCartDTO.setSpuId(String.valueOf(command.getSpuId()));
        shoppingCartDTO.setSkuId(String.valueOf(command.getSkuId()));
        shoppingCartDTO.setAddChannel(ShoppingCartConstant.ShoppingCartChannelEnum.APP_MALL.name());
        shoppingCartDTO.setSkuNum(1);
        shoppingCartDTO.setChecked(GlobalConstant.YES);
        return shoppingCartDTO;
    }


    public static PageResult<UserOrderDTO> covert(PageResult<OrderAndDetailsDTO> pageResult) {
        PageResult<UserOrderDTO> newPageResult = new PageResult<>();
        newPageResult.setTotal(pageResult.getTotal());
        newPageResult.setSize(pageResult.getSize());
        newPageResult.setCurrent(pageResult.getCurrent());
        newPageResult.setOrders(pageResult.getOrders());
        newPageResult.setSearchCount(pageResult.isSearchCount());
        newPageResult.setOptimizeCountSql(pageResult.isOptimizeCountSql());
        newPageResult.setHitCount(pageResult.isHitCount());
        final List<UserOrderDTO> list = pageResult.getRecords().stream()
                .map(MallCommonConvertor::covertUserOrderDTO)
                .collect(Collectors.toList());
        newPageResult.setRecords(list);
        return newPageResult;
    }

    public static UserOrderDTO covertUserOrderDTO(OrderAndDetailsDTO orderAndDetailsDTO) {
        final UserOrderDTO userOrderDTO = Convertor.convert(UserOrderDTO.class, orderAndDetailsDTO);

        //订单详情转换
        final List<UserOrderDetailDTO> userOrderDetailList = orderAndDetailsDTO.getDetails().stream().map(orderDetailDTO -> {
            final UserOrderDetailDTO userOrderDetailDTO = new UserOrderDetailDTO();
            userOrderDetailDTO.setId(orderDetailDTO.getId());
            userOrderDetailDTO.setSkuNum(orderDetailDTO.getSkuNum());
            userOrderDetailDTO.setTotalAmount(orderDetailDTO.getTotalAmount());
            userOrderDetailDTO.setShareAmount(orderDetailDTO.getShareAmount());
            //解析快照信息
            final String skuSnapshoot = orderDetailDTO.getSkuSnapshoot();
            final ProductSpuDetailDTO productSpuDTO = JSONUtil.toBean(skuSnapshoot, ProductSpuDetailDTO.class);
            final Optional<ProductSkuDTO> skuOptional = productSpuDTO.getSkuList().stream().findFirst();
            if (!skuOptional.isPresent()) {
                log.error("系统异常，数据错误，订单详情快照中找不到sku信息，订单详情信息：{}", JSONUtil.toJsonStr(orderDetailDTO));
                throw Errors.SYSTEM.exception("系统异常，数据错误");
            }
            final ProductSkuDTO skuDTO = skuOptional.get();

            //spu、sku...
            final UserOrderSpuDTO userOrderSpuDTO = new UserOrderSpuDTO();
            userOrderSpuDTO.setId(productSpuDTO.getId());
            userOrderSpuDTO.setSpuName(productSpuDTO.getSpuName());
            userOrderSpuDTO.setIconUrl(StrUtil.split(productSpuDTO.getSpuImgs(),",")[0]);
            userOrderSpuDTO.setSpuImgs(productSpuDTO.getSpuImgs());
            final PriceDTO priceDTO = new PriceDTO();
            userOrderSpuDTO.setPriceInfo(priceDTO);
            final UserOrderSkuDTO userOrderSkuDTO = new UserOrderSkuDTO();
            userOrderSkuDTO.setId(skuDTO.getId());
            userOrderSkuDTO.setSpuId(skuDTO.getSpuId().toString());
            userOrderSkuDTO.setSkuName(skuDTO.getSkuName());
            userOrderSkuDTO.setSkuUnit(skuDTO.getSkuUnit());
            userOrderSkuDTO.setSkuImage(skuDTO.getSkuImage());
            userOrderSkuDTO.setSkuNum(orderDetailDTO.getSkuNum());
            priceDTO.setPrice(skuDTO.getSkuPrice());
            priceDTO.setUnit(skuDTO.getSkuUnit());
            userOrderSkuDTO.setPriceInfo(priceDTO);
            userOrderSpuDTO.setSku(userOrderSkuDTO);
            userOrderDetailDTO.setUserOrderSpuDTO(userOrderSpuDTO);
            return userOrderDetailDTO;
        }).collect(Collectors.toList());
        userOrderDTO.setOrderDetailList(userOrderDetailList);
        return userOrderDTO;
    }

    public static OrderPayStatusEnum convertPayStatusFromTradeStatus(TradeOrderStatusEnum statusEnum){
        switch (statusEnum){
            case SUCCESS:
                return OrderPayStatusEnum.PAID;
            case WAITING_4_UPDATE:
                return OrderPayStatusEnum.PAYING;
            case FAILURE:
                return OrderPayStatusEnum.NO_PAY;
            default:
                throw Errors.BIZ.exception("未知的交易状态");
        }
    }
}
