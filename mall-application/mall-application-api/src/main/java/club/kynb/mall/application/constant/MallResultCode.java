package club.kynb.mall.application.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.common.web.model.response.ResultCode;

/**
 * @author kynb_club@163.com
 * @since 2020/11/16 2:38 下午
 * 基础领10
 * 用户域11
 * 商品域12
 * 订单域13
 * 支付领14
 */
@AllArgsConstructor
@Getter
public enum MallResultCode implements ResultCode {
    CODE_11001(11001, "累计登录失败次数超过%d次，请%d分钟后再尝试登录"),
    CODE_11002(11002, "图片验证码已失效"),
    CODE_11003(11003, "图片验证码输入错误"),
    CODE_11004(11004, "短信验证码已经过期，请重新发送"),
    CODE_11005(11005, "短信验证码输入有误"),
    CODE_11006(11006, "登录密码错误"),
    CODE_11007(11007, "短信发送太频繁，请60秒后重试"),
    CODE_11008(11008, "用户已经注册了，前往登录吧"),
    CODE_11009(11009, "没有携带accessToken"),
    CODE_11010(11010, "accessToken已经过期，请重新登录"),

    //商品
    CODE_12001(12001, "商品不存在或已下架"),

    //订单
    CODE_13001(13001, "预览订单已失效，请跳转购物车重新提交"),
    CODE_13002(13002, "提交订单失败，无效的收货地址"),
    CODE_13003(13003, "提交订单失败，收货地址无在配送区域内"),
    CODE_13004(13004, "提交订单失败，部分商品库存不足"),
    CODE_13005(13005, "提交订单失败，无效的用户优惠券"),

    //支付
    CODE_14001(14001, "支付方式不可用，请联系客服"),
    CODE_14002(14002, "支付参数错误"),
    ;;

    int code;
    String msg;

    @Override
    public int code() {
        return code;
    }

    @Override
    public String msg() {
        return msg;
    }
}
