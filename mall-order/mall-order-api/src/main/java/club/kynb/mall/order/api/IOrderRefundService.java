package club.kynb.mall.order.api;


import club.kynb.mall.order.model.dto.OrderRefundDTO;
import club.kynb.mall.order.model.dto.OrderRefundHistoryPageQuery;
import club.kynb.mall.order.model.dto.OrderRefundPageQuery;
import org.pizza.model.page.PageResult;

import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/7/11 2:28 下午
 */
public interface IOrderRefundService {

    void create(OrderRefundDTO orderRefund);

    void applyCancel(String refundNo);

    PageResult<OrderRefundDTO> orderRefundHistoryPageQuery(OrderRefundHistoryPageQuery pageQuery);

    Optional<OrderRefundDTO> getRefundOrderBy(String refundNo);

    PageResult<OrderRefundDTO> orderRefundPage(OrderRefundPageQuery pageQuery);

    void orderRefundOperation(String refundNo,Integer status);

}
