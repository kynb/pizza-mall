package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

/**
 * sku价格等级
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Getter
@AllArgsConstructor
public enum SkuPriceLevelEnum implements EnumValidI {

    ONE("1","一级"),
    TWO("2","二级");

    private String level;
    private String desc;

    @Override
    public String code() {
        return this.level;
    }

    @Override
    public String message() {
        return this.desc;
    }
}
