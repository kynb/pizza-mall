package club.kynb.mall.application.flow.order;

import club.kynb.mall.application.constant.MallResultCode;
import club.kynb.mall.application.context.OrderSubmitContext;
import club.kynb.mall.application.model.model.command.SubmitOrderCommand;
import club.kynb.mall.application.model.model.dto.PreviewOrderDTO;
import club.kynb.mall.application.repository.OrderRepository;
import club.kynb.mall.product.dto.ProductSpuDetailDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.exception.Errors;
import org.pizza.util.Checker;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/7/7 1:56 下午
 */
@Slf4j
@Component
@AllArgsConstructor
public class LoadPreOrderCacheFilter extends AbstractSubmitOrderFlowFilter {
    private final OrderRepository orderRepository;

    @Override
    protected void doService(OrderSubmitContext orderSubmitContext) {
        final SubmitOrderCommand command = orderSubmitContext.getCommand();
        final PreviewOrderDTO preOrder = orderRepository.getPreOrder(command.getInnerOrderNo());
        final List<ProductSpuDetailDTO> preOrderSpuList = orderRepository.getPreOrderSpuList(command.getInnerOrderNo());
        Checker.ifNullThrow(preOrder, () -> Errors.BIZ.exception(MallResultCode.CODE_13001));
        Checker.ifEmptyThrow(preOrderSpuList, () -> Errors.BIZ.exception(MallResultCode.CODE_13001));
        orderSubmitContext.setPreviewOrder(preOrder);
        orderSubmitContext.setInnerSpuList(preOrderSpuList);
        orderSubmitContext.getOrderSubmitResultVO().setInnerOrderNo(orderSubmitContext.getCommand().getInnerOrderNo());
        log.info("加载预览订单缓存过滤器 end");
    }
}
