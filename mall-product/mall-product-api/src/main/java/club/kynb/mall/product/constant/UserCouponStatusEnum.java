package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 2:53 下午
 */
@Getter
@AllArgsConstructor
public enum UserCouponStatusEnum implements EnumValidI {

    UN_USE(0, "未使用"),
    USED(1, "已经使用"),
    OVERDUE(2, "已经过期"),
    /**
     * 特殊状态仅使用desc
     */
    NEAR_OVERDUE_DAY(999, "%d天后即将过期"),
    NEAR_OVERDUE_HOUR(999, "%d小时后即将过期"),
    NEAR_OVERDUE_MINUTE(999, "%d分钟后即将过期"),
    ;

    private final Integer status;
    private final String desc;

    @Override
    public String code() {
        return String.valueOf(this.status);
    }

    @Override
    public String message() {
        return this.desc;
    }

    public static Optional<UserCouponStatusEnum> get(Integer status) {
        return Arrays.stream(UserCouponStatusEnum.values())
                .filter(item -> item.getStatus().equals(status))
                .findFirst();
    }
}
