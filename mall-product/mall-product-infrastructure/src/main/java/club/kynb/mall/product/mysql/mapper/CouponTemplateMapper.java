package club.kynb.mall.product.mysql.mapper;

import club.kynb.mall.product.mysql.po.CouponTemplatePO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 优惠券模板表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */
@Mapper
public interface CouponTemplateMapper extends BaseMapper<CouponTemplatePO> {

    @Update("update mall_coupon_template set residue_quantity=residue_quantity-1 where residue_quantity>0  and id=#{couponTemplateId}")
    int deduct(@Param("couponTemplateId") Long couponTemplateId);
}
