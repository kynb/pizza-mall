package club.kynb.mall.application.model.model.query;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Query;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 11:13 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "BusinessCategoryQuery对象", description = "客户经营类目查询")
@Accessors(chain = true)
public class BusinessCategoryQuery extends Query {

}
