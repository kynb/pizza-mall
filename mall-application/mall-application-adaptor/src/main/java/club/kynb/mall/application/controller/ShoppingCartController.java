package club.kynb.mall.application.controller;

import club.kynb.mall.application.service.ShoppingCartApplicationService;
import club.kynb.mall.application.model.model.dto.UserShoppingCartDTO;
import club.kynb.mall.application.model.model.command.ShoppingCartAddCommand;
import club.kynb.mall.application.model.model.command.ShoppingCartCheckedCommand;
import club.kynb.mall.application.model.model.command.ShoppingCartReduceCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.response.R;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * @author kynb_club@163.com
 * @since 2021/6/29 8:36 下午
 */
@Slf4j
@RestController
@RequestMapping
@AllArgsConstructor
@Api(value = "", tags = "购物车相关接口")
public class ShoppingCartController {
    private final ShoppingCartApplicationService shoppingCartApplicationService;

    @GetMapping("/shoppingCart/list")
    @ApiOperation(value = "购物车-用户购物车列表", notes = "")
    public R<UserShoppingCartDTO> userShoppingCartList() {
        return R.ok(shoppingCartApplicationService.userShoppingCartList());
    }
    @GetMapping("/shoppingCart/count")
    @ApiOperation(value = "购物车-用户购物车商品数量", notes = "")
    public R<Long> userShoppingCartCount() {
        return R.ok(shoppingCartApplicationService.userShoppingCartCount());
    }

    @PostMapping("/shoppingCart/add")
    @ApiOperation(value = "购物车-添加商品", notes = "")
    public R<UserShoppingCartDTO> add(@RequestBody @Valid ShoppingCartAddCommand command) {
        return R.ok(shoppingCartApplicationService.add(command));
    }

    @PostMapping("/shoppingCart/reduce")
    @ApiOperation(value = "购物车-减少商品", notes = "PS：删除也复用此接口，删除数量大一点即可")
    public R<UserShoppingCartDTO> reduce(@RequestBody @Valid ShoppingCartReduceCommand command) {
        return R.ok(shoppingCartApplicationService.reduce(command));
    }

    @PostMapping("/shoppingCart/checked")
    @ApiOperation(value = "购物车-勾选商品", notes = "")
    public R<UserShoppingCartDTO> checked(@RequestBody @Valid ShoppingCartCheckedCommand command) {
        return R.ok(shoppingCartApplicationService.checked(command));
    }

}
