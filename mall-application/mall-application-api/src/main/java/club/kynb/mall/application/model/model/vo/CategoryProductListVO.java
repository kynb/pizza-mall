package club.kynb.mall.application.model.model.vo;

import club.kynb.mall.product.dto.ProductSpuDetailDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.VO;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @date: 2021/6/26
 */
@Data
@ApiModel(value = "ProductInfoResult对象", description = "商品结果对象")
@Accessors(chain = true)
public class CategoryProductListVO implements VO {

    @ApiModelProperty(value = "商品列表", example = "")
    private List<ProductSpuDetailDTO> spuList;

}
