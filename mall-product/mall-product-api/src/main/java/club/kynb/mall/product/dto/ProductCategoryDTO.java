package club.kynb.mall.product.dto;

import org.pizza.model.DTO;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;

/**
 * 商品分类表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "ProductCategoryDTO对象", description = "商品分类表")
@Accessors(chain = true)
public class ProductCategoryDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ApiModelProperty(value = "", example = "")
    private Long id;
    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID", example = "")
    private Long parentId;
    /**
     * 分类名称
     */
    @ApiModelProperty(value = "分类名称", example = "")
    private String cateName;
    /**
     * 图片地址
     */
    @ApiModelProperty(value = "图片地址", example = "")
    private String imageUrl;
    /**
     * 图标
     */
    @ApiModelProperty(value = "图标", example = "")
    private String iconUrl;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序", example = "")
    private Integer sequence;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
