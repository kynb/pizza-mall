package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author kynb_club@163.com
 * @since 2021/7/1 9:09 下午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "ShoppingCartAddCommand对象", description = "购物车添加商品命令")
@Accessors(chain = true)
public class ShoppingCartAddCommand extends Command {
    @ApiModelProperty(value = "spuId", example = "1")
    @NotNull(message = "spuId不能为空")
    private Long spuId;

    @ApiModelProperty(value = "skuId", example = "1")
    @NotNull(message = "skuId不能为空")
    private Long skuId;

    @ApiModelProperty(value = "userId", example = "1",hidden = true)
    private Long userId;

    @ApiModelProperty(value = "加购渠道，商城 APP_MALL", example = "APP_MALL")
    @NotBlank(message = "addChannel不能为空，商城 APP_MALL")
    private String addChannel;


}
