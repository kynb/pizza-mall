package club.kynb.mall.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-06-24
 */

@Data
@ApiModel(value = "ProductCategoryFullDTO对象", description = "分类信息逐级递归")
@Accessors(chain = true)
public class ProductCategoryFullDTO extends ProductCategoryDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "创建时间", example = "")
    private List<ProductCategoryDTO> subList;
}
