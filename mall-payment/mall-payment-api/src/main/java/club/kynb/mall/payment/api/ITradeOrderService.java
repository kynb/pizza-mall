package club.kynb.mall.payment.api;

import club.kynb.mall.payment.dto.PayTradeOrderDTO;

import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @date: 2021/7/2
 */
public interface ITradeOrderService {

    Optional<PayTradeOrderDTO> getByTradeNo(String tradeOrderNo);

    void update(PayTradeOrderDTO tradeOrderDTO);
}
