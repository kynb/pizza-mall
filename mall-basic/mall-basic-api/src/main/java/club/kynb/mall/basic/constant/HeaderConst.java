package club.kynb.mall.basic.constant;

/**
 * @author: fyh
 * @date: 2021/7/4
 */
public interface HeaderConst {

    /**
     * 请求头 token key
     */
    String TOKEN = "accessToken";
    /**
     * 请求头 经度 key
     */
    String LONGITUDE = "longitude";
    /**
     * 请求头 纬度 key
     */
    String LATITUDE = "latitude";
    /**
     * 请求头 设备号 key
     */
    String DEVICE_ID = "deviceId";
    /**
     * 请求头 设备类型 key
     */
    String DEVICE_TYPE = "deviceType";
     /**
     * 请求头 引用类型 key
     */
    String CLIENT_TYPE = "clientType";
    /**
     * 请求头 应用版本 key
     */
    String APP_VERSION = "appVersion";
    /**
     * 请求头 应用版本 key
     */
    String API_VERSION = "apiVersion";
}
