package club.kynb.mall.order.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "OrderDTO对象", description = "订单表")
@Accessors(chain = true)
public class OrderDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", example = "")
    private Long userId;
    /**
     * 外部订单编号（显示）
     */
    @ApiModelProperty(value = "外部订单编号（显示）", example = "")
    private String outerOrderNo;
    /**
     * 内部订单编号
     */
    @ApiModelProperty(value = "内部订单编号", example = "")
    private String innerOrderNo;
    /**
     * 订单状态 见订单状态枚举
     */
    @ApiModelProperty(value = "订单状态 见订单状态枚举", example = "")
    private String orderStatus;
    /**
     * 总金额
     */
    @ApiModelProperty(value = "总金额", example = "")
    private BigDecimal totalAmount;
    /**
     * 应付金额
     */
    @ApiModelProperty(value = "应付金额", example = "")
    private BigDecimal payableAmount;
    /**
     * 实付金额
     */
    @ApiModelProperty(value = "实付金额", example = "")
    private BigDecimal payAmount;
    /**
     * 运费金额
     */
    @ApiModelProperty(value = "运费金额", example = "")
    private BigDecimal freightAmount;
    /**
     * 优惠总金额
     */
    @ApiModelProperty(value = "优惠总金额", example = "")
    private BigDecimal discountAmount;
    /**
     * 优惠券总金额
     */
    @ApiModelProperty(value = "优惠券总金额", example = "")
    private BigDecimal couponAmount;
    /**
     * 订单商品总数
     */
    @ApiModelProperty(value = "订单商品总数", example = "")
    private Integer totalItemNum;
    /**
     * 用户优惠券ID
     */
    @ApiModelProperty(value = "用户优惠券ID", example = "")
    private Long userCouponId;
    /**
     * 下单时间
     */
    @ApiModelProperty(value = "下单时间", example = "")
    private Date placeTime;
    /**
     * 自动确认收货时间
     */
    @ApiModelProperty(value = "自动确认收货时间", example = "")
    private Date autoDeliveryTime;
    /**
     * 完成时间
     */
    @ApiModelProperty(value = "完成时间", example = "")
    private Date endTime;
    /**
     * 订单最后付款时间
     */
    @ApiModelProperty(value = "订单最后付款时间", example = "")
    private Date cancelEndTime;
    /**
     * 支付流水号
     */
    @ApiModelProperty(value = "支付流水号", example = "")
    private String payNo;
    /**
     * 支付方式
     */
    @ApiModelProperty(value = "支付方式", example = "")
    private String payChannel;
    /**
     * 支付开始时间
     */
    @ApiModelProperty(value = "支付开始时间", example = "")
    private Date payCreateTime;
    /**
     * 支付状态
     */
    @ApiModelProperty(value = "支付状态", example = "")
    private String payStatus;
    /**
     * 支付完成时间
     */
    @ApiModelProperty(value = "支付完成时间", example = "")
    private Date payFinishTime;
    /**
     * 订单支付时间
     */
    @ApiModelProperty(value = "订单支付时间", example = "")
    private Date depositPayTime;
    /**
     * 订单取消时间
     */
    @ApiModelProperty(value = "订单取消时间", example = "")
    private Date cancelTime;
    /**
     * 取消订单类型
     */
    @ApiModelProperty(value = "取消订单类型", example = "")
    private String cancelType;
    /**
     * 取消原因
     */
    @ApiModelProperty(value = "取消原因", example = "")
    private String cancelDesc;
    /**
     * 物流方式
     */
    @ApiModelProperty(value = "物流方式", example = "")
    private String deliveryType;
    /**
     * 收货人名称
     */
    @ApiModelProperty(value = "收货人名称", example = "")
    private String deliveryName;
    /**
     * 收货人手机号
     */
    @ApiModelProperty(value = "收货人手机号", example = "")
    private String deliveryMobile;
    /**
     * 收货地址信息
     */
    @ApiModelProperty(value = "收货地址信息", example = "")
    private String deliveryAddress;
    /**
     * 确认发货时间
     */
    @ApiModelProperty(value = "确认发货时间", example = "")
    private Date confirmDeliveryTime;
    /**
     * 实际送达时间
     */
    @ApiModelProperty(value = "实际送达时间", example = "")
    private Date actualArriveTime;
    /**
     * 经度
     */
    @ApiModelProperty(value = "经度", example = "")
    private Double lng;
    /**
     * 纬度
     */
    @ApiModelProperty(value = "纬度", example = "")
    private Double lat;
    /**
     * 用户备注信息
     */
    @ApiModelProperty(value = "用户备注信息", example = "")
    private String userRemark;
    /**
     * 内部备注信息
     */
    @ApiModelProperty(value = "内部备注信息", example = "")
    private String innerRemark;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
