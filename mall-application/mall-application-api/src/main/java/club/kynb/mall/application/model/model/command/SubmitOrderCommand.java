package club.kynb.mall.application.model.model.command;


import club.kynb.mall.payment.constant.PayChannelEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;
import org.pizza.valid.EnumValid;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author kynb_club@163.com
 * @since 2021/7/1 9:09 下午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "SubmitOrderCommand对象", description = "提交订单命令")
@Accessors(chain = true)
public class SubmitOrderCommand extends Command {

    @ApiModelProperty(value = "内部订单编号", example = "454158125549748224")
    @NotBlank(message = "内部订单编号不能为空")
    private String innerOrderNo;

    @ApiModelProperty(value = "用户优惠券ID", example = "2")
    private Long userCouponId;

    @ApiModelProperty(value = "收货地址ID", example = "5")
    @NotNull(message = "收货地址ID不能为空")
    private Long shippingAddressId;

    @ApiModelProperty(value = "订单备注信息（用户）", example = "要新鲜要新新鲜鲜")
    private String userRemark;

    @ApiModelProperty(value = "支付方式:WECHAT、ALIPAY、CREDIT", example = "WECHAT")
    @EnumValid(name = "支付方式", target = PayChannelEnum.class)
    private String payChannel;




}
