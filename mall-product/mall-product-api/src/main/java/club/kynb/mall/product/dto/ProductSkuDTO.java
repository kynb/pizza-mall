package club.kynb.mall.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品sku表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "ProductSkuDTO对象", description = "商品sku表")
@Accessors(chain = true)
public class ProductSkuDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ApiModelProperty(value = "", example = "")
    private String id;
    /**
     * 
     */
    @ApiModelProperty(value = "", example = "")
    private Long spuId;
    /**
     * 
     */
    @ApiModelProperty(value = "", example = "")
    private String skuCode;
    /**
     * 
     */
    @ApiModelProperty(value = "", example = "")
    private String skuOutCode;
    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称", example = "")
    private String skuName;
    /**
     * 商品价格
     */
    @ApiModelProperty(value = "商品价格", example = "")
    private BigDecimal skuPrice;
    /**
     * 计价单位
     */
    @ApiModelProperty(value = "计价单位", example = "")
    private String skuUnit;
    /**
     * 重量(g)
     */
    @ApiModelProperty(value = "重量(g)", example = "")
    private BigDecimal skuWeight;
    /**
     * 
     */
    @ApiModelProperty(value = "", example = "")
    private String skuImage;
    /**
     * 状态 0初始化 1上架 2下架
     */
    @ApiModelProperty(value = "状态 0初始化 1上架 2下架", example = "")
    private Integer skuStatus;
    /**
     * 销售数量
     */
    @ApiModelProperty(value = "销售数量", example = "")
    private Integer skuSaleNum;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序", example = "")
    private Integer sequence;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
