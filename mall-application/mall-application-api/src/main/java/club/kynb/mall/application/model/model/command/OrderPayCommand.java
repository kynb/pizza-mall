package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author kynb_club@163.com
 * @since 2020/12/28 9:00 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "OrderPayCommand对象", description = "订单支付命令")
@Accessors(chain = true)
public class OrderPayCommand extends Command {

    @NotBlank(message = "订单号不能为空")
    @ApiModelProperty(value = "订单号", example = "OR1212212122", required = true)
    private String innerOrderNo;

    @NotBlank(message = "订单类型")
    @ApiModelProperty(value = "订单类型(PURCHASE:采购订单,BILL:账单)", example = "PURCHASE", required = true)
    private String orderType;


    @NotNull(message = "应付金额不能为空")
    @ApiModelProperty(value = "应付金额", example = "100.01", required = true)
    private BigDecimal orderAmount;
    /**
     * 支付方式
     */
    @NotEmpty(message = "请选择支付方式")
    @ApiModelProperty(value = "支付方式", example = "WECHAT", required = true)
    private String payChannel;

}
