package club.kynb.mall.basic.service.impl;

import club.kynb.mall.basic.api.ISmsService;
import club.kynb.mall.basic.constant.SmsScene;
import club.kynb.mall.basic.gateway.SmsGateway;
import cn.hutool.core.util.RandomUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.exception.Errors;
import org.springframework.stereotype.Service;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 2:44 下午
 */
@Slf4j
@Service
@AllArgsConstructor
public class SmsServiceImpl implements ISmsService {
    private final SmsGateway smsGateway;

    @Override
    public String smsSend(String phone, Integer scene) {
        //默认返回 111111
        if (true) {
            return "111111";
        }
        final String code = RandomUtil.randomNumbers(6);
        try {
            smsGateway.send(phone, SmsScene.ofScene(scene).getSmsTemplateId(), code);
        } catch (Exception e) {
            log.error("短信发送异常：", e);
            throw Errors.SYSTEM.exception("短信发送失败");
        }
        return code;
    }
}
