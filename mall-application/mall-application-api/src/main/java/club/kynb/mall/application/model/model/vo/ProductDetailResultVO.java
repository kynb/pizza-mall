package club.kynb.mall.application.model.model.vo;

import club.kynb.mall.product.dto.ProductSpuDetailDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.VO;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Data
@ApiModel(value = "ProductDetailResult对象", description = "商品详情对象")
@Accessors(chain = true)
public class ProductDetailResultVO implements VO {

    @ApiModelProperty(value = "商品信息", example = "")
    private ProductSpuDetailDTO spu;
    @ApiModelProperty(value = "相关商品列表", example = "")
    private List<ProductSpuDetailDTO> relatedList;
    @ApiModelProperty(value = "推荐商品列表", example = "")
    private List<ProductSpuDetailDTO> recommendList;
}
