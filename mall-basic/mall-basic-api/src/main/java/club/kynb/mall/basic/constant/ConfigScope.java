package club.kynb.mall.basic.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 2:53 下午
 */
@Getter
@AllArgsConstructor
public enum ConfigScope {
    /**
     * APP
     */
    APP

}
