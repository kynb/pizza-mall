package club.kynb.mall.basic.api;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 2:42 下午
 */
public interface ISmsService {
    String smsSend(String phone,Integer scene);
}
