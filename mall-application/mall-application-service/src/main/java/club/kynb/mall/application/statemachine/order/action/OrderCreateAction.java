package club.kynb.mall.application.statemachine.order.action;

import club.kynb.mall.application.context.OrderContext;
import club.kynb.mall.application.context.OrderSubmitContext;
import club.kynb.mall.application.flow.order.*;
import club.kynb.mall.order.constant.OrderEventEnum;
import club.kynb.mall.order.constant.OrderStatusEnum;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.pattern.filter.FilterChain;
import org.pizza.pattern.filter.FilterChainFactory;
import org.pizza.statemachine.Action;
import org.springframework.stereotype.Component;

/**
 * @author kynb_club@163.com
 * @since 2021/7/9 11:18 上午
 */
@Slf4j
@Component
@AllArgsConstructor
public class OrderCreateAction implements Action<OrderStatusEnum, OrderEventEnum, OrderContext> {

    @Override
    public void execute(OrderStatusEnum from, OrderStatusEnum to, OrderEventEnum on, OrderContext orderContext) {
        final FilterChain<OrderSubmitContext> filterChain = this.buildOrderSubmitChain();
        filterChain.doFilter((OrderSubmitContext) orderContext);
    }

    private FilterChain<OrderSubmitContext> buildOrderSubmitChain() {
        return FilterChainFactory.buildFilterChain(
                LoadPreOrderCacheFilter.class
                , CheckShippingAddressFilter.class
                , CheckWarehouseStockFilter.class
                , CheckUserCouponFilter.class
                , CalculateSubmitAmountFilter.class
                , CheckSubmitOrderAmountFilter.class
                , DoSubmitOrderFilter.class
                , SubmitOrderSuccessFilter.class
        );
    }
}
