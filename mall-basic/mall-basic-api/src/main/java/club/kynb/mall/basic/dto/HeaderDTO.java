package club.kynb.mall.basic.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

/**
 * Header信息表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-06-26
 */

@Data
@ApiModel(value = "HeaderDTO对象", description = "请求头基础信息表")
@Accessors(chain = true)
public class HeaderDTO implements DTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "客户端ip", example = "121.484963")
    private String clientIp;
    /**
     * 经度
     */
    @ApiModelProperty(value = "经度值", example = "121.484963")
    private String longitude;
    /**
     * 纬度
     */
    @ApiModelProperty(value = "纬度值", example = "29.883341")
    private String latitude;
    /**
     * 设备唯一标识
     */
    @ApiModelProperty(value = "设备唯一码", example = "")
    private String deviceId;
    /**
     * 设备类型 IOS、ANDROID、H5
     */
    @ApiModelProperty(value = "设备类型 IOS、ANDROID、H5", example = "IOS")
    private String deviceType;

    /**
     * 客户端类型 例如：PC,APP
     */
    @ApiModelProperty(value = "客户端类型 例如：PC,APP",example = "PC")
    private String clientType;
    /**
     * 应用版本
     */
    @ApiModelProperty(value = "应用版本", example = "1.0.0")
    private String appVersion;
    /**
     * apiVersion 应用版本 1.0
     */
    @ApiModelProperty(value = "应用版本", example = "1.0")
    private String apiVersion;


}
