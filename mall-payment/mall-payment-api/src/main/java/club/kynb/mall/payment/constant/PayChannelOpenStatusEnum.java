package club.kynb.mall.payment.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

/**
 * 开启状态 0关闭 1开启
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Getter
@AllArgsConstructor
public enum PayChannelOpenStatusEnum implements EnumValidI {

    CLOSE(0,"关闭"),
    OPEN(1,"开启"),
    ;

    private Integer status;
    private String desc;

    @Override
    public String code() {
        return String.valueOf(this.status);
    }

    @Override
    public String message() {
        return this.desc;
    }


}
