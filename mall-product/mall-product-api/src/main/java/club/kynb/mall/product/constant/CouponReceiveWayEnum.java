package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Arrays;
import java.util.Optional;

/**
 * 活动分类 1 精选 2 特价促销 3丰盛午餐 4 红叶午餐
 * @author kynb_club@163.com
 * @since 2021/6/24 2:53 下午
 */
@Getter
@AllArgsConstructor
public enum CouponReceiveWayEnum implements EnumValidI {

    ONE(1,"主动领取"),
    TWO(2,"系统发放"),
    ;

    private final Integer way;
    private final String desc;

    @Override
    public String code() {
        return String.valueOf(this.way);
    }

    @Override
    public String message() {
        return this.desc;
    }

    public static Optional<CouponReceiveWayEnum> get(Integer way){
        return Arrays.stream(CouponReceiveWayEnum.values())
                .filter(item-> item.getWay().equals(way))
                .findFirst();
    }
}
