package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

/**
 * 商品信息表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-07-06
 */

@Data
@ApiModel(value = "PreviewOrderSkuDTO对象", description = "预览订单SKU信息")
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public  class PreviewOrderSkuDTO implements DTO {
    @ApiModelProperty(value = "skuId", example = "1")
    private String id;
    /**
     * spuId
     */
    @ApiModelProperty(value = "spuId", example = "")
    private String spuId;
    /**
     * sku名称
     */
    @ApiModelProperty(value = "sku名称", example = "3斤")
    private String skuName;
    /**
     * 计价单位
     */
    @ApiModelProperty(value = "计价单位", example = "斤")
    private String skuUnit;
    /**
     * 加购数量
     */
    @ApiModelProperty(value = "商品数量", example = "3")
    private Integer skuNum;
    /**
     * sku价格
     */
    @ApiModelProperty(value = "sku价格", example = "")
    private PreviewOrderSkuPriceDTO skuPriceDTO;
}
