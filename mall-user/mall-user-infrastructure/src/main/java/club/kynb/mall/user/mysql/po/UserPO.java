package club.kynb.mall.user.mysql.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.mybatis.plus.base.BaseEntity;


/**
 * 用户基础信息表持久实体类
 * @author kynb_club@163.com
 * @since 2021-06-26
 */

@Data
@Accessors(chain = true)
@TableName("mall_user")
@EqualsAndHashCode(callSuper = true)
public class UserPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID(userId)
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 电话
     */
    private String phone;

    private String nickname;
    /**
     * 密码MD5(密码+盐)
     */
    private String password;
    /**
     * 盐
     */
    private String salt;
    /**
     * 状态枚举(FREEZE、NORMAL)
     */
    private String status;
    /**
     * 类型枚举(CUSTOMER、ADMINISTRATOR)
     */
    private String type;
    /**
     * 当前token
     */
    private String currentToken;
    /**
     * 扩展字段
     */
    private String extension;


}
