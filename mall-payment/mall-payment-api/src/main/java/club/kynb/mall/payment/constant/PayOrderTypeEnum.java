package club.kynb.mall.payment.constant;
/** 
* Description: 	订单类型
* @author 
* @version 1.0.0 2020年8月25日 下午5:46:38
*/

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Optional;
import java.util.stream.Stream;

@Getter
@AllArgsConstructor
public enum PayOrderTypeEnum implements EnumValidI {

	NORMAL("NORMAL", "普通订单", "PO"),
	;
	
	private final String code;
	private final String desc;
	private final String prefix;

	@Override
	public String code() {
		return this.code;
	}

	@Override
	public String message() {
		return this.desc;
	}

	@JsonIgnore
	public static Optional<PayOrderTypeEnum> get(String code) {
		return Stream.of(PayOrderTypeEnum.values()).filter(i -> i.getCode().equalsIgnoreCase(code)).findFirst();
	}
}
