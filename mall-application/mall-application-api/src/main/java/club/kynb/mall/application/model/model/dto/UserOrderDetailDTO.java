package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;

/**
 * 订单详情表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-07-07
 */

@Data
@ApiModel(value = "UserOrderDetailDTO对象", description = "用户订单详情对象")
@Accessors(chain = true)
public class UserOrderDetailDTO implements DTO {
    private static final long serialVersionUID = 1L;
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量", example = "")
    private Integer skuNum;
    /**
     * 原商品总金额
     */
    @ApiModelProperty(value = "原商品总金额", example = "")
    private BigDecimal totalAmount;
    /**
     * 分摊优惠后总金额
     */
    @ApiModelProperty(value = "分摊优惠后总金额", example = "")
    private BigDecimal shareAmount;
    /**
     * 下单时sku的快照信息
     */
    @ApiModelProperty(value = "下单时sku的信息", example = "")
    private UserOrderSpuDTO userOrderSpuDTO;

    @Data
    @ApiModel(value = "UserOrderSpuDTO对象", description = "用户订单spu信息表")
    @Accessors(chain = true)
    public static class UserOrderSpuDTO implements DTO {
        private static final long serialVersionUID = 1L;
        /**
         *
         */
        @ApiModelProperty(value = "", example = "")
        private String id;
        /**
         * 商品名称
         */
        @ApiModelProperty(value = "商品名称", example = "")
        private String spuName;
        /**
         * 列表展示图片
         */
        @ApiModelProperty(value = "商品图片", example = "")
        private String iconUrl;
        /**
         * 商品图片
         */
        @ApiModelProperty(value = "商品图片", example = "")
        private String spuImgs;
        /**
         * 商品价格
         */
        @ApiModelProperty(value = "商品价格", example = "")
        private PriceDTO priceInfo;
        /**
         * sku
         */
        @ApiModelProperty(value = "规格明细")
        private UserOrderSkuDTO sku;
    }

    @Data
    @ApiModel(value = "UserOrderSkuDTO对象", description = "用户订单商品sku信息")
    @Accessors(chain = true)
    public static class UserOrderSkuDTO implements DTO {
        private static final long serialVersionUID = 1L;
        /**
         *
         */
        @ApiModelProperty(value = "", example = "")
        private String id;
        /**
         * spuId
         */
        @ApiModelProperty(value = "spuId", example = "")
        private String spuId;
        /**
         * sku名称
         */
        @ApiModelProperty(value = "sku名称", example = "")
        private String skuName;
        /**
         * 计价单位
         */
        @ApiModelProperty(value = "计价单位", example = "")
        private String skuUnit;
        /**
         * 图片
         */
        @ApiModelProperty(value = "图片", example = "")
        private String skuImage;
        /**
         * 库存
         */
        @ApiModelProperty(value = "库存", example = "",notes = "库存数量")
        private Integer skuNum;

        @ApiModelProperty(value = "等级价格", example = "",notes = "")
        private PriceDTO priceInfo;
    }
}
