package club.kynb.mall.application.constant;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 1:49 下午
 */
public interface LockKeyConst {
    /**
     * 添加购物车
     */
    String ADD_SHOPPING_CART = "ADD_SHOPPING_CART";

    String ADD_SUMMARY_BILL = "ADD_SUMMARY_BILL";

    String SUBMIT_ORDER = "SUBMIT_ORDER";
}
