package club.kynb.mall.order.mysql.po;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import org.pizza.mybatis.plus.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 订单详情表持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_order_detail")
@EqualsAndHashCode(callSuper = true)
public class OrderDetailPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 内部订单编号
     */
    private String innerOrderNo;
    /**
     * 商品id
     */
    private Long spuId;
    /**
     * skuid
     */
    private Long skuId;
    /**
     * 商品数量
     */
    private Integer skuNum;
    /**
     * 退款数量
     */
    private Integer refundNum;
    /**
     * 原商品总金额
     */
    private BigDecimal totalAmount;
    /**
     * 分摊优惠后总金额
     */
    private BigDecimal shareAmount;
    /**
     * 下单时sku的快照信息
     */
    private String skuSnapshoot;
    /**
     * 扩展字段
     */
    private String extension;


}
