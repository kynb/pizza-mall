package club.kynb.mall.user.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.util.Date;

/**
 * 客服表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-08-03
 */

@Data
@ApiModel(value = "AdministratorDTO对象", description = "管理员表")
@Accessors(chain = true)
public class AdministratorDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID(userId)
     */
    @ApiModelProperty(value = "主键ID(userId)", example = "")
    private Long id;
    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码", example = "")
    private String phone;
    /**
     * 真实姓名
     */
    @ApiModelProperty(value = "真实姓名", example = "")
    private String realName;
    /**
     * 密码MD5(密码+盐)
     */
    @ApiModelProperty(value = "密码MD5(密码+盐)", example = "")
    private String password;
    /**
     * 状态枚举(FREEZE、NORMAL)
     */
    @ApiModelProperty(value = "状态枚举(FREEZE、NORMAL)", example = "")
    private String status;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
