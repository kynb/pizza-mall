package club.kynb.mall.application.service;

import club.kynb.mall.application.assembler.MallCommonAssembler;
import club.kynb.mall.application.context.MallUserContext;
import club.kynb.mall.application.model.model.dto.UserCouponTemplateDTO;
import club.kynb.mall.application.model.model.dto.UserReceiveCouponDTO;
import club.kynb.mall.application.model.model.command.ReceiveCouponCommand;
import club.kynb.mall.product.api.ICouponService;
import club.kynb.mall.product.dto.CouponTemplateDTO;
import club.kynb.mall.product.dto.UserCouponDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/6/29 12:55 下午
 */
@Slf4j
@Service
@AllArgsConstructor
public class CouponApplicationService {
    private final ICouponService iCouponService;

    public List<UserCouponTemplateDTO> couponList() {
        final MallUserContext mallUserContext = MallUserContext.get(true);
        final List<CouponTemplateDTO> templateList = iCouponService.drawCouponTemplateList();
        final List<UserCouponDTO> userCouponList = iCouponService.userCouponList(mallUserContext.getUserDTO().getId(), -1);
        return MallCommonAssembler.assembleUserCouponTemplateDTO(userCouponList, templateList);
    }

    public void receiveCoupon(ReceiveCouponCommand command) {
        final MallUserContext mallUserContext = MallUserContext.get(true);
        iCouponService.receiveCoupon(mallUserContext.getUserDTO().getId(), command.getCouponTemplateId());
    }


    public List<UserReceiveCouponDTO> userCouponList(Integer status) {
        final MallUserContext mallUserContext = MallUserContext.get(true);
        final List<UserCouponDTO> userCouponList = iCouponService.userCouponList(mallUserContext.getUserDTO().getId(), status);
        final List<CouponTemplateDTO> templateList = iCouponService.allCouponTemplateList();
        return MallCommonAssembler.assembleUserReceiveCouponDTO(userCouponList, templateList);
    }
}
