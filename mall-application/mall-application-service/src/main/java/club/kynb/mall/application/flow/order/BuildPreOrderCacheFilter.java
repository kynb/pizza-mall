package club.kynb.mall.application.flow.order;

import club.kynb.mall.application.context.OrderPreviewContext;
import club.kynb.mall.application.model.model.dto.PreviewOrderDTO;
import club.kynb.mall.application.model.model.vo.OrderPreviewResultVO;
import club.kynb.mall.application.repository.OrderRepository;
import club.kynb.mall.product.dto.ProductSpuDetailDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.pattern.filter.Filter;
import org.pizza.pattern.filter.FilterInvoker;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author kynb_club@163.com
 * @since 2021/7/5 9:02 下午
 */
@Slf4j
@Component
@AllArgsConstructor
public class BuildPreOrderCacheFilter implements Filter<OrderPreviewContext> {
    private final OrderRepository orderRepository;

    @Override
    public void doFilter(OrderPreviewContext orderPreviewContext, FilterInvoker<OrderPreviewContext> filterInvoker) {
        log.info("构建预览订单缓存过滤器 start");
        final OrderPreviewResultVO orderPreviewResultVO = orderPreviewContext.getOrderPreviewResultVO();
        final String innerOrderNo = orderPreviewResultVO.getPreviewOrderDTO().getInnerOrderNo();
        final PreviewOrderDTO previewOrderDTO = orderPreviewResultVO.getPreviewOrderDTO();
        final List<ProductSpuDetailDTO> filteredSpuList = orderPreviewContext.getFilteredSpuList();
        orderRepository.savePreOrder(innerOrderNo, previewOrderDTO);
        orderRepository.savePreOrderSpuList(innerOrderNo,filteredSpuList);
        log.info("构建预览订单缓存过滤器 end");
        filterInvoker.invoke(orderPreviewContext);
    }
}
