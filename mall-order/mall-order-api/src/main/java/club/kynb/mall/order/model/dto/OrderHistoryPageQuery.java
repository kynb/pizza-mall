package club.kynb.mall.order.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.page.PageQuery;


/**
 * @author kynb_club@163.com
 * @since 2021/7/8 9:23 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "OrderHistoryPageQuery对象", description = "历史订单分页查询")
@Accessors(chain = true)
public class OrderHistoryPageQuery extends PageQuery {

    @ApiModelProperty(value = "(不传即全部，多个用逗号拼接，例如：UNPAID,UN_DELIVERY,UN_RECEIVE)订单提交状态枚举 -> UNPAID:待支付、" +
            "UN_DELIVERY:待发货（同已付款）、" +
            "UN_RECEIVE:待收货（同已发货）、" +
            "FINISHED:已完成（同待评价）、" +
            "CANCELLED:已取消（同已关闭）、" +
            "WAITING_REFUND:待退款（同退款中）、" +
            "AGREE_REFUND:已退款", example = "UNPAID,UN_DELIVERY,UN_RECEIVE"
    )
    private String orderStatusString;


    @ApiModelProperty(value = "用户ID", example = "4496", hidden = true)
    private Long userId;

}
