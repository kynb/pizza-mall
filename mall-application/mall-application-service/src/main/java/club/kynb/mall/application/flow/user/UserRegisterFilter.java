package club.kynb.mall.application.flow.user;

import club.kynb.mall.application.context.AuthenticationContext;
import club.kynb.mall.application.model.model.command.AuthenticationCommand;
import club.kynb.mall.application.model.model.command.SmsRegistryCommand;
import club.kynb.mall.application.model.model.event.CustomerCreatedEvent;
import club.kynb.mall.basic.constant.ClientType;
import club.kynb.mall.user.api.IUserService;
import club.kynb.mall.user.constant.UserStatus;
import club.kynb.mall.user.constant.UserType;
import club.kynb.mall.user.model.dto.UserDTO;
import cn.hutool.crypto.SecureUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.exception.Errors;
import org.pizza.event.publish.EventBus;
import org.pizza.pattern.filter.Filter;
import org.pizza.pattern.filter.FilterInvoker;
import org.pizza.util.Checker;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author kynb_club@163.com
 * @since 2020/12/18 1:48 下午
 * 用户注册过滤器
 */
@Slf4j
@Component
@AllArgsConstructor
public class UserRegisterFilter implements Filter<AuthenticationContext> {
    private final IUserService userService;
    private final EventBus eventBus;

    @Override
    public void doFilter(AuthenticationContext authenticationContext, FilterInvoker<AuthenticationContext> filterInvoker) {
        log.info("用户注册过滤器 start");
        AuthenticationCommand command = authenticationContext.getAuthenticationCommand();
        Checker.ifNotThrow(command instanceof SmsRegistryCommand
                , (b) -> log.error("流程配置有误，当前过滤器无法处理：{}", command.getClass().getSimpleName())
                , () -> Errors.BIZ.exception("流程配置有误，请联系开发者~"));
        SmsRegistryCommand smsRegistryCommand =(SmsRegistryCommand)command;
        UserDTO userDTO = initUser(smsRegistryCommand);
        final UserDTO user = userService.userRegister(userDTO);
        eventBus.publishEvent(CustomerCreatedEvent.builder()
                .userId(user.getId())
                .clientType(smsRegistryCommand.getClientType())
                .clientIp(smsRegistryCommand.getClientIp())
                .build());
        authenticationContext.setUser(user);
        log.info("用户注册过滤器 end");
        filterInvoker.invoke(authenticationContext);
    }

    private UserDTO initUser(SmsRegistryCommand smsRegistryCommand) {
        UserDTO userDTO=new UserDTO();
        userDTO.setPhone(smsRegistryCommand.getPhone());
        userDTO.setSalt(UUID.randomUUID().toString());
        userDTO.setPassword(SecureUtil.md5(smsRegistryCommand.getPassword() + userDTO.getSalt()));
        userDTO.setStatus(UserStatus.NORMAL.name());
        userDTO.setType(UserType.CUSTOMER.name());
        userDTO.setExtension(ClientType.APP.name());
        return userDTO;
    }


}
