package club.kynb.mall.payment;


import org.pizza.mybatis.plus.generate.CodeConfig;
import org.pizza.mybatis.plus.generate.DDDPlusCodeGenerator;

import java.util.HashMap;

/**
 * @author kynb_club@163.com
 * @since 2020/11/19 9:37 上午
 */
public class PaymentCodeGenerator {
    /**
     * 代码所在服务名
     */
    public static String SERVICE_NAME = "mall-payment";
    /**
     * 代码生成的包名
     */
    public static String PACKAGE_NAME = "club.kynb.mall";
    /**
     * 后端代码生成路径
     */
    public static String PACKAGE_DIR = ".code";
    /**
     * 需要去掉的表前缀
     */
    public static String[] TABLE_PREFIX = {"mall_"};
    /**
     * 返回要生成的包名和其对应的所在领域
     * key->表名 value->所在领域
     * @return
     */
    private static HashMap<String, String> includeTables() {
        HashMap<String, String> map = new HashMap<>();
        //key->表名 value->所在领域
        map.put("mall_pay_channel","payment");
        map.put("mall_pay_trade_order","payment");

        return map;
    }

    public static void main(String[] args) {
        DDDPlusCodeGenerator generator = new DDDPlusCodeGenerator();
        generator.setServiceName(SERVICE_NAME);
        generator.setPackageName(PACKAGE_NAME);
        generator.setTablePrefix(TABLE_PREFIX);
        //覆盖项目内的代码、会覆盖mapper.java、mapper.xml、po对象
        generator.setTableDomainMapping(includeTables());
        generator.setPackageDir(PACKAGE_DIR);
        generator.run(new CodeConfig()
                .setUrl("jdbc:mysql://127.0.0.1:3306/pizza-mall?useUnicode=true&characterEncoding=UTF-8&useSSL=true&autoReconnect=true&serverTimezone=Asia/Shanghai")
                .setDriverName("com.mysql.cj.jdbc.Driver")
                .setUsername("root")
                .setPassword("root")
                .setAuthor("kynb_club@163.com"));
    }
}
