# 自定义接口文档

##  请求头部通用

- longitude     经度  `121.484963`
- latitude      纬度  `29.883341`
- deviceId      设备唯一标识 `122312`
- deviceType    客户端类型 `IOS、ANDROID、H5`
- appVersion    应用版本  `1.0.0`
- apiVersion    应用版本  `1.0`
- accessToken   访问令牌   `123456789`


## 订单状态枚举
NONE,           //无状态

INIT,           //初始化

UNPAID,         //待支付

UN_DELIVERY,    //待发货（同已付款）

UN_RECEIVE,     //待收货（同已发货）

FINISHED,       //已完成（同待评价）终结状态

CANCELLED,      //已取消（同已关闭）终结状态

WAITING_REFUND, //待退款（同退款中）

REFUNDED,       //已退款 终结状态