package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import javax.validation.constraints.NotBlank;

/**
 * @author kynb_club@163.com
 * @since 2021/7/1 9:09 下午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "RefundOrderCommand对象", description = "订单申请退款命令")
@Accessors(chain = true)
public class RefundOrderCommand extends Command {

    @ApiModelProperty(value = "内部订单编号", example = "454158125549748224")
    @NotBlank(message = "内部订单编号不能为空")
    private String innerOrderNo;

    @ApiModelProperty(value = "退款原因描述", example = "7天无理由退款")
    @NotBlank(message = "退款原因描述")
    private String refundDesc;



}
