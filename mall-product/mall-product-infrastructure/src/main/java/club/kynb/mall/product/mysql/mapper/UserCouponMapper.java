package club.kynb.mall.product.mysql.mapper;

import club.kynb.mall.product.mysql.po.UserCouponPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * 用户优惠券表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */
@Mapper
public interface UserCouponMapper extends BaseMapper<UserCouponPO> {


}
