package club.kynb.mall.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Arrays;
import java.util.Optional;

/**
 * 订单状态
 * @author kynb_club@163.com
 * @since  2021/7/8
 */
@Getter
@AllArgsConstructor
public enum OrderStatusEnum implements EnumValidI {
    NONE,           //无状态
    INIT,           //初始化
    UNPAID,         //待支付
    UN_DELIVERY,    //待发货（同已付款）
    UN_RECEIVE,     //待收货（同已发货）
    FINISHED,       //已完成（同待评价）终结状态
    CANCELLED,      //已取消（同已关闭）终结状态
    WAITING_REFUND, //待退款（同退款中）
    REFUNDED,       //已退款 终结状态
    ;

    @Override
    public String code() {
        return this.name();
    }

    @Override
    public String message() {
        return this.name();
    }

    public static Optional<OrderStatusEnum> get(String name){
        return Arrays.stream(OrderStatusEnum.values())
                .filter(item-> item.code().equals(name))
                .findFirst();
    }
}
