package club.kynb.mall.user.api;


import club.kynb.mall.user.model.dto.UserDTO;

import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/20 4:59 下午
 */
public interface IUserService {

    UserDTO userRegister(UserDTO userDTO);

    Optional<UserDTO> getUserByPhone(String phone, String type);

    void updateUser(UserDTO user);

    void loginSuccess(UserDTO userDTO);

    Optional<UserDTO> getUserById(Long userId);
}
