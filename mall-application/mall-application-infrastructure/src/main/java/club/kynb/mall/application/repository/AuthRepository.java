package club.kynb.mall.application.repository;

import club.kynb.mall.application.context.MallUserContext;

import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 2:19 下午
 */
public interface AuthRepository {
    void saveVerificationCode(String key,String value,Long expire);
    Optional<String> getVerificationCode(String key);
    void clear(String key);
    void saveContext(String key, MallUserContext context);
    void deleteToken(String shortToken);
    Optional<MallUserContext> getContext(String token);
}
