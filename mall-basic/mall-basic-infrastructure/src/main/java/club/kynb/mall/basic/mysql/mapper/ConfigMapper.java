package club.kynb.mall.basic.mysql.mapper;

import club.kynb.mall.basic.mysql.po.ConfigPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * 配置表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2021-06-27
 */
@Mapper
public interface ConfigMapper extends BaseMapper<ConfigPO> {


}
