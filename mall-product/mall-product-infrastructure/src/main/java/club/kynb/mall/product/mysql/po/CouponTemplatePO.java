package club.kynb.mall.product.mysql.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.mybatis.plus.base.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 优惠券模板表持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_coupon_template")
@EqualsAndHashCode(callSuper = true)
public class CouponTemplatePO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 券名字（外部显示）
     */
    private String name;
    /**
     * 标题（外部显示）
     */
    private String title;
    /**
     * 描述信息（外部显示）
     */
    private String description;
    /**
     * 标签（外部显示）
     */
    private String tag;
    /**
     * 面值（单位：抵扣券：元；折扣券：百分号前的数值，0.1-99.9）
     */
    private BigDecimal couponValue;
    /**
     * 券类型（1：现金券、2：折扣券）
     */
    private Integer couponType;
    /**
     * 叠加使用，0：不允许叠加，1：允许同类叠加
     */
    private Integer useSuperimposedType;
    /**
     * 领取方式，1：主动领取，2：系统发放
     */
    private Integer receiveWay;
    /**
     * 发放数量，主动领取方式有效
     */
    private Integer releaseQuantity;
    /**
     * 剩余数量，主动领取方式有效
     */
    private Integer residueQuantity;
    /**
     * 领取开始时间，主动领取方式有效
     */
    private Date receiveStartTime;
    /**
     * 领取截止时间，主动领取方式有效
     */
    private Date receiveEndTime;
    /**
     * 生效时间
     */
    private Date effectiveTime;
    /**
     * 失效时间
     */
    private Date invalidTime;
    /**
     * 商品使用范围（1：指定商品，2：全部商品）
     */
    private Integer spuRange;
    /**
     * 限定的商品ID列表，指定商品时有效
     */
    private String spuIds;
    /**
     * 撤单返还 0：不返还，1：返还
     */
    private Integer returnQualification;
    /**
     * 使用金额门槛，达到门槛，才能使用券
     */
    private BigDecimal thresholdAmount;
    /**
     * 状态：0待生效，1进行中，2已结束，3禁用
     */
    private Integer status;
    /**
     * 内部备注
     */
    private String remark;
    /**
     * 扩展字段
     */
    private String extension;


}
