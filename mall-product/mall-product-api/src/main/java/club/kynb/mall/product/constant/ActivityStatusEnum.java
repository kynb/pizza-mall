package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

/**
 * 活动状态 0 禁用 1启用
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Getter
@AllArgsConstructor
public enum ActivityStatusEnum implements EnumValidI {

    DISABLE(0,"禁用"),
    USABLE(1,"启用"),
    ;

    private Integer type;
    private String desc;

    @Override
    public String code() {
        return String.valueOf(this.type);
    }

    @Override
    public String message() {
        return this.desc;
    }
}
