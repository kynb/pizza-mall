package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author kynb_club@163.com
 * @since 2020/12/18 9:35 上午
 */
@Data
@Accessors(chain = true)
public class AuthenticationCommand {

    @ApiModelProperty(value = "客户端类型（PC、APP、H5）", example = "APP", required = true)
    @NotBlank(message = "客户端类型不能为空")
    private String clientType;

    @ApiModelProperty(value = "客户端IP", example = "127.0.0.1", hidden = true)
    private String clientIp;

    @NotBlank(message = "手机号码不能为空")
    @Pattern(regexp = "^1\\d{10}$", message = "请输入11位的手机号码")
    @ApiModelProperty(value = "手机号码", example = "18058525327", required = true)
    private String phone;

}
