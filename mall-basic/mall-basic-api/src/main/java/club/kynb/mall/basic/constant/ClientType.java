package club.kynb.mall.basic.constant;


import org.pizza.valid.EnumValidI;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2020/12/8 2:44 下午
 */
public enum ClientType implements EnumValidI {
    APP;

    @Override
    public String code() {
        return this.name();
    }

    @Override
    public String message() {
        return "类型：" + this.name();
    }

    public static Optional<ClientType> mapping(String clientType) {
        return Arrays.stream(ClientType.values()).filter(ut -> ut.code().equals(clientType)).findFirst();
    }
}
