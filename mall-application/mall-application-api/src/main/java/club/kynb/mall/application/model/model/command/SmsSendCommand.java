package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;
import org.pizza.model.Command;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author kynb_club@163.com
 * @since 2020/12/28 9:00 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "SmsSendCommand对象", description = "短信验证码发送命令")
@Accessors(chain = true)
public class SmsSendCommand extends Command {

    @NotBlank(message = "手机号码不能为空")
    @Pattern(regexp = "^1\\d{10}$", message = "请输入11位的手机号码")
    @ApiModelProperty(value = "手机号码", example = "18058525327", required = true)
    private String phone;

    @NotNull(message = "场景枚举不能为空, 1注册 2登录 3找回密码")
    @ApiModelProperty(value = "场景枚举 1注册 2登录 3找回密码", example = "1", required = true)
    @Range(min = 1, max = 3, message = "场景枚举范围，1注册 2登录 3找回密码")
    private Integer scene;
}
