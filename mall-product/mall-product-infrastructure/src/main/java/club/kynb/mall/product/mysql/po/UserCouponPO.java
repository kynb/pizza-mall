package club.kynb.mall.product.mysql.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import org.pizza.mybatis.plus.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 用户优惠券表持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_user_coupon")
@EqualsAndHashCode(callSuper = true)
public class UserCouponPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 优惠券ID
     */
    private Long couponTemplateId;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 领取时间
     */
    private Date receiveTime;
    /**
     * 状态：0未使用，1已使用，2已过期
     */
    private Integer status;
    /**
     * 扩展字段
     */
    private String extension;


}
