package club.kynb.mall.application.constant;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 1:49 下午
 */
public interface AuthConst {

    /**
     * 短信验证码存储Key
     */
    String CAPTCHA = "CAPTCHA";
    /**
     * 短信验证码存储Key
     */
    String SMS_CODE = "SMS_CODE";
    /**
     * 认证短信频率
     */
    String AUTH_SMS_RATE = "AUTH_SMS_RATE";
    /**
     * 存储token前缀
     */
    String TOKEN_PREFIX = "TOKEN";
    /**
     * 短信发送频率60秒一次
     */
    long RATE = 60L;
    /**
     * 短信验证码过期时间
     */
    long SMS_EXPIRE = 60 * 15L;
    /**
     * 图片验证码过期时间
     */
    long CAPTCHA_EXPIRE = 60 * 20L;
}
