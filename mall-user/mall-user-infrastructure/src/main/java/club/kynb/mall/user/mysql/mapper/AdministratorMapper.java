package club.kynb.mall.user.mysql.mapper;

import club.kynb.mall.user.mysql.po.AdministratorPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 客服表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2021-08-03
 */
@Mapper
public interface AdministratorMapper extends BaseMapper<AdministratorPO> {

    @Delete("delete from mall_administrator where id = #{administratorId}")
    void deleteBy(@Param("administratorId") Long administratorId);
}
