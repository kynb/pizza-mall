package club.kynb.mall.application.context;

import club.kynb.mall.application.model.model.command.OrderPayCommand;
import club.kynb.mall.order.model.dto.OrderDetailDTO;
import club.kynb.mall.payment.dto.ext.PayRequestDTO;
import club.kynb.mall.payment.dto.ext.PayResultDTO;
import club.kynb.mall.payment.dto.third.PaySplitDTO;
import lombok.Data;
import org.pizza.model.Model;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/7/5 4:58 下午
 */
@Data
public class OrderPayContext implements Model{

    private OrderPayCommand command;

    private List<OrderDetailDTO> orderDetailDTOS;

    private PayRequestDTO payRequestDTO = new PayRequestDTO();

    private PayResultDTO payResultDTO;

    private List<PaySplitDTO> supplierSplitList;


}
