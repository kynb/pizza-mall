package club.kynb.mall.payment.mysql.mapper;

import club.kynb.mall.payment.mysql.po.PayRefundTradeOrderPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * 退款交易流水表 Mapper 接口
 *
 * @author fyh
 * @since 2021-06-29
 */
@Mapper
public interface PayRefundTradeOrderMapper extends BaseMapper<PayRefundTradeOrderPO> {


}
