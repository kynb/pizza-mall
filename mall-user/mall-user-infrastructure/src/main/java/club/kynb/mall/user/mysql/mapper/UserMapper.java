package club.kynb.mall.user.mysql.mapper;

import club.kynb.mall.user.mysql.po.UserPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 用户基础信息表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2021-06-24
 */
@Mapper
public interface UserMapper extends BaseMapper<UserPO> {

    @Delete("delete from mall_user where id = #{userId}")
    void deleteBy(@Param("userId") Long userId);
}
