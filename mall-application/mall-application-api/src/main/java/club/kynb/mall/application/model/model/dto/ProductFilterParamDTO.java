package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author kynb_club@163.com
 * @date: 2021/6/30
 */
@Data
@ApiModel(value = "ProductFilterParamDTO对象", description = "商品过滤参数")
@Accessors(chain = true)
public class ProductFilterParamDTO {

    @ApiModelProperty(value = "等级",example = "")
    private String level;

    public ProductFilterParamDTO level(String level){
        this.level = level;
        return this;
    }



}
