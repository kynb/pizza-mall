package club.kynb.mall.basic.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 2:53 下午
 */
@Getter
@AllArgsConstructor
public enum SmsScene {
    /**
     * 注册
     */
    REGISTRY(1, "SMS_123456"),
    /**
     * 登录
     */
    LOGIN(2, "SMS_123456"),
    /**
     * 找回密码
     */
    RETRIEVE_PWD(3, "SMS_123456");
    private final Integer scene;
    private final String smsTemplateId;

    public static SmsScene ofScene(Integer scene) {
        return Arrays.stream(SmsScene.values())
                .collect(Collectors.toMap(SmsScene::getScene, keywordType -> keywordType, (k1, k2) -> k1)).get(scene);
    }

}
