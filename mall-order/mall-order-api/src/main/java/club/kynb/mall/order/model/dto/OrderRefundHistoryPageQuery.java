package club.kynb.mall.order.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.page.PageQuery;


/**
 * @author kynb_club@163.com
 * @since 2021/7/8 9:23 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "OrderRefundHistoryPageQuery对象", description = "退货历史订单分页查询")
@Accessors(chain = true)
public class OrderRefundHistoryPageQuery extends PageQuery {

    @ApiModelProperty(value = "(不传即全部，退款状态  1审核通过 2审核失败 3进行中)", example = "1")
    private Integer status;

    @ApiModelProperty(value = "用户ID", example = "4496", hidden = true)
    private Long userId;

}
