package club.kynb.mall.application.flow.pay;

import club.kynb.mall.application.context.OrderPayContext;
import club.kynb.mall.payment.api.IPayService;
import club.kynb.mall.payment.dto.ext.PayRequestDTO;
import club.kynb.mall.payment.dto.ext.PayResultDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.pattern.filter.Filter;
import org.pizza.pattern.filter.FilterInvoker;
import org.springframework.stereotype.Component;

/**
 * @author kynb_club@163.com
 * @date: 2021/7/8
 */
@Slf4j
@Component
@AllArgsConstructor
public class PayInvokeFilter implements Filter<OrderPayContext> {

    private final IPayService payService;

    @Override
    public void doFilter(OrderPayContext orderPayContext, FilterInvoker<OrderPayContext> filterInvoker) {
        log.info("支付调用过滤器 start");
        PayRequestDTO payRequestDTO = orderPayContext.getPayRequestDTO();
        PayResultDTO resultDTO = payService.createPay(payRequestDTO);
        orderPayContext.setPayResultDTO(resultDTO);
        log.info("支付调用过滤器 end");
        filterInvoker.invoke(orderPayContext);
    }

}
