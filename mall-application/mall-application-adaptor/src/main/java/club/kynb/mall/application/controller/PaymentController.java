package club.kynb.mall.application.controller;

import club.kynb.mall.application.model.model.command.OrderPayCommand;
import club.kynb.mall.application.model.model.query.PayResultQuery;
import club.kynb.mall.application.model.model.vo.OrderPayResultVO;
import club.kynb.mall.application.service.PaymentApplicationService;
import club.kynb.mall.payment.dto.PayChannelDTO;
import club.kynb.mall.payment.dto.ext.PayResultDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.response.R;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author kynb_club@163.com
 * @date: 2021/6/29
 */
@Slf4j
@RestController
@RequestMapping("/pay")
@AllArgsConstructor
@Api(value = "", tags = "支付相关接口")
public class PaymentController {

    private final PaymentApplicationService paymentService;

    @GetMapping("/channels")
    @ApiOperation(value = "支付页-支付方式列表", notes = "支付方式列表")
    public R<List<PayChannelDTO>> listChannel(@ApiParam(name = "orderType", value = "订单类型(PURCHASE:普通采购订单，BILL:账单)", required = true)
                                                   @RequestParam(value = "orderType")String orderType) {
        return R.ok(paymentService.listOrderPayChannel(orderType));
    }

    @PostMapping("/create")
    @ApiOperation(value = "支付页-预支付", notes = "支付订单创建")
    public R<PayResultDTO> create(@RequestBody @Valid OrderPayCommand command) {
        return R.ok(paymentService.create(command));
    }

    @GetMapping("/query")
    @ApiOperation(value = "支付页-查询支付结果", notes = "订单支付结果查询")
    public R<OrderPayResultVO> query(@Valid PayResultQuery payResultQuery) {
        return R.ok(paymentService.query(payResultQuery));
    }



}
