package club.kynb.mall.payment.exec;

import club.kynb.mall.payment.dto.ext.PayRequestDTO;
import club.kynb.mall.payment.dto.ext.RefundRequestDTO;
import club.kynb.mall.payment.dto.ext.RefundResultDTO;
import club.kynb.mall.payment.dto.ext.TradeNotifyDTO;
import club.kynb.mall.payment.dto.third.PayExecutorQueryResultDTO;
import club.kynb.mall.payment.dto.third.PayExecutorResultDTO;
import club.kynb.mall.payment.mysql.po.PayTradeOrderPO;

/**
 * 支付执行
 *
 * @author kynb_club@163.com
 * @date: 2021/7/7
 */
public interface PayExecutorService {

    /**
     * 获取支付方式code
     * @return
     */
    String getChannel();
    /**
     * 创建预支付
     * @param dto
     * @return
     */
    PayExecutorResultDTO createPay(PayRequestDTO dto, PayTradeOrderPO tradeOrder);

    /**
     * 支付结果查询
     * @param tradeOrder
     * @return
     */
    PayExecutorQueryResultDTO query( PayTradeOrderPO tradeOrder);

    /**
     * 退款
     * @param dto
     * @return
     */
    RefundResultDTO refund(RefundRequestDTO dto);

    /**
     * 通知
     * @param dto
     */
    void notify(TradeNotifyDTO dto);
}
