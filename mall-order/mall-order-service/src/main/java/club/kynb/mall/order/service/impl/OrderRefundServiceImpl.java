package club.kynb.mall.order.service.impl;

import club.kynb.mall.order.api.IOrderRefundService;
import club.kynb.mall.order.constant.OrderRefundStatusEnum;
import club.kynb.mall.order.model.dto.OrderRefundDTO;
import club.kynb.mall.order.model.dto.OrderRefundHistoryPageQuery;
import club.kynb.mall.order.model.dto.OrderRefundPageQuery;
import club.kynb.mall.order.mysql.mapper.OrderRefundMapper;
import club.kynb.mall.order.mysql.po.OrderRefundPO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.exception.Errors;
import org.pizza.model.page.PageResult;
import org.pizza.mybatis.plus.support.Pages;
import org.pizza.util.Checker;
import org.pizza.util.Convertor;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/7/11 2:29 下午
 */
@Slf4j
@Service
@AllArgsConstructor
public class OrderRefundServiceImpl implements IOrderRefundService {
    private final OrderRefundMapper orderRefundMapper;

    @Override
    public void create(OrderRefundDTO orderRefund) {
        final OrderRefundPO refundPO = Convertor.convert(OrderRefundPO.class, orderRefund);
        final int insert = orderRefundMapper.insert(refundPO);
        Checker.ifNotThrow(insert > 0, () -> Errors.SYSTEM.exception("系统异常，持久化退款申请失败"));
    }

    @Override
    public void applyCancel(String refundNo) {
        OrderRefundPO orderRefundPO = orderRefundMapper.selectOne(Wrappers.<OrderRefundPO>lambdaQuery().eq(OrderRefundPO::getRefundNo, refundNo));
        Checker.ifNullThrow(orderRefundPO, () -> Errors.BIZ.exception("传入退款申请编号有误，申请不存在"));
        Checker.ifNotThrow(OrderRefundStatusEnum.PROCESSING.getStatus().equals(orderRefundPO.getStatus()), () -> Errors.BIZ.exception("退款申请状态已变更，请刷新重试"));
        orderRefundPO.setStatus(OrderRefundStatusEnum.CANCELED.getStatus());
        orderRefundMapper.updateById(orderRefundPO);
    }

    @Override
    public PageResult<OrderRefundDTO> orderRefundHistoryPageQuery(OrderRefundHistoryPageQuery pageQuery) {
        final LambdaQueryWrapper<OrderRefundPO> queryWrapper = Wrappers.<OrderRefundPO>lambdaQuery()
                .eq(OrderRefundPO::getUserId, pageQuery.getUserId())
                .eq(Objects.nonNull(pageQuery.getStatus()),OrderRefundPO::getStatus,pageQuery.getStatus())
                .orderByDesc(OrderRefundPO::getCreateTime);
        //mybatis page转换
        IPage<OrderRefundPO> pageParam = Pages.convert(pageQuery);
        //分页查询
        IPage<OrderRefundPO> page = orderRefundMapper.selectPage(pageParam, queryWrapper);
        final PageResult<OrderRefundDTO> pageResult = Pages.convert(page, OrderRefundDTO.class);
        return pageResult;
    }

    @Override
    public Optional<OrderRefundDTO> getRefundOrderBy(String refundNo) {
        OrderRefundPO orderPO = orderRefundMapper.selectOne(Wrappers.<OrderRefundPO>lambdaQuery().eq(OrderRefundPO::getRefundNo, refundNo));
        if (orderPO == null) {
            return Optional.empty();
        }
        return Optional.of(Convertor.convert(OrderRefundDTO.class, orderPO));
    }

    @Override
    public PageResult<OrderRefundDTO> orderRefundPage(OrderRefundPageQuery pageQuery) {
        final LambdaQueryWrapper<OrderRefundPO> queryWrapper = Wrappers.<OrderRefundPO>lambdaQuery()
                .eq(Objects.nonNull(pageQuery.getStatus()),OrderRefundPO::getStatus,pageQuery.getStatus())
                .orderByDesc(OrderRefundPO::getCreateTime);
        //mybatis page转换
        IPage<OrderRefundPO> pageParam = Pages.convert(pageQuery);
        //分页查询
        IPage<OrderRefundPO> page = orderRefundMapper.selectPage(pageParam, queryWrapper);
        final PageResult<OrderRefundDTO> pageResult = Pages.convert(page, OrderRefundDTO.class);
        return pageResult;
    }


    @Override
    public void orderRefundOperation(String refundNo,Integer status) {
        OrderRefundPO orderRefundPO = orderRefundMapper.selectOne(Wrappers.<OrderRefundPO>lambdaQuery().eq(OrderRefundPO::getRefundNo, refundNo));
        Checker.ifNullThrow(orderRefundPO, () -> Errors.BIZ.exception("传入退款申请编号有误，申请不存在"));
        orderRefundPO.setStatus(status);
        final int update = orderRefundMapper.updateById(orderRefundPO);
        Checker.ifNotThrow(update > 0, () -> Errors.SYSTEM.exception("系统异常，退款审核操作失败！"));

    }

}
