package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

/**
 * @author kynb_club@163.com
 * @since 2021/7/7 3:18 下午
 */
@Data
@ApiModel(value = "PreviewOrderDetailDTO对象", description = "预览订单详情信息")
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class PreviewOrderDetailDTO implements DTO {

    @ApiModelProperty(value = "预览订单SPU信息")
    private PreviewOrderSpuDTO previewOrderSpuDTO;
}
