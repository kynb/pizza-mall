package club.kynb.mall.payment.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

/**
 * 退款结果类型
 * @author kynb_club@163.com
 * @date: 2021/7/2
 */
@Getter
@AllArgsConstructor
public enum RefundResultStatusEnum implements EnumValidI {

    SUCCESS(1,"成功"),
    PAYING(2,"退款中"),
    ERROR(3,"退款失败"),
    ;

    private Integer status;
    private String desc;

    @Override
    public String code() {
        return String.valueOf(this.status);
    }

    @Override
    public String message() {
        return this.desc;
    }
}
