package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import javax.validation.constraints.NotBlank;

/**
 * @author kynb_club@163.com
 * @since 2021/7/1 9:09 下午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "RefundOrderCancelCommand对象", description = "取消订单申请退款命令")
@Accessors(chain = true)
public class RefundOrderCancelCommand extends Command {

    @ApiModelProperty(value = "退款申请编号", example = "454158125549748224")
    @NotBlank(message = "退款申请编号不能为空")
    private String refundNo;

    /**
     * 退款申请前的订单状态 见订单状态枚举
     */
    @ApiModelProperty(value = "退款申请前的订单状态 见订单状态枚举", example = "")
    @NotBlank(message = "订单原始状态不能为空")
    private String orderOriginalStatus;



}
