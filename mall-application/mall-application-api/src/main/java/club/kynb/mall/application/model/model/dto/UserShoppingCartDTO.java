package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/7/4 11:29 上午
 */
@Data
@ApiModel(value = "UserShoppingCartDTO对象", description = "用户购物车")
@Accessors(chain = true)
public class UserShoppingCartDTO {
    /**
     * 商品总价格
     */
    @ApiModelProperty(value = "商品总价格", example = "18.88")
    private BigDecimal totalAmount;
    /**
     * 购物车商品列表
     */
    @ApiModelProperty(value = "购物车商品列表", example = "")
    private List<UserShoppingCartItemDTO> itemList;
}
