package club.kynb.mall.application.util;

import cn.hutool.core.util.StrUtil;
import org.apache.commons.lang3.RandomUtils;

import java.util.Calendar;

/**
 * @author kynb_club@163.com
 * @date: 2021/7/13
 */
public class NoUtil {


    public static String generateNo(){
        return generateNo(StrUtil.EMPTY);
    }

    public static String generateNo(String prefix) {
        StringBuilder builder = new StringBuilder();
        builder.append(prefix);
        int seed = RandomUtils.nextInt(2, 4);
        builder.append(seed);

        Calendar calendar = Calendar.getInstance();
        int sum = 0;
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        builder.append(year)
                .append(String.format("%02d", month))
                .append(String.format("%02d", day))
                .append(String.format("%02d", hour))
                .append(String.format("%02d", minute))
                .append(String.format("%02d", second));

        int randomNum = RandomUtils.nextInt(1, 9999);

        sum = sum + year + month + day + hour + minute + second + randomNum;
        builder.append(String.format("%04d", randomNum))
                .append(sum % (second == 0 ? 1 : second) );
        return builder.toString();
    }
}
