package club.kynb.mall.product.mysql.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import org.pizza.mybatis.plus.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 商品分类表持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_product_category")
@EqualsAndHashCode(callSuper = true)
public class ProductCategoryPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 父ID
     */
    private Long parentId;
    /**
     * 分类名称
     */
    private String cateName;
    /**
     * 图片地址
     */
    private String imageUrl;
    /**
     * 图标
     */
    private String iconUrl;
    /**
     * 排序
     */
    private Integer sequence;
    /**
     * 扩展字段
     */
    private String extension;


}
