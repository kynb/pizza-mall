package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Arrays;
import java.util.Optional;

/**
 * 提供商类型 1 自营 2 供应商
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Getter
@AllArgsConstructor
public enum SpuSupplyTypeEnum implements EnumValidI {

    SELF(1,"自营"),
    SUPPLIER(2,"供应商"),
    ;

    private Integer type;
    private String desc;

    @Override
    public String code() {
        return String.valueOf(this.type);
    }

    @Override
    public String message() {
        return this.desc;
    }

    public static Optional<SpuSupplyTypeEnum> get(Integer type){
        return Arrays.stream(SpuSupplyTypeEnum.values())
                .filter(item-> item.getType().equals(type))
                .findFirst();
    }
}
