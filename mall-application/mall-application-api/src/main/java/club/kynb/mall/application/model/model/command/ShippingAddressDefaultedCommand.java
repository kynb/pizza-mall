package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import javax.validation.constraints.NotNull;

/**
 * @author kynb_club@163.com
 * @since 2020/12/28 9:00 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "ShippingAddressDefaultedCommand对象", description = "设置默认收货地址命令")
@Accessors(chain = true)
public class ShippingAddressDefaultedCommand extends Command {

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "收货地址ID", example = "")
    @NotNull(message = "收货地址ID不能为空")
    private Long id;

}
