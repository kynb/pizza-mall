package club.kynb.mall.payment.api;

import club.kynb.mall.payment.constant.PayClientTypeEnum;
import club.kynb.mall.payment.constant.PayOrderTypeEnum;
import club.kynb.mall.payment.dto.PayChannelDTO;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @date: 2021/6/29
 */
public interface IPayChannelService {

    /**
     * 开启的支付方式
     * @return
     */
    List<PayChannelDTO> listUsable(PayOrderTypeEnum orderType, PayClientTypeEnum clientType);
    

}
