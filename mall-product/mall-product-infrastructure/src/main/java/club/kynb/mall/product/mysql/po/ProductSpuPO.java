package club.kynb.mall.product.mysql.po;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import org.pizza.mybatis.plus.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 商品信息表持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_product_spu")
@EqualsAndHashCode(callSuper = true)
public class ProductSpuPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 二级分类
     */
    private String categoryIds;
    /**
     * 商品编码
     */
    private String spuCode;
    /**
     * 外部编码
     */
    private String spuOutCode;
    /**
     * 名称
     */
    private String spuName;
    /**
     * 关键词
     */
    private String spuKeywords;
    /**
     * 价格
     */
    private BigDecimal spuPrice;
    /**
     * 详情
     */
    private String spuDesc;
    /**
     * 图片
     */
    private String spuImgs;
    /**
     * 上架状态
     */
    private Integer spuOnSale;
    /**
     * 下架时间
     */
    private Date offSaleTime;
    /**
     * 销售数量
     */
    private Integer spuSaleNum;
    /**
     * 排序
     */
    private Integer sequence;
    /**
     * 扩展字段
     */
    private String extension;


}
