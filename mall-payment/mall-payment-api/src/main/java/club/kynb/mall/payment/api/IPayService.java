package club.kynb.mall.payment.api;

import club.kynb.mall.payment.dto.ext.*;
import club.kynb.mall.payment.dto.ext.PayQueryDTO;

/**
 * @author kynb_club@163.com
 * @date: 2021/7/2
 */
public interface IPayService {

    /**
     * 创建预支付
     * @param dto
     * @return
     */
    PayResultDTO createPay(PayRequestDTO dto);

    /**
     * 支付结果查询
     * @param query
     * @return
     */
    PayQueryResultDTO query(PayQueryDTO query);

    /**
     * 退款
     * @param dto
     * @return
     */
    RefundResultDTO refund(RefundRequestDTO dto);

    /**
     * 通知
     * @param dto
     */
    void notify(TradeNotifyDTO dto);
}
