package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author kynb_club@163.com
 * @since 2020/12/18 9:35 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "SmsAuthenticationCommand对象", description = "短信认证登录命令")
@Accessors(chain = true)
public class SmsAuthenticationCommand extends AuthenticationCommand {

    @Length(message = "短信验证码最少为6个字符", min = 6, max = 6)
    @NotBlank
    @ApiModelProperty(value = "短信验证码", example = "125226", required = true)
    private String smsVerificationCode;


}
