package club.kynb.mall.payment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付交易订单表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "PayTradeOrderDTO对象", description = "支付交易订单表")
@Accessors(chain = true)
public class PayTradeOrderDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ApiModelProperty(value = "", example = "")
    private Long id;
    /**
     * 原始订单类型
     */
    @ApiModelProperty(value = "原始订单类型", example = "")
    private String originOrderType;
    /**
     * 原始订单编号
     */
    @ApiModelProperty(value = "原始订单编号", example = "")
    private String originOrderNo;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", example = "")
    private Long userId;
    /**
     * 交易单流水
     */
    @ApiModelProperty(value = "交易单流水", example = "")
    private String tradeOrderNo;
    /**
     * 订单金额
     */
    @ApiModelProperty(value = "订单金额", example = "")
    private BigDecimal orderAmount;
    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", example = "")
    private Integer status;
    /**
     * 支付方式
     */
    @ApiModelProperty(value = "支付方式", example = "")
    private String payChannel;
    /**
     * 客户端类型
     */
    @ApiModelProperty(value = "客户端类型", example = "")
    private String clientType;
    /**
     * 实付金额
     */
    @ApiModelProperty(value = "实付金额", example = "")
    private BigDecimal payAmount;
    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间", example = "")
    private Date payTime;
    /**
     * 对接系统流水号
     */
    @ApiModelProperty(value = "对接系统流水号", example = "")
    private String payFlowNo;
    /**
     * 业务参数
     */
    @ApiModelProperty(value = "业务参数", example = "")
    private String bizParams;
    /**
     * 通知信息
     */
    @ApiModelProperty(value = "通知信息", example = "")
    private String notifyInfo;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
