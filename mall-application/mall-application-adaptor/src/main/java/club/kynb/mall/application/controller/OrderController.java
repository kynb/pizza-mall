package club.kynb.mall.application.controller;

import club.kynb.mall.application.model.model.command.*;
import club.kynb.mall.application.model.model.dto.UserOrderDTO;
import club.kynb.mall.application.model.model.vo.OrderPreviewResultVO;
import club.kynb.mall.application.model.model.vo.OrderSubmitResultVO;
import club.kynb.mall.application.service.OrderApplicationService;
import club.kynb.mall.order.model.dto.OrderHistoryPageQuery;
import club.kynb.mall.order.model.dto.OrderRefundDTO;
import club.kynb.mall.order.model.dto.OrderRefundHistoryPageQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.response.R;
import org.pizza.model.page.PageResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author kynb_club@163.com
 * @since 2021/7/4 2:19 下午
 */
@Slf4j
@RestController
@RequestMapping
@AllArgsConstructor
@Api(value = "", tags = "订单相关接口")
public class OrderController {

    private final OrderApplicationService orderApplicationService;

    @GetMapping("/order/preview")
    @ApiOperation(value = "订单确认页-订单预览", notes = "")
    public R<OrderPreviewResultVO> previewOrder() {
        return R.ok(orderApplicationService.previewOrder());
    }

    @PostMapping("/order/submit")
    @ApiOperation(value = "订单确认页-提交订单", notes = "")
    public R<OrderSubmitResultVO> submitOrder(@RequestBody @Valid SubmitOrderCommand command) {
        final OrderSubmitResultVO orderSubmitResultVO = orderApplicationService.submitOrder(command);
        return R.ok(orderSubmitResultVO);
    }

    @GetMapping("/order/history")
    @ApiOperation(value = "订单列表页-订单历史", notes = "")
    public R<PageResult<UserOrderDTO>> orderHistory(@Valid OrderHistoryPageQuery orderHistoryPageQuery) {
        final PageResult<UserOrderDTO> pageResult = orderApplicationService.orderHistoryPageQuery(orderHistoryPageQuery);
        return R.ok(pageResult);
    }

    @GetMapping("/order/detail/{innerOrderNo}")
    @ApiOperation(value = "订单列表页-订单详情", notes = "")
    public R<UserOrderDTO> orderDetail(@PathVariable("innerOrderNo") String innerOrderNo) {
        final UserOrderDTO userOrderDTO = orderApplicationService.orderDetail(innerOrderNo);
        return R.ok(userOrderDTO);
    }

    @PostMapping("/order/cancel")
    @ApiOperation(value = "订单列表页-取消订单", notes = "")
    public R<UserOrderDTO> orderCancel(@RequestBody @Valid CancelOrderCommand command) {
        orderApplicationService.orderCancel(command);
        return R.ok();
    }

    @PostMapping("/order/confirm")
    @ApiOperation(value = "订单列表页-确认收货", notes = "")
    public R<UserOrderDTO> orderConfirm(@RequestBody @Valid ConfirmOrderCommand command) {
        orderApplicationService.orderConfirm(command);
        return R.ok();
    }

    @PostMapping("/order/again")
    @ApiOperation(value = "订单列表页-再来一单", notes = "")
    public R<UserOrderDTO> orderAgain(@RequestBody @Valid AgainOrderCommand command) {
        orderApplicationService.orderAgain(command);
        return R.ok();
    }

    @PostMapping("/order/refund")
    @ApiOperation(value = "订单列表页-申请退款", notes = "")
    public R<UserOrderDTO> orderRefund(@RequestBody @Valid RefundOrderCommand command) {
        orderApplicationService.orderRefund(command);
        return R.ok();
    }

    @PostMapping("/order/refund/cancel")
    @ApiOperation(value = "订单列表页-取消申请退款", notes = "")
    public R<UserOrderDTO> orderRefundCancel(@RequestBody @Valid RefundOrderCancelCommand command) {
        orderApplicationService.orderRefundCancel(command);
        return R.ok();
    }

    @PostMapping("/order/refund/history")
    @ApiOperation(value = "退款订单列表页-退货/售后", notes = "")
    public R<PageResult<OrderRefundDTO>> orderRefundHistory(@RequestBody @Valid OrderRefundHistoryPageQuery pageQuery) {
        final PageResult<OrderRefundDTO> pageResult = orderApplicationService.orderRefundHistory(pageQuery);
        return R.ok(pageResult);
    }

}
