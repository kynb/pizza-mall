package club.kynb.mall.payment.dto.third;

import lombok.Data;

import java.io.Serializable;

/**
 * 分账
 *
 * @author kynb_club@163.com
 * @date: 2021/7/5
 */
@Data
public class PaySplitDTO implements Serializable {

    /**
     * 分账子订单号，商户系统唯一
     */
    private String splitNo;
    /**
     * 分账子商户号
     */
    private String splitMerId;
    /**
     * 分账金额
     */
    private String orderAmt;
    /**
     * 分账手续费
     */
    private String orderFee;
    /**
     * 商品标签(可选参数)
     */
    private String goodsTag;
    /**
     * 商品信息(可选参数)
     */
    //private String goodsInfo;
    /**
     * 订单类型(可选参数)
     */
    //private String orderType;
    /**
     * 订单信息(可选参数)
     */
    //private String orderInfo;
}
