package club.kynb.mall.application.flow.order;

import club.kynb.mall.application.context.OrderSubmitContext;
import lombok.extern.slf4j.Slf4j;
import org.pizza.pattern.filter.Filter;
import org.pizza.pattern.filter.FilterInvoker;

/**
 * @author kynb_club@163.com
 * @since 2021/7/8 6:00 下午
 */
@Slf4j
public abstract class AbstractSubmitOrderFlowFilter implements Filter<OrderSubmitContext> {

    @Override
    public void doFilter(OrderSubmitContext orderSubmitContext, FilterInvoker<OrderSubmitContext> filterInvoker) {
        if (!orderSubmitContext.isFastFailed()) {
            log.info("提交订单过滤器：{} start", this.getClass().getSimpleName());
            doService(orderSubmitContext);
            log.info("提交订单过滤器：{} end", this.getClass().getSimpleName());
        }
        filterInvoker.invoke(orderSubmitContext);
    }

    abstract protected void doService(OrderSubmitContext orderSubmitContext);

}
