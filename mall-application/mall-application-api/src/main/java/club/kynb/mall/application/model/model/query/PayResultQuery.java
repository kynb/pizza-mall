package club.kynb.mall.application.model.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Query;

import javax.validation.constraints.NotBlank;

/**
 * @author kynb_club@163.com
 * @date: 2021/7/11
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "PayResultQuery对象", description = "支付结果查询")
@Accessors(chain = true)
public class PayResultQuery extends Query {
    @NotBlank(message = "订单号不能为空")
    @ApiModelProperty(value = "订单号", example = "OR1212212122", required = true)
    private String innerOrderNo;

    @NotBlank(message = "订单类型")
    @ApiModelProperty(value = "订单类型(PURCHASE:普通采购订单,BILL:账单)", example = "PURCHASE", required = true)
    private String orderType;
}
