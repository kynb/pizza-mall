package club.kynb.mall.application.flow.order;

import club.kynb.mall.application.context.MallUserContext;
import club.kynb.mall.application.context.OrderSubmitContext;
import club.kynb.mall.application.model.model.event.OrderSubmitSuccessEvent;
import club.kynb.mall.order.constant.OrderSubmitStatusEnum;
import club.kynb.mall.order.model.dto.OrderDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.event.publish.EventBus;
import org.springframework.stereotype.Component;

/**
 * @author kynb_club@163.com
 * @since 2021/7/7 1:56 下午
 */
@Slf4j
@Component
@AllArgsConstructor
public class SubmitOrderSuccessFilter extends AbstractSubmitOrderFlowFilter {
    private final EventBus eventBus;

    @Override
    protected void doService(OrderSubmitContext orderSubmitContext) {
        //发布事件
        final OrderDTO orderDTO = orderSubmitContext.getOrderDTO();
        //成功
        orderSubmitContext.getOrderSubmitResultVO().setSubmitStatus(OrderSubmitStatusEnum.SUCCESS.getStatus());
        orderSubmitContext.getOrderSubmitResultVO().setFailureReason("");
        orderSubmitContext.getOrderSubmitResultVO().setCancelEndTime(orderDTO.getCancelEndTime());
        orderSubmitContext.getOrderSubmitResultVO().setPayAmount(orderDTO.getPayAmount());

        eventBus.publishEvent(OrderSubmitSuccessEvent.builder()
                .userId(MallUserContext.get(true)
                        .getUserDTO()
                        .getId())
                .innerOrderNo(orderDTO.getInnerOrderNo())
                .build()
        );

    }
}
