package club.kynb.mall.order.api;


import club.kynb.mall.order.model.dto.*;
import club.kynb.mall.order.constant.OrderCancelTypeEnum;
import club.kynb.mall.order.constant.OrderConfirmTypeEnum;
import org.pizza.model.page.PageResult;

import java.util.List;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/28 8:51 下午
 */
public interface IOrderService {

    boolean existsOrderNo(String innerOrderNo);

    Optional<OrderAndDetailsDTO> getByOrderAndDetails(String innerOrderNo);

    List<OrderDetailDTO> listOrdersDetail(List<String> orderNos);

    List<OrderDetailDTO> listOrderDetail(String orderNo);

    PageResult<OrderAndDetailsDTO>  orderHistoryPageQuery(OrderHistoryPageQuery orderHistoryPageQuery);

    void updateByOrderNo(OrderDTO dto);

    void createOrder(OrderDTO orderDTO, List<OrderDetailDTO> orderDetailList);

    void orderCancel(String innerOrderNo, OrderCancelTypeEnum cancelTypeEnum, String cancelDesc);

    Optional<OrderDTO> getOrderBy(String innerOrderNo);

    void orderConfirm(String innerOrderNo, OrderConfirmTypeEnum confirmTypeEnum);

    void applyRefund(String innerOrderNo);

    void applyRefundCancel(String innerOrderNo, String orderOriginalStatus);

    PageResult<OrderDTO>  orderPage(OrderPageQuery query);
}
