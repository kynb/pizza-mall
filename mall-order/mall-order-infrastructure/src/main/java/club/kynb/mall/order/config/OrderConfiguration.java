package club.kynb.mall.order.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author kynb_club@163.com
 * @since 2021/6/24 8:57 上午
 */
@Configuration
@MapperScan("club.kynb.mall.order.mysql.mapper")
public class OrderConfiguration {

}
