package club.kynb.mall.order.mysql.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.mybatis.plus.base.BaseEntity;

import java.math.BigDecimal;


/**
 * 订单退款详情表持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_order_refund_detail")
@EqualsAndHashCode(callSuper = true)
public class OrderRefundDetailPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 退款申请编号
     */
    private String refundNo;
    /**
     * 订单详情id
     */
    private Long orderDetailId;
    /**
     * 数量
     */
    private Integer num;
    /**
     * 退款金额
     */
    private BigDecimal amount;
    /**
     * 备注信息
     */
    private String remark;
    /**
     * 扩展字段
     */
    private String extension;


}
