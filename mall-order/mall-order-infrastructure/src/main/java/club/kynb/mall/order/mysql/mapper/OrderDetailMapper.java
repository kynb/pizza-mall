package club.kynb.mall.order.mysql.mapper;

import club.kynb.mall.order.mysql.po.OrderDetailPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
/**
 * 订单详情表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */
@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetailPO> {


}
