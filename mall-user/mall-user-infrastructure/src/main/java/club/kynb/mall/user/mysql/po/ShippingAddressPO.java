package club.kynb.mall.user.mysql.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.mybatis.plus.base.BaseEntity;


/**
 * 收货地址表持久实体类
 * @author kynb_club@163.com
 * @since 2021-06-27
 */

@Data
@Accessors(chain = true)
@TableName("mall_shipping_address")
@EqualsAndHashCode(callSuper = true)
public class ShippingAddressPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 地标名
     */
    private String landmark;
    /**
     * 门牌号
     */
    private String houseNo;
    /**
     * 收货地址(地图上定位的地址)
     */
    private String address;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 电话
     */
    private String phone;
    /**
     * 标签
     */
    private String tag;
    /**
     * 经度
     */
    private Double longitude;
    /**
     * 纬度
     */
    private Double latitude;
    /**
     * 是否默认0否1是
     */
    private Integer defaulted;
    /**
     * 扩展字段
     */
    private String extension;


}
