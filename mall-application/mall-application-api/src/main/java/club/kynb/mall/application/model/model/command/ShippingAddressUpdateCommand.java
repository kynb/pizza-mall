package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author kynb_club@163.com
 * @since 2020/12/28 9:00 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "ShippingAddressUpdateCommand对象", description = "更新收货地址命令")
@Accessors(chain = true)
public class ShippingAddressUpdateCommand extends Command {
    public interface Update {
    }
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "收货地址ID", example = "")
    @NotNull(groups = {ShippingAddressUpdateCommand.Update.class}, message = "收货地址ID不能为空")
    private Long id;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id", example = "", hidden = true)
    private Long userId;
    /**
     * 地标名
     */
    @ApiModelProperty(value = "地标名", example = "")
    private String landmark;
    /**
     * 门牌号
     */
    @ApiModelProperty(value = "门牌号", example = "")
    private String houseNo;
    /**
     * 收货地址(地图上定位的地址)
     */
    @ApiModelProperty(value = "收货地址", example = "")
    @NotBlank(message = "收货地址不能为空")
    private String address;
    /**
     * 联系人
     */
    @ApiModelProperty(value = "联系人", example = "")
    @NotBlank(message = "联系人不能为空")
    private String contact;
    /**
     * 电话
     */
    @ApiModelProperty(value = "联系电话", example = "")
    @Pattern(regexp = "^1\\d{10}$", message = "请输入11位的手机号码")
    private String phone;
    /**
     * 经度
     */
    @ApiModelProperty(value = "经度", example = "")
    @NotNull(message = "经度不能为空")
    private Double longitude;
    /**
     * 纬度
     */
    @ApiModelProperty(value = "纬度", example = "")
    @NotNull(message = "纬度不能为空")
    private Double latitude;
    /**
     * 标签
     */
    @ApiModelProperty(value = "标签", example = "")
    private String tag;
    /**
     * 是否默认0否1是
     */
    @ApiModelProperty(value = "是否默认0否1是", example = "")
    @NotNull( message = "是否默认地址不能为空")
    private Integer defaulted;

}
