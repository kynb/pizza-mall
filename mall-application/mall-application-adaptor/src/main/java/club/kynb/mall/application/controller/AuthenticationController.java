package club.kynb.mall.application.controller;

import club.kynb.mall.application.service.AuthenticationApplicationService;
import club.kynb.mall.application.service.RegisterApplicationService;
import club.kynb.mall.application.model.model.command.*;
import club.kynb.mall.application.model.model.vo.LoginResultVO;
import club.kynb.mall.application.model.model.vo.RegisterResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.mvc.util.WebUtil;
import org.pizza.common.web.response.R;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author kynb_club@163.com
 * @since 2021/6/20 3:40 下午
 */
@Slf4j
@RestController
@RequestMapping
@AllArgsConstructor
@Api(value = "", tags = "登录注册接口")
public class AuthenticationController {
    private final AuthenticationApplicationService authenticationApplicationService;
    private final RegisterApplicationService registerApplicationService;

    @PostMapping("/authentication/pwdLogin")
    @ApiOperation(value = "登录页-手机密码登录接口", notes = "传入PwdAuthenticationCommand")
    public R<LoginResultVO> pwdLogin(HttpServletRequest request, @RequestBody @Valid PwdAuthenticationCommand command) {
        command.setClientIp(WebUtil.getIP(request));
        return R.ok(authenticationApplicationService.pwdLogin(command));
    }

    @PostMapping("/authentication/smsLogin")
    @ApiOperation(value = "登录页-手机短验登录接口", notes = "传入SmsAuthenticationCommand")
    public R<LoginResultVO> smsLogin(HttpServletRequest request, @RequestBody @Valid SmsAuthenticationCommand command) {
        command.setClientIp(WebUtil.getIP(request));
        return R.ok(authenticationApplicationService.smsLogin(command));
    }

    @PostMapping("/authentication/logout")
    @ApiOperation(value = "设置页-登出", notes = "")
    public R<Void> logout(@RequestHeader(value = "accessToken") String token) {
        LogoutCommand logoutCommand = new LogoutCommand();
        logoutCommand.setToken(token);
        authenticationApplicationService.logout(logoutCommand);
        return R.ok();
    }

    @PostMapping("/register/sms")
    @ApiOperation(value = "注册页-手机短验注册接口", notes = "传入SmsRegistryCommand")
    public R<RegisterResultVO> register(@RequestBody @Valid SmsRegistryCommand command) {
        return R.ok(registerApplicationService.register(command));
    }

    @PostMapping("/findPwd/sms")
    @ApiOperation(value = "登录页-手机短验找回密码接口", notes = "传入SmsFindPasswordCommand")
    public R<Void> findPwd(@RequestBody @Valid SmsFindPasswordCommand command) {
        authenticationApplicationService.findPwd(command);
        return R.ok();
    }

}
