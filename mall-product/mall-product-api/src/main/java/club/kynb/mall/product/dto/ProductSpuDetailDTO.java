package club.kynb.mall.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 商品信息表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "ProductSpuDetailDTO对象", description = "商品信息表")
@Accessors(chain = true)
public class ProductSpuDetailDTO extends ProductSpuDTO {
    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private List<ProductSkuDTO> skuList;
}
