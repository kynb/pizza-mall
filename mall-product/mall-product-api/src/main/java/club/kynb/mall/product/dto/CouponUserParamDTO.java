package club.kynb.mall.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.page.PageQuery;

import javax.validation.constraints.NotNull;


@Data
@ApiModel(value = "CouponUserParamDTO对象", description = "用户优惠券列表搜索参数")
@Accessors(chain = true)
public class CouponUserParamDTO extends PageQuery {


    @ApiModelProperty(value = "优惠券ID", example = "1")
    @NotNull(message = "请选择优惠券")
    private Long couponTemplateId;

    @ApiModelProperty(value = "状态：0未使用，1已使用，2已过期", example = "1")
    private Integer status;






}
