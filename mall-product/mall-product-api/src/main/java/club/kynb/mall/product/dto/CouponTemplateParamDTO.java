package club.kynb.mall.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.page.PageQuery;


@Data
@ApiModel(value = "CouponTemplateParamDTO对象", description = "优惠券列表搜索参数")
@Accessors(chain = true)
public class CouponTemplateParamDTO extends PageQuery {


    @ApiModelProperty(value = "关键词搜索（标题和名称模糊查询）", example = "标题")
    private String keyword;


}
