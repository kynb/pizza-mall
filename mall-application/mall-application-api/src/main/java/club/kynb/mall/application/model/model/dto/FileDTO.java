package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

/**
 * @author kynb_club@163.com
 * @since 2021/2/24 3:57 下午
 */
@Data
@ApiModel(value = "FileDTO对象", description = "文件对象")
@Accessors(chain = true)
public class FileDTO implements DTO {
    /**
     * 对象名称
     */
    @ApiModelProperty(value = "对象名称", example = "")
    private String objectName;
    @ApiModelProperty(value = "原始文件名称", example = "")
    private String originalFileName;
    @ApiModelProperty(value = "临时预览地址", example = "")
    private String previewUrl;
    @ApiModelProperty(value = "访问失效时间", example = "")
    private Long expireAt;

    public void setPreviewUrl(String previewUrl) {
        if (!previewUrl.startsWith("https")) {
            previewUrl = previewUrl.replace("http", "https");
        }
        this.previewUrl = previewUrl;
    }
}
