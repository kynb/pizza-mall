package club.kynb.mall.application.model.model.vo;

import club.kynb.mall.application.model.model.dto.TokenDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.VO;


/**
 * @author kynb_club@163.com
 * @since 2021/6/20 4:05 下午
 */
@Data
@ApiModel(value = "LoginResult对象", description = "登录结果对象")
@Accessors(chain = true)
public class LoginResultVO implements VO {
    @ApiModelProperty(value = "用户ID", example = "1")
    private String userId;
    @ApiModelProperty(value = "用户名称", example = "")
    private String userName;
    @ApiModelProperty(value = "访问令牌(审核通过的用户才发放令牌)", example = "")
    private TokenDTO accessToken;
}
