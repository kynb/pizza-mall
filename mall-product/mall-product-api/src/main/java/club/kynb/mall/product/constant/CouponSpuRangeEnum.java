package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.Arrays;
import java.util.Optional;

/**
 * 活动分类 1 精选 2 特价促销 3丰盛午餐 4 红叶午餐
 * @author kynb_club@163.com
 * @since 2021/6/24 2:53 下午
 */
@Getter
@AllArgsConstructor
public enum CouponSpuRangeEnum implements EnumValidI {

    ASSIGN(1,"指定商品"),
    ALL(2,"全部商品"),
    ;

    private final Integer type;
    private final String desc;

    @Override
    public String code() {
        return String.valueOf(this.type);
    }

    @Override
    public String message() {
        return this.desc;
    }

    public static Optional<CouponSpuRangeEnum> get(Integer type){
        return Arrays.stream(CouponSpuRangeEnum.values())
                .filter(item-> item.getType().equals(type))
                .findFirst();
    }
}
