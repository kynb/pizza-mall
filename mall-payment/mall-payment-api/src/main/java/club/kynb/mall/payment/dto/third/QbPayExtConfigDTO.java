package club.kynb.mall.payment.dto.third;

import club.kynb.mall.payment.dto.ext.PayExtConfigDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 钱宝支付扩展参数
 *
 * @author kynb_club@163.com
 * @date: 2021/7/7
 */
@Data
@Accessors(chain = true)
public class QbPayExtConfigDTO extends PayExtConfigDTO {

    private List<PaySplitDTO> list;

}
