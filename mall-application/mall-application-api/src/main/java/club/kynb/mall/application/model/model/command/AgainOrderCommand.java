package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import javax.validation.constraints.NotBlank;

/**
 * @author kynb_club@163.com
 * @since 2021/7/1 9:09 下午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "AgainOrderCommand对象", description = "再来一单命令")
@Accessors(chain = true)
public class AgainOrderCommand extends Command {

    @ApiModelProperty(value = "内部订单编号", example = "454158125549748224")
    @NotBlank(message = "内部订单编号不能为空")
    private String innerOrderNo;



}
