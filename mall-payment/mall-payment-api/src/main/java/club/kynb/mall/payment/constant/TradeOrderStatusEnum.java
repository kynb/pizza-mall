package club.kynb.mall.payment.constant;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.stream.Stream;

/**
 * 状态：0 待处理； 1 成功；2 失败  -1 取消
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Getter
@AllArgsConstructor
public enum TradeOrderStatusEnum implements EnumValidI {

    WAITING_4_UPDATE(0, "待处理"),
    SUCCESS(1, "成功"),
    FAILURE(2, "失败"),
    ;

    private Integer status;
    private String desc;

    @Override
    public String code() {
        return String.valueOf(this.status);
    }

    @Override
    public String message() {
        return this.desc;
    }

    @JsonIgnore
    public static TradeOrderStatusEnum get(Integer status) {
        return Stream.of(TradeOrderStatusEnum.values())
                .filter(i -> i.getStatus().equals(status)).findFirst()
                .orElse(null);
    }
}
