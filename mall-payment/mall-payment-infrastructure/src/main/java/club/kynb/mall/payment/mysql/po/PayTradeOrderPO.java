package club.kynb.mall.payment.mysql.po;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import org.pizza.mybatis.plus.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 支付交易订单表持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_pay_trade_order")
@EqualsAndHashCode(callSuper = true)
public class PayTradeOrderPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 原始订单类型
     */
    private String originOrderType;
    /**
     * 原始订单编号
     */
    private String originOrderNo;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 交易单流水
     */
    private String tradeOrderNo;
    /**
     * 订单金额
     */
    private BigDecimal orderAmount;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 支付方式
     */
    private String payChannel;
    /**
     * 客户端类型
     */
    private String clientType;
    /**
     * 实付金额
     */
    private BigDecimal payAmount;
    /**
     * 支付时间
     */
    private Date payTime;
    /**
     * 对接系统流水号
     */
    private String payFlowNo;
    /**
     * 业务参数
     */
    private String bizParams;
    /**
     * 通知信息
     */
    private String notifyInfo;
    /**
     * 扩展字段
     */
    private String extension;


}
