package club.kynb.mall.basic.dto;

import org.pizza.model.DTO;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;

/**
 * 配置表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-06-27
 */

@Data
@ApiModel(value = "ConfigDTO对象", description = "配置表")
@Accessors(chain = true)
public class ConfigDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 作用域枚举(MALL)
     */
    @ApiModelProperty(value = "作用域枚举(MALL)", example = "")
    private String scope;
    /**
     * 配置描述
     */
    @ApiModelProperty(value = "配置描述", example = "")
    private String configDesc;
    /**
     * 配置key
     */
    @ApiModelProperty(value = "配置key", example = "")
    private String configKey;
    /**
     * 配置value
     */
    @ApiModelProperty(value = "配置value", example = "")
    private String configValue;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息", example = "")
    private String remark;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
