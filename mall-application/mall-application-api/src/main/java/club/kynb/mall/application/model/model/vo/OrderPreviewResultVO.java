package club.kynb.mall.application.model.model.vo;

import club.kynb.mall.application.model.model.dto.PreviewOrderDTO;
import club.kynb.mall.application.model.model.dto.UserReceiveCouponDTO;
import club.kynb.mall.user.model.dto.ShippingAddressDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.VO;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/7/5 9:12 下午
 */
@Data
@ApiModel(value = "OrderPreviewResult对象", description = "订单预览结果")
@Accessors(chain = true)
public class OrderPreviewResultVO implements VO {
    /**
     * 推荐的优惠券
     */
    @ApiModelProperty(value = "推荐使用的优惠券", example = "")
    private UserReceiveCouponDTO recommendCoupon;
    /**
     * 可使用的的优惠券
     */
    @ApiModelProperty(value = "用户可使用的优惠券列表", example = "")
    private List<UserReceiveCouponDTO> userCouponList;
    /**
     * 默认收货地址
     */
    @ApiModelProperty(value = "默认收货地址", example = "")
    private ShippingAddressDTO defaultShippingAddress;
    /**
     * 收货地址列表
     */
    @ApiModelProperty(value = "用户的收货地址列表", example = "")
    private List<ShippingAddressDTO> shippingAddressList;
    /**
     * 预览订单信息
     */
    @ApiModelProperty(value = "预览订单信息", example = "")
    private PreviewOrderDTO previewOrderDTO =new PreviewOrderDTO();


}
