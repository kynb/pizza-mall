package club.kynb.mall.payment.dto.ext;

import club.kynb.mall.payment.constant.TradeOrderStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;

/**
 * 支付结果对象
 *
 * @author kynb_club@163.com
 * @date:  2021/7/2
 */
@Data
@ApiModel(value = "PayResultDTO对象", description = "支付结果信息")
@Accessors(chain = true)
public class PayResultDTO implements DTO {
	/**
	 * 	原始订单号
	 */
	@ApiModelProperty(value = "订单号", example = "")
	private String orderNo;
	/**
	 * 	支付订单编号
	 */
	@ApiModelProperty(value = "支付流水号", example = "")
	private String payOrderNo;
	/**
	 * 	支付金额
	 */
	@ApiModelProperty(value = "支付金额", example = "")
	private BigDecimal payAmount;

	@ApiModelProperty(value = "支付结果", example = "")
	private TradeOrderStatusEnum status;
	/**
	 * 	封装信息
	 */
	@ApiModelProperty(value = "扩展信息", example = "")
	private String payData;

}
