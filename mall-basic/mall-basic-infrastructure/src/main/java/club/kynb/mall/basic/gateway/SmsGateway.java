package club.kynb.mall.basic.gateway;


/**
 * @author kynb_club@163.com
 * @since 2021/6/24 3:05 下午
 */
public interface SmsGateway {
    void send(String phone,String templateId,String code) throws Exception;
}
