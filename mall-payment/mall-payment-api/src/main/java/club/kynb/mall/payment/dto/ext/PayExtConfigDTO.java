package club.kynb.mall.payment.dto.ext;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

/**
 * 支付提交扩展参数
 * @author kynb_club@163.com
 * @date: 2021/7/2
 */
@Data
@ApiModel(value = "PayExtConfigDTO对象", description = "支付请求扩展信息")
@Accessors(chain = true)
public class PayExtConfigDTO implements DTO {

}
