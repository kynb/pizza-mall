package club.kynb.mall.payment.dto.third;

import club.kynb.mall.payment.constant.TradeOrderStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;

/**
 * 支付结果对象
 *
 * @author kynb_club@163.com
 * @date:  2021/7/2
 */
@Data
@ApiModel(value = "PayExecutorResultDTO对象", description = "支付结果信息")
@Accessors(chain = true)
public class PayExecutorResultDTO implements DTO {

	/**
	 * 	支付方式
	 */
	@ApiModelProperty(value = "支付方式", example = "")
	private String payChannel;
	/**
	 * 	支付订单编号
	 */
	@ApiModelProperty(value = "支付流水号", example = "")
	private String payFlowNo;
	/**
	 * 	支付金额
	 */
	@ApiModelProperty(value = "支付金额", example = "")
	private BigDecimal payAmount;

	@ApiModelProperty(value = "支付状态", example = "")
	private TradeOrderStatusEnum status;
	/**
	 * 	支付信息
	 */
	@ApiModelProperty(value = "支付信息", example = "")
	private String payData;

	@ApiModelProperty(value = "三方返回信息", example = "")
	private String returnInfo;

	@ApiModelProperty(value = "支付参数", example = "")
	private String params;
}
