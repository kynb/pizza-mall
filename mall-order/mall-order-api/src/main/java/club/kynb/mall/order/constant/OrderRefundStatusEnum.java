package club.kynb.mall.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

/**
 * 订单退款申请状态
 * @author kynb_club@163.com
 * @since  2021/7/8
 */
@Getter
@AllArgsConstructor
public enum OrderRefundStatusEnum implements EnumValidI {

    CANCELED(0,"取消"),
    SUCCESS(1,"成功"),         //成功
    FAILED(2,"失败"),           //失败
    PROCESSING(3,"进行中"),           //进行中
    ;
    private final Integer status;
    private final String desc;
    @Override
    public String code() {
        return String.valueOf(this.status);
    }

    @Override
    public String message() {
        return this.desc;
    }


}
