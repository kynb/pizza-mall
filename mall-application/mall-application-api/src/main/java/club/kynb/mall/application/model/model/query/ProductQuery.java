package club.kynb.mall.application.model.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Query;

import javax.validation.constraints.NotEmpty;

/**
 * @author kynb_club@163.com
 * @date: 2021/6/26
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "ProductQuery对象", description = "商品搜索 排序字段为 price,saleNum 默认排序不传")
@Accessors(chain = true)
public class ProductQuery extends Query {

    @NotEmpty(message = "请输入要搜索的商品")
    @ApiModelProperty(value = "商品搜索关键词", example = "",required = true)
    private String keyword;

    @ApiModelProperty(value = "排序类型", example = "",notes = "1 销量倒序 2 价格正序  3 价格倒序")
    private Integer orderType;

}
