package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;

/**
 * 商品信息表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-06-26
 */

@Data
@ApiModel(value = "PreviewOrderSkuPriceDTO对象", description = "预览订单SKU价格信息")
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public  class PreviewOrderSkuPriceDTO implements DTO {
    /**
     *
     */
    @ApiModelProperty(value = "基本价格", example = "18.88")
    private BigDecimal skuPrice;
    @ApiModelProperty(value = "计价单位", example = "斤")
    private String unit;
}
