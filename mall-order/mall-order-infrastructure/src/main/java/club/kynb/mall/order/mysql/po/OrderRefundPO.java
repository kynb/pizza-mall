package club.kynb.mall.order.mysql.po;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import org.pizza.mybatis.plus.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 订单退款表持久实体类
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@Accessors(chain = true)
@TableName("mall_order_refund")
@EqualsAndHashCode(callSuper = true)
public class OrderRefundPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 审核人用户ID
     */
    private Long auditUserId;
    /**
     * 退款申请编号
     */
    private String refundNo;
    /**
     * 内部订单编号
     */
    private String innerOrderNo;
    /**
     * 退款申请前的订单状态 见订单状态枚举
     */
    private String fromOrderStatus;
    /**
     * 退款状态  1审核通过 2审核失败 3进行中
     */
    private Integer status;
    /**
     * 退款状态  1全部退款 2部分退款
     */
    private Integer type;
    /**
     * 退款原因描述
     */
    private String refundDesc;
    /**
     * 退款金额
     */
    private BigDecimal refundAmount;
    /**
     * 备注信息
     */
    private String remark;
    /**
     * 扩展字段
     */
    private String extension;


}
