package club.kynb.mall.order.constant;

/**
 * @author kynb_club@163.com
 * @since 2021/7/1 10:25 下午
 */
public interface OrderConstant {
    /**
     * 账期支付方式code
     */
    String CREDIT_CHANNEL = "CREDIT";

    String NOTIFY_ORDER_LOCK = "LOCK:NOTIFY_ORDER:%s:%s";

}
