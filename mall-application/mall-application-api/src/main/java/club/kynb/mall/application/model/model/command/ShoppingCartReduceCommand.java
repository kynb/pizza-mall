package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author kynb_club@163.com
 * @since 2021/7/1 9:09 下午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "ShoppingCartReduceCommand对象", description = "购物车减少商品命令")
@Accessors(chain = true)
public class ShoppingCartReduceCommand extends Command {

    @ApiModelProperty(value = "spuId", example = "1")
    @NotNull(message = "spuId不能为空")
    private Long spuId;

    @ApiModelProperty(value = "skuId", example = "1")
    @NotNull(message = "skuId不能为空")
    private Long skuId;

    @ApiModelProperty(value = "减少数量（全部移除传递-1）", example = "1")
    @NotNull(message = "数量不能为空，减少数量（全部移除传递-1")
    @Min(value = 1, message = "数量不能为空需要大于0")
    private Integer num;

}
