package club.kynb.mall.order.mysql.mapper;

import club.kynb.mall.order.mysql.po.OrderRefundDetailPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * 订单退款详情表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */
@Mapper
public interface OrderRefundDetailMapper extends BaseMapper<OrderRefundDetailPO> {


}
