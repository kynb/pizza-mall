package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

/**
 * 商品信息表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-07-06
 */

@Data
@ApiModel(value = "PreviewOrderSpuDTO对象", description = "预览订单SPU信息")
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public  class PreviewOrderSpuDTO implements DTO {
    @ApiModelProperty(value = "spuId", example = "1")
    private String id;
    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称", example = "云南甜玉米")
    private String spuName;

    /**
     * 列表展示图片
     */
    @ApiModelProperty(value = "商品图片", example = "")
    private String iconUrl;
    /**
     * SKU信息
     */
    @ApiModelProperty(value = "SKU信息", example = "")
    private PreviewOrderSkuDTO previewOrderSkuDTO;
}
