package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/7/1 9:09 下午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "ShoppingCartCheckedCommand对象", description = "购物车选中商品命令")
@Accessors(chain = true)
public class ShoppingCartCheckedCommand extends Command {

    @ApiModelProperty(value = "userId", example = "1",hidden = true)
    private Long userId;

    @ApiModelProperty(value = "选中的skuId列表")
    private List<Long> checkedSkuIdList;

    @ApiModelProperty(value = "未选中的skuId列表")
    private List<Long> unCheckedSkuIdList;

}
