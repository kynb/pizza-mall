package club.kynb.mall.application.model.model.event;


import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.pizza.event.model.DomainEvent;

import java.util.Date;

/**
 * @author kynb_club@163.com
 * @since 2020/11/21 10:55 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class AuthSuccessEvent extends DomainEvent {
    /**
     * 成员ID
     */
    private Long userId;
    /**
     * 请求客户端IP
     */
    private String clientIp;
    /**
     * 客户端类型
     */
    private String clientType;
    /**
     * 动作类型（1登录2登出）
     */
    private Integer actionType;
    /**
     * 持有令牌
     */
    private String token;
    /**
     * 令牌过期时间
     */
    private Date tokenExpireTime;




    @Override
    public boolean isStore() {
        return true;
    }
}


