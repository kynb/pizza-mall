package club.kynb.mall.order.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.page.PageQuery;



@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "OrderRefundPageQuery对象", description = "退货订单分页查询")
@Accessors(chain = true)
public class OrderRefundPageQuery extends PageQuery {

    @ApiModelProperty(value = "(不传即全部，退款状态  1审核通过 2审核失败 3进行中)", example = "1")
    private Integer status;


}
