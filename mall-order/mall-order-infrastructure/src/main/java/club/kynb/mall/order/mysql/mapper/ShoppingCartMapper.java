package club.kynb.mall.order.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import club.kynb.mall.order.mysql.po.ShoppingCartPO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 购物车表 Mapper 接口
 *
 * @author kynb_club@163.com
 * @since 2021-06-28
 */
@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCartPO> {

    @Delete("delete from mall_shopping_cart where user_id = #{userId} and spu_id= #{spuId} and sku_id= #{skuId}")
    void physicsDelete(@Param("userId") Long userId, @Param("spuId") Long spuId, @Param("skuId") Long skuId);

    @Delete("delete from mall_shopping_cart where user_id = #{userId} and checked= #{checked}")
    void physicsDeleteBy(@Param("userId") Long userId, @Param("checked") Integer checked);
}
