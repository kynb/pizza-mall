package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import javax.validation.constraints.NotNull;

/**
 * @author kynb_club@163.com
 * @since 2020/12/28 9:00 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "ReceiveCouponCommand对象", description = "领取优惠券命令")
@Accessors(chain = true)
public class ReceiveCouponCommand extends Command {

    @ApiModelProperty(value = "优惠券模板ID", example = "1", required = true)
    @NotNull(message = "优惠券模板ID不能为空")
    private Long couponTemplateId;

}
