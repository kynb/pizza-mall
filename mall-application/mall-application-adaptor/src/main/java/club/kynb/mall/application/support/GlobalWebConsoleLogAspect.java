package club.kynb.mall.application.support;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.pizza.common.web.mvc.aspect.WebConsoleLogAspect;
import org.springframework.stereotype.Component;

/**
 * @author kynb_club@163.com
 * @since 2020-04-09 10:23 上午
 */
@Aspect
@Component
public class GlobalWebConsoleLogAspect extends WebConsoleLogAspect {

    @Around("execution(* club.kynb.mall.application.*.*.*Controller.*(..))")
    public Object aroundControllerMethod(final ProceedingJoinPoint pjp) throws Throwable {
        return around(pjp);
    }

    @Override
    protected boolean prettyPrint() {
        //FIXME  配置格式化打印
        return true;
    }

    @Override
    protected int outThreshold() {
        return 5000;
    }
}
