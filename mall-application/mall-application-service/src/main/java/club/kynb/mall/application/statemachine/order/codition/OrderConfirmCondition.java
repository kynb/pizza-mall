package club.kynb.mall.application.statemachine.order.codition;

import club.kynb.mall.application.context.OrderContext;
import club.kynb.mall.application.model.model.command.ConfirmOrderCommand;
import lombok.AllArgsConstructor;
import org.pizza.common.web.exception.Errors;
import org.pizza.model.Command;
import org.pizza.statemachine.Condition;
import org.springframework.stereotype.Component;

/**
 * @author kynb_club@163.com
 * @since 2021/7/9 10:29 上午
 */
@Component
@AllArgsConstructor
public class OrderConfirmCondition implements Condition<OrderContext> {

    @Override
    public boolean isSatisfied(OrderContext orderContext) {
        final Command command = orderContext.getCommand();
        if (command instanceof ConfirmOrderCommand) {
            return true;
        }
        //后续流程中校验
        throw Errors.SYSTEM.exception("创建订单命令转换失败");
    }

    @Override
    public String name() {
        return Condition.super.name();
    }
}
