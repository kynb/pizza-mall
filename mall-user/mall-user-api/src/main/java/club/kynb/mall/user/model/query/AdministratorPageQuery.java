package club.kynb.mall.user.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.page.PageQuery;

/**
 * @author kynb_club@163.com
 * @since 2021/7/20 9:45 下午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "AdministratorPageQuery对象", description = "管理员分页查询")
@Accessors(chain = true)
public class AdministratorPageQuery extends PageQuery {
    /**
     * 昵称
     */
    @ApiModelProperty(value = "关键字", example = "超级管理员")
    private String keyword;

}
