package club.kynb.mall.user.mysql.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.mybatis.plus.base.BaseEntity;


/**
 * 客服表持久实体类
 * @author kynb_club@163.com
 * @since 2021-08-03
 */

@Data
@Accessors(chain = true)
@TableName("mall_administrator")
@EqualsAndHashCode(callSuper = true)
public class AdministratorPO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID(userId)
     */
    @TableId(value = "id", type = IdType.NONE)
    private Long id;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 扩展字段
     */
    private String extension;


}
