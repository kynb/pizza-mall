package club.kynb.mall.product.api;


import club.kynb.mall.product.dto.*;
import org.pizza.model.page.PageResult;

import java.util.List;
import java.util.Optional;

/**
 * @author kynb_club@163.com
 * @since 2021/6/20 4:59 下午
 */
public interface IProductCategoryService {


    List<ProductCategoryDTO> listByIds(List<Long> categoryIds);
    /**
     * 分类列表
     * @return
     */
    List<ProductCategoryDTO> listByParent(long parentId);

    /**
     * 分类列表(分页)
     * @param dto
     * @return
     */
    PageResult<ProductCategoryDTO> pageProductCategory(ProductCategoryParamDTO dto);

    /**
     * 所有分类
     * @return
     */
    List<ProductCategoryFullDTO> listFull();



    /**
     * 创建分类
     * @param dto
     */
    void createProductCategory(ProductCategoryDTO dto);

    /**
     * 修改分类
     * @param dto
     */
    void updateProductCategory(ProductCategoryDTO dto);

    /**
     *  删除分类
     * @param id
     */
    void deleteProductCategory(Long id);

    /**
     * 商品分类详情
     * @param id
     * @return
     */
    Optional<ProductCategoryDTO> detailById(Long id);

    Optional<ProductCategoryDTO> getByName(Long parentId,String cateName);

    boolean existCategory(Long parentId,String cateName);

}
