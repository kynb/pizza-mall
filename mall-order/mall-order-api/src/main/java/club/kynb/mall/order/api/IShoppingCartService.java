package club.kynb.mall.order.api;


import club.kynb.mall.order.model.dto.ShoppingCartDTO;

import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/6/28 8:51 下午
 */
public interface IShoppingCartService {

    List<ShoppingCartDTO> userShoppingCart(Long userId);

    void add(ShoppingCartDTO shoppingCartDTO);

    void batchAdd(List<ShoppingCartDTO> cartList);

    void reduce(Long userId, Long spuId, Long skuId, Integer num);

    void checked(Long userId, List<Long> checkedSkuIdList, List<Long> unCheckedSkuIdList);

    List<ShoppingCartDTO> getCheckedList(Long userId);

    void clearShoppingCart(Long userId);
}
