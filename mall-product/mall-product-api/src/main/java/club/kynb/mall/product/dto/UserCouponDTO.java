package club.kynb.mall.product.dto;

import org.pizza.model.DTO;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;

/**
 * 用户优惠券表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "UserCouponDTO对象", description = "用户优惠券表")
@Accessors(chain = true)
public class UserCouponDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 优惠券ID
     */
    @ApiModelProperty(value = "优惠券ID", example = "")
    private Long couponTemplateId;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", example = "")
    private Long userId;
    /**
     * 领取时间
     */
    @ApiModelProperty(value = "领取时间", example = "")
    private Date receiveTime;
    /**
     * 状态：0未使用，1已使用，2已过期
     */
    @ApiModelProperty(value = "状态：0未使用，1已使用，2已过期", example = "")
    private Integer status;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
