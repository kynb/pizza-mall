package club.kynb.mall.payment.exec;

import club.kynb.mall.payment.constant.PayChannelEnum;
import club.kynb.mall.payment.mysql.po.PayTradeOrderPO;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.springframework.stereotype.Service;

/**
 * 支付宝
 * @author kynb_club@163.com
 * @date: 2021/7/7
 */
@Service("aliPayExecutorImpl")
public class AliPayExecutorImpl extends AbstractPayExecutor {

    public AliPayExecutorImpl() {
        super();
    }

    @Override
    public String getChannel() {
        return PayChannelEnum.ALIPAY.code();
    }

    @Override
    protected JSONObject doCreatePay() {
        return JSONUtil.parseObj("{\"flowNo\":\"001\",\"payData\":\"1\"}");
    }

    @Override
    protected JSONObject doQuery(PayTradeOrderPO tradeOrder) {
        return JSONUtil.parseObj("{\"status\":\"1\",\"amt\":\"1\"}");
    }
}
