# 微服务脚手架
   
    
## 背景
    
```
    几个多年从事java开发的程序员想一起搞点事情（有兴趣请联系：kynb_club@163.com）
于是将平时工作中用到的代码、组件(开源项目的代码，及思想——站在巨人的肩膀上看世界 ^_^)等进行了整理，总结。
旨在——把健壮的项目结构搭建变的更简单
```

## 本项目说明
```
    以电商场景为例，目前以多模块聚合的单体模式，DDD架构模型为基础
易扩展和维护：
    比如业务膨胀需要拆分， mall-user可以快速切换为dubbo-service
    或者 微服务下用户服务
项目地址：https://gitee.com/kynb/pizza-mall.git 
顶层依赖通用组件：https://gitee.com/kynb/pizza.git
```

项目结构如下：
```
pizza-mall
   |-- mall-application                 应用层服务
        |-- mall-application-api
        |-- mall-application-domain
        |-- mall-application-infrastructure
        |-- mall-application-service
        |-- mall-application-adaptor
        |-- mall-application-bootstrap
   |-- mall-user                         用户模块
        |-- mall-user-api
        |-- mall-user-infrastructure
        |-- mall-user-service
        |-- mall-user-adaptor
        |-- mall-user-bootstrap
   |-- mall-product                      商品模块
        |-- mall-product-api
        |-- mall-product-infrastructure
        |-- mall-product-service
        |-- mall-product-adaptor
        |-- mall-product-bootstrap
   |-- mall-order                        订单模块
        |-- mall-order-api
        |-- mall-order-infrastructure
        |-- mall-order-service
        |-- mall-order-adaptor
        |-- mall-order-bootstrap
   |-- pizza-payment                     支付模块
        |-- mall-payment-api
        |-- mall-payment-infrastructure
        |-- mall-payment-service
        |-- mall-payment-adaptor
        |-- mall-payment-bootstrap
   |-- pizza-basic                        基础支持模块
        |-- mall-basic-api
        |-- mall-basic-infrastructure
        |-- mall-basic-service
        |-- mall-basic-adaptor
        |-- mall-basic-bootstrap
     ...
     ...
   
```

## DDD六边形架构模型

[]( "./image.png")![image](doc/images/image.png)




## 工程结构

[]( "./image.png")![image](doc/images/image-2.png)

◆ start模块:工程入口

◆ `adaptor模块`:适配器、**负责对前端展示（web，wireless，wap）的路由和适配，对于传统B/S系统而言，adapter就相当于MVC中的controller。**

◆ application模块:主要负责获取输入，组装上下文，参数校验，调用领域层做业务处理，如果需要的话，发送消息通知等。层次是开放的，应用层也可以绕过领域层，*直接访问基础实施*层；

◆ client模块:提供给外部服务调用的相关类

◆ domain模块:主要是封装了核心业务逻辑，并通过领域服务（Domain Service）和领域对象（Domain Entity）的方法对App层提供业务实体和业务逻辑计算。领域是应用的核心，不依赖任何其他层次；

◆ infrastructure模块:主要负责技术细节问题的处理，比如数据**库的C**RUD、搜索引擎、文件系统、分布式服务的RPC等。此外，领域防腐的重任也落在这里，外部依赖需要通过**gateway**、**repository**的转义处理，才能被上面的App层和Domain层使用。



### client

```text
|--club.kynb.mall
     |--global
          |--constant
     |--example
          |--client
          |--dto
               |--request
               |--response
               |--event
```

◆ global:包下放置全局相关类

          ◆ constant:常量

◆ example:采用领域分包策略，其中放置example领域下的类

          ◆ client放置服务暴露的接口、

          ◆ dto为数据传输对象，分为command、query、dto、event等



### Adaptor

```text
|--club.kynb.mall
     |--config
     |--controller
     |--mobile
     |--web
     |--support
     
```

◆ config:web相关的配置，例如：WebmvcConfig完成拦截器的注册

◆ controller:传统的控制层完成，实现其他服务内部*调用的接口*

◆ mobile:提供给移动端调用的接口

◆ web:提供给web端调用的接口

◆ support:web层的支持包，包含：切面日志、全局异常、拦截器等



### Application

```text
|--club.kynb.mall.application
     |--example
          |--service
          |--event
               |--handler
		  |--executor
               |--command
               |--query
     |--…
```

◆ example:采用领域分包策略，其中放置~~example~~领域下的类

       ◆ service:应用服务

       ◆ event:事件包

◆ publish:事件发布

               ◆ subscribe:事件订阅



### Domain

```text
|--club.kynb.mall.domain
     |--example
          |--service
          |--gateway
          |--entity
          |--repository
```

◆ example:采用领域分包策略，其中放置example领域下的类

◆ service:领域服务

       ◆ entity:实体（Do领域对象）

◆ gateway:网关接口

       ◆ `repository`:仓储接口





### Infrastructure

```text
|--club.kynb.mall
     |--global
          |--constant
          |--config
     |--example
          |--mapper
          |--po
```

◆ global:包下放置全局相关类

         ◆ common:通用组件，例如：线程池定义等组件

         ◆ config:配置类，例如：缓存配置等

◆ example:采用领域分包策略，其中放置example领域下的类，如仓储实现、领域对象转换器。

        ◆ mapper:mybatis的mapper接口

        ◆ po:持久对象

◆ gateway:网关接口实现

       ◆ repository:仓储接口实现



### Start

```text
club.kynb.mall.Applicaion.java
```

◆ Application类完成springboot应用的启动









## CQRS模式调用视图

[]( "./image.png")![image](doc/images/DDD调用视图.png)







   