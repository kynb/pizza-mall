package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kynb_club@163.com
 * @since 2021/7/7 3:18 下午
 */
@Data
@ApiModel(value = "PreviewOrderDTO对象", description = "预览订单信息")
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class PreviewOrderDTO implements DTO {

    @ApiModelProperty(value = "内部订单编号", example = "")
    private String innerOrderNo;

    @ApiModelProperty(value = "订单状态 见订单状态枚举", example = "")
    private String orderStatus;
    /**
     * 预计送达时间
     */
    @ApiModelProperty(value = "预计送达时间", example = "约2021-08-08 08:00:00前送达")
    private String deliveryTime;
    /**
     * 商品总价格
     */
    @ApiModelProperty(value = "商品总价格", example = "18.00")
    private BigDecimal totalAmount;
    /**
     * 运费
     */
    @ApiModelProperty(value = "运费价格", example = "6.88")
    private BigDecimal freightAmount;
    /**
     * 应付金额
     */
    @ApiModelProperty(value = "应付金额（商品总价+运费）", example = "24.88")
    private BigDecimal payableAmount;
    /**
     * 实付金额
     */
    @ApiModelProperty(value = "实付金额（商品总价+运费-优惠总金额）", example = "21.88")
    private BigDecimal payAmount;
    /**
     * 优惠总金额
     */
    @ApiModelProperty(value = "优惠总金额（优惠券总金额+其他优惠金额）", example = "3.88")
    private BigDecimal discountAmount;
    /**
     * 优惠券总金额
     */
    @ApiModelProperty(value = "优惠券总金额", example = "3.88")
    private BigDecimal couponAmount;
    /**
     * 订单商品总数
     */
    @ApiModelProperty(value = "订单商品总数", example = "1")
    private Integer totalItemNum;

    @ApiModelProperty(value = "订单详情列表")
    private List<PreviewOrderDetailDTO> orderDetailList = new ArrayList<>();
}
