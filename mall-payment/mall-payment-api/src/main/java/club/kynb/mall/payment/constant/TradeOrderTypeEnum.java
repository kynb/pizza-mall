package club.kynb.mall.payment.constant;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

import java.util.stream.Stream;

/**
 * 交易订单类型
 *
 * @author kynb_club@163.com
 * @date:  2021/7/2
 */

@Getter
@AllArgsConstructor
public enum TradeOrderTypeEnum implements EnumValidI {

	PAY(1, "支付"),
	REFUND(2, "退款"),
	;
	
	private final Integer type;
	private final String desc;

	@Override
	public String code() {
		return String.valueOf(this.type);
	}

	@Override
	public String message() {
		return this.desc;
	}

	@JsonIgnore
	public static TradeOrderTypeEnum get(Integer type) {
		return Stream.of(TradeOrderTypeEnum.values()).filter(i -> i.getType().intValue() == type.intValue()).findFirst().orElse(null);
	}
}
