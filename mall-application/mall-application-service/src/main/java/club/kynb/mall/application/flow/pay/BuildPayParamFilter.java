package club.kynb.mall.application.flow.pay;

import club.kynb.mall.application.context.MallUserContext;
import club.kynb.mall.application.context.OrderPayContext;
import club.kynb.mall.application.model.model.command.OrderPayCommand;
import club.kynb.mall.payment.constant.PayChannelEnum;
import club.kynb.mall.payment.constant.PayClientTypeEnum;
import club.kynb.mall.payment.constant.PayOrderTypeEnum;
import club.kynb.mall.payment.dto.ext.PayRequestDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pizza.common.web.exception.Errors;
import org.pizza.pattern.filter.Filter;
import org.pizza.pattern.filter.FilterInvoker;
import org.springframework.stereotype.Component;

/**
 * @author kynb_club@163.com
 * @date: 2021/7/8
 */
@Slf4j
@Component
@AllArgsConstructor
public class BuildPayParamFilter implements Filter<OrderPayContext> {


    @Override
    public void doFilter(OrderPayContext orderPayContext, FilterInvoker<OrderPayContext> filterInvoker) {
        log.info("支付参数构建过滤器 start");
        MallUserContext userContext = MallUserContext.get(true);
        OrderPayCommand command = orderPayContext.getCommand();
        PayRequestDTO payRequestDTO = orderPayContext.getPayRequestDTO();
        payRequestDTO.setPayClientType(PayClientTypeEnum.get(MallUserContext.getHeader().getDeviceType()).orElseThrow(()-> Errors.SYSTEM.exception("系统异常，付款->客户类型丢失")));
        payRequestDTO.setPayChannel(PayChannelEnum.get(command.getPayChannel()).orElseThrow(()->Errors.BIZ.exception("支付方式错误")));
        payRequestDTO.setOrderType(PayOrderTypeEnum.get(command.getOrderType()).orElseThrow(()->Errors.BIZ.exception("订单类型错误")));
        payRequestDTO.setOrderNo(command.getInnerOrderNo());
        payRequestDTO.setUserId(userContext.getUserDTO().getId());
        payRequestDTO.setPayAmount(command.getOrderAmount());

        log.info("支付参数构建过滤器 end");
        filterInvoker.invoke(orderPayContext);
    }



}
