package club.kynb.mall.application.model.model.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.pizza.model.Command;

/**
 * @author kynb_club@163.com
 * @since 2020/12/28 9:00 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "CaptchaSaveCommand对象", description = "验证码保存命令")
@Accessors(chain = true)
public class CaptchaSaveCommand extends Command {
    @Length(message = "图形验证key长度有误", min = 10, max = 32)
    @ApiModelProperty(value = "图形验证key", example = "1234-1234-1234-1234")
    private String captchaKey;

    private String captchaCode;

}
