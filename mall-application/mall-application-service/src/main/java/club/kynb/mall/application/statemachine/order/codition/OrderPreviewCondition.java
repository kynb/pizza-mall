package club.kynb.mall.application.statemachine.order.codition;

import club.kynb.mall.application.context.OrderContext;
import club.kynb.mall.application.context.OrderPreviewContext;
import lombok.AllArgsConstructor;
import org.pizza.common.web.exception.Errors;
import org.pizza.statemachine.Condition;
import org.springframework.stereotype.Component;

/**
 * @author kynb_club@163.com
 * @since 2021/7/9 10:29 上午
 */
@Component
@AllArgsConstructor
public class OrderPreviewCondition implements Condition<OrderContext> {

    @Override
    public boolean isSatisfied(OrderContext orderContext) {
        if (orderContext instanceof OrderPreviewContext) {
            return true;
        }
        throw Errors.SYSTEM.exception("订单预览上下文转换失败");
    }

    @Override
    public String name() {
        return Condition.super.name();
    }
}
