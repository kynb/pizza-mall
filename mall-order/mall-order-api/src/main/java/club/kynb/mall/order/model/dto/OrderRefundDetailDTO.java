package club.kynb.mall.order.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单退款详情表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "OrderRefundDetailDTO对象", description = "订单退款详情表")
@Accessors(chain = true)
public class OrderRefundDetailDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", example = "")
    private Long userId;
    /**
     * 退款申请编号
     */
    @ApiModelProperty(value = "退款申请编号", example = "")
    private String refundNo;
    /**
     * 订单详情id
     */
    @ApiModelProperty(value = "订单详情id", example = "")
    private Long orderDetailId;
    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", example = "")
    private Integer num;
    /**
     * 退款金额
     */
    @ApiModelProperty(value = "退款金额", example = "")
    private BigDecimal amount;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息", example = "")
    private String remark;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
