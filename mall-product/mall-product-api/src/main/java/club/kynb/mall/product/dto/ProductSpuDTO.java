package club.kynb.mall.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品信息表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "ProductSpuDTO对象", description = "商品信息表")
@Accessors(chain = true)
public class ProductSpuDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ApiModelProperty(value = "", example = "")
    private String id;
    /**
     * 二级分类
     */
    @ApiModelProperty(value = "二级分类", example = "")
    private String categoryIds;
    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码", example = "")
    private String spuCode;
    /**
     * 外部编码
     */
    @ApiModelProperty(value = "外部编码", example = "")
    private String spuOutCode;
    /**
     * 名称
     */
    @ApiModelProperty(value = "名称", example = "")
    private String spuName;
    /**
     * 关键词
     */
    @ApiModelProperty(value = "关键词", example = "")
    private String spuKeywords;
    /**
     * 价格
     */
    @ApiModelProperty(value = "价格", example = "")
    private BigDecimal spuPrice;
    /**
     * 详情
     */
    @ApiModelProperty(value = "详情", example = "")
    private String spuDesc;
    /**
     * 图片
     */
    @ApiModelProperty(value = "图片", example = "")
    private String spuImgs;
    /**
     * 上架状态
     */
    @ApiModelProperty(value = "上架状态", example = "")
    private Integer spuOnSale;
    /**
     * 下架时间
     */
    @ApiModelProperty(value = "下架时间", example = "")
    private Date offSaleTime;
    /**
     * 销售数量
     */
    @ApiModelProperty(value = "销售数量", example = "")
    private Integer spuSaleNum;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序", example = "")
    private Integer sequence;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
