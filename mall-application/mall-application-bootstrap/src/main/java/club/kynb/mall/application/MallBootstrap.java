package club.kynb.mall.application;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.EnableSpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.NumberFormat;

/**
 * Spring Boot Starter
 *
 * @author archetype create
 */
@Slf4j
@EnableAsync
@EnableSpringUtil
@EnableFeignClients
@SpringBootApplication(scanBasePackages = {"club.kynb.mall"})
public class MallBootstrap {

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(MallBootstrap.class).run(args);
        Environment env = context.getEnvironment();
        Runtime runtime = Runtime.getRuntime();
        final NumberFormat format = NumberFormat.getInstance();
        final long maxMemory = runtime.maxMemory();
        final long allocatedMemory = runtime.totalMemory();
        final long freeMemory = runtime.freeMemory();
        final long mb = 1024 * 1024;
        final String mega = "MB";
        log.warn("--------------------------------------------------------\n" +
                        "  Application:\t'{}' is running!          \n" +
                        "  Local: \t\thttp://localhost:{}{}          \n" +
                        "  doc:   \t\thttp://{}:{}{}/doc.html       \n" +
                        "  Active: \t\t{}                    \n" +
                        "  Free memory: \t\t{}                    \n" +
                        "  Allocated memory: \t\t{}                    \n" +
                        "  Max memory: \t\t{}                    \n" +
                        "  Total free memory: \t\t{}                    \n" +
                        "  探讨请联系：kynb_club@163.com                  \n " +
                        "--------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"),
                StrUtil.blankToDefault(env.getProperty("server.servlet.context-path"), "/"),

                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                StrUtil.blankToDefault(env.getProperty("server.servlet.context-path"), "/"),

                StrUtil.blankToDefault(env.getProperty("spring.profiles.active"), "-"),
                format.format(freeMemory / mb) + mega,
                format.format(allocatedMemory / mb) + mega,
                format.format(maxMemory / mb) + mega,
                format.format((freeMemory + (maxMemory - allocatedMemory)) / mb) + mega
        );

    }

}
