package club.kynb.mall.application.limiter;


/**
 * @author kynb_club@163.com
 * @since 2020/12/18 6:05 下午
 */
public interface AuthFailedCountLimiter extends CountLimiter {
}
