package club.kynb.mall.payment.mysql.mapper;

import club.kynb.mall.payment.mysql.po.PayChannelPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 *  Mapper 接口
 *
 * @author fyh
 * @since 2021-06-29
 */
@Mapper
public interface PayChannelMapper extends BaseMapper<PayChannelPO> {


}
