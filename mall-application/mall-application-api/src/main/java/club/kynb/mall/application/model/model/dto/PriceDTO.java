package club.kynb.mall.application.model.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;

/**
 * 数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2021-06-27
 */

@Data
@ApiModel(value = "PriceDTO对象", description = "商品价格对象")
@Accessors(chain = true)
public class PriceDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ApiModelProperty(value = "商品价格", example = "")
    private BigDecimal price;

    /**
     *
     */
    @ApiModelProperty(value = "商品计价单位", example = "")
    private String unit;
}
