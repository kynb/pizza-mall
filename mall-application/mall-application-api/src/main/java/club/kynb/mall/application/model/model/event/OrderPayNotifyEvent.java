package club.kynb.mall.application.model.model.event;


import club.kynb.mall.order.constant.OrderPayStatusEnum;
import club.kynb.mall.payment.constant.PayOrderTypeEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.pizza.event.model.DomainEvent;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author kynb_club@163.com
 * @since 2020/11/21 10:55 上午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class OrderPayNotifyEvent extends DomainEvent {
    /**
     * 内部订单流水号
     */
    private String innerOrderNo;

    private PayOrderTypeEnum orderTypeEnum;

    private OrderPayStatusEnum statusEnum;

    private BigDecimal payAmount;

    private Date payTime;

    @Override
    public boolean isStore() {
        return true;
    }
}


