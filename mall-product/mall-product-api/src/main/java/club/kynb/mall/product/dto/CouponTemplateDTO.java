package club.kynb.mall.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.pizza.model.DTO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 优惠券模板表数据传输实体类
 *
 * @author kynb_club@163.com
 * @since 2022-03-16
 */

@Data
@ApiModel(value = "CouponTemplateDTO对象", description = "优惠券模板表")
@Accessors(chain = true)
public class CouponTemplateDTO implements DTO {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID", example = "")
    private Long id;
    /**
     * 券名字（外部显示）
     */
    @ApiModelProperty(value = "券名字（外部显示）", example = "")
    private String name;
    /**
     * 标题（外部显示）
     */
    @ApiModelProperty(value = "标题（外部显示）", example = "")
    private String title;
    /**
     * 描述信息（外部显示）
     */
    @ApiModelProperty(value = "描述信息（外部显示）", example = "")
    private String description;
    /**
     * 标签（外部显示）
     */
    @ApiModelProperty(value = "标签（外部显示）", example = "")
    private String tag;
    /**
     * 面值（单位：抵扣券：元；折扣券：百分号前的数值，0.1-99.9）
     */
    @ApiModelProperty(value = "面值（单位：抵扣券：元；折扣券：百分号前的数值，0.1-99.9）", example = "")
    private BigDecimal couponValue;
    /**
     * 券类型（1：现金券、2：折扣券）
     */
    @ApiModelProperty(value = "券类型（1：现金券、2：折扣券）", example = "")
    private Integer couponType;
    /**
     * 叠加使用，0：不允许叠加，1：允许同类叠加
     */
    @ApiModelProperty(value = "叠加使用，0：不允许叠加，1：允许同类叠加", example = "")
    private Integer useSuperimposedType;
    /**
     * 领取方式，1：主动领取，2：系统发放
     */
    @ApiModelProperty(value = "领取方式，1：主动领取，2：系统发放", example = "")
    private Integer receiveWay;
    /**
     * 发放数量，主动领取方式有效
     */
    @ApiModelProperty(value = "发放数量，主动领取方式有效", example = "")
    private Integer releaseQuantity;
    /**
     * 剩余数量，主动领取方式有效
     */
    @ApiModelProperty(value = "剩余数量，主动领取方式有效", example = "")
    private Integer residueQuantity;
    /**
     * 领取开始时间，主动领取方式有效
     */
    @ApiModelProperty(value = "领取开始时间，主动领取方式有效", example = "")
    private Date receiveStartTime;
    /**
     * 领取截止时间，主动领取方式有效
     */
    @ApiModelProperty(value = "领取截止时间，主动领取方式有效", example = "")
    private Date receiveEndTime;
    /**
     * 生效时间
     */
    @ApiModelProperty(value = "生效时间", example = "")
    private Date effectiveTime;
    /**
     * 失效时间
     */
    @ApiModelProperty(value = "失效时间", example = "")
    private Date invalidTime;
    /**
     * 商品使用范围（1：指定商品，2：全部商品）
     */
    @ApiModelProperty(value = "商品使用范围（1：指定商品，2：全部商品）", example = "")
    private Integer spuRange;
    /**
     * 限定的商品ID列表，指定商品时有效
     */
    @ApiModelProperty(value = "限定的商品ID列表，指定商品时有效", example = "")
    private String spuIds;
    /**
     * 撤单返还 0：不返还，1：返还
     */
    @ApiModelProperty(value = "撤单返还 0：不返还，1：返还", example = "")
    private Integer returnQualification;
    /**
     * 使用金额门槛，达到门槛，才能使用券
     */
    @ApiModelProperty(value = "使用金额门槛，达到门槛，才能使用券", example = "")
    private BigDecimal thresholdAmount;
    /**
     * 状态：0待生效，1进行中，2已结束，3禁用
     */
    @ApiModelProperty(value = "状态：0待生效，1进行中，2已结束，3禁用", example = "")
    private Integer status;
    /**
     * 内部备注
     */
    @ApiModelProperty(value = "内部备注", example = "")
    private String remark;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", example = "")
    private String extension;
    /**
     * 是否删除0否1是
     */
    @ApiModelProperty(value = "是否删除0否1是", example = "")
    private Integer deleted;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", example = "")
    private Long createUser;
    /**
     * 更新用户
     */
    @ApiModelProperty(value = "更新用户", example = "")
    private Long updateUser;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", example = "")
    private Long tenantId;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "")
    private Date createTime;
}
