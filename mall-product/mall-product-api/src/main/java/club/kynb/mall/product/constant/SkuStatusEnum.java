package club.kynb.mall.product.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.pizza.valid.EnumValidI;

/**
 * spu在售状态
 *
 * @author kynb_club@163.com
 * @date: 2021/6/27
 */
@Getter
@AllArgsConstructor
public enum SkuStatusEnum implements EnumValidI {

    INIT(0, "初始化"),
    UP(1, "上架"),
    DOWN(2, "下架");

    private final Integer status;
    private final String desc;

    @Override
    public String code() {
        return String.valueOf(this.status);
    }

    @Override
    public String message() {
        return this.desc;
    }
}
