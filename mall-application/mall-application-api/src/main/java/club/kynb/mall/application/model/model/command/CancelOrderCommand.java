package club.kynb.mall.application.model.model.command;

import club.kynb.mall.order.constant.OrderCancelTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.pizza.model.Command;

import javax.validation.constraints.NotBlank;

/**
 * @author kynb_club@163.com
 * @since 2021/7/1 9:09 下午
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "CancelOrderCommand对象", description = "取消订单命令")
@Accessors(chain = true)
public class CancelOrderCommand extends Command {

    @ApiModelProperty(value = "内部订单编号", example = "454158125549748224")
    @NotBlank(message = "内部订单编号不能为空")
    private String innerOrderNo;

    @ApiModelProperty(value = "取消订单描述", example = "拍错了")
    @NotBlank(message = "取消订单描述")
    private String cancelDesc;

    @ApiModelProperty(value = "取消订单类型枚举", example = "MANUAL_CANCELLED",hidden = true)
    private OrderCancelTypeEnum cancelTypeEnum ;


}
